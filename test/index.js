//Global var assignments
window.map = util.initMap(); //util.initMap({ editorControl: true });
window.fce = L.FCE.addTo(window.map);
//Tests to run.
//TODO: update this loader to pull in config ==> wrap tests into separate suites {e.g., all, init, api, edit, draw, etc.)
var tests = {
    map: require("./spec/map.spec.js"),
    core: require("./spec/fce.core.spec.js"),
    controller: require("./spec/fce.controller.spec.js"),
    toolbar: require("./spec/fce.toolbar.spec.js"),
    control: require("./spec/fce.control.spec.js"),
    util: require("./spec/util.spec.js"),
    action: require("./spec/fce.action.spec.js"),
    storage: require("./spec/fce.storage.spec.js"),
    operations: require("./spec/fce.operations.spec.js"),
    interaction: require('./spec/fce.interaction.spec.js')
};
module.exports = tests;