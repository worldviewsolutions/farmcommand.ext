var Util = function(){};
Util.prototype = {
    constructor:Util,
    initMap: function(options){
        // Create map since no html is passed to karma
        var map = document.createElement("div");
        map.setAttribute('id', 'map');
        document.body.appendChild(map);
        L.Icon.Default.imagePath = 'leaflet/images/marker-icon.png';
        return new L.Map('map').setView([37.532855, -77.431561], 13);
    }
};
module.exports = new Util;
