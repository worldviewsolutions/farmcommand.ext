describe('L.FCE.Operations', function () {
    var name = "L.FCE.Operations ";
    var ops = fce.ops;

    describe(name, function () {
        it('should initialize.', function () {
            assert.isObject(ops);
        });

        it('should have defaults.', function () {
            assert.isNumber(ops._max);
            assert.isArray(ops._stack);
            assert.isNumber(ops._current);
        });

        it('defaults should have values.', function () {
            expect(fce.ops._max).gte(0);
            expect(ops._stack.length).equal(0);
            expect(fce.ops._current).equal(-1);
        });
    });

    describe(name + 'public methods ', function () {
        it('should exist.', function () {
            assert.isFunction(ops.start);
            assert.isFunction(ops.stop);
            assert.isFunction(ops.clear);
            assert.isFunction(ops.next);
            assert.isFunction(ops.prev);
            assert.isFunction(ops.setWorkflow);
        });
    });

    describe(name + 'private methods', function () {
        it('should exist.', function () {
            assert.isFunction(ops._pushOp);
            assert.isFunction(ops._getIndex);
            assert.isFunction(ops._setCurrent);
            assert.isFunction(ops._add);
            assert.isFunction(ops.updateWorkflow);
            assert.isFunction(ops._eventHandler);
        });
    });

    describe(name + 'stack operations and behavior', function () {

        var eventHandler = function (e) {
            if (!e.message || !e.data || !e.data.value) { return; }
            var value = e.data.value;
            e.callback(true); //let ops know revert success
        };
        var setTarget = function () {
            // handle fired updated event
            fce.on(fce.events.ops.updated.type, eventHandler);
            // set L.FCE.ops target
            var targetHandler = function (e) { e.callback(); };
            fce.on(fce.events.ops.reset.type, targetHandler);
            var target = new fce._Base.Event({
                source: { name: "target" }, callback: eventHandler,
                type: "action", message: "Test", data: { value: "Initial" }
            });  //create event
            ops.load(target);
        };
        var fireEvent = function (name, workflow, value) {
            var inWorkflow = function() {
                return workflow;
            }
            var setWorkflowState = function(){}
            var createEvent = function (val) {
                return new L.FCE._Base.Event({
                    source: { name: name, setWorkflowState:setWorkflowState, workflow:workflow, inWorkflow: inWorkflow, end: function () { } }, callback: eventHandler,
                    type: "action", message: "test", data: { value: val }, target: ''
                });
            }
            // send multiple events if value an array of actions
            if (value instanceof Array) {
                value.forEach(function (v) {
                    var e = createEvent(v)
                    fce.fire(L.FCE.events.ops.new.type, e, this);
                })
                return;
            }
            var e = createEvent(value);
            fce.fire(L.FCE.events.ops.new.type, e, this);
        };

        var undo = function (number) {
            for (var i = 0; i < number; i++) {
                ops.prev();
            }
        };

        var redo = function (number) {
            for (var i = 0; i < number; i++) {
                ops.next();
            }
        };

        var currentValue = function () {
            return ops._stack[ops._current].data.value
        };

        var finish = function () {
            ops.updateWorkflow(false);
        };

        var cancel = function () {
            ops.setWorkflow();
        };

        var clear = function () {
            ops.clear();
        }

        setTarget();

        beforeEach(function () {
            ops.clear();
        });

        describe('#add()', function() {
            it('should add to stack', function () {
                fireEvent("A", false, 1);
                assert.equal(ops._stack[ops._current].source.name, "A");
                assert.equal(currentValue(), 1);
            });
        });

        describe('#clear()', function() {
            it('should add to stack', function () {
                fireEvent("A", true, 1);
                clear();
                assert.equal(ops._stack.length, 0);
            });
        });

        describe('#workflow()', function() {
            it('#should start a workflow', function () {
                fireEvent("A", true, 1);
                assert.equal(ops._stack[ops._current].source.name, "A");
                assert.equal(currentValue(), 1);
            });
        });

        describe('#finish()', function() {
            it('should finish a workflow', function () {
                fireEvent("A", true, "1");
                finish();
                assert.equal(currentValue(), "1");
                ops.clear();
                fireEvent("A", true, ["1", "1,2"]);
                finish();
                assert.equal(currentValue(), "1,2");
                ops.clear();
                fireEvent("A", true, ["1", "1,2", "1,2,3"]);
                finish();
                assert.equal(currentValue(), "1,2,3");
            });
        });

        describe('#undo()', function () {
            it('should undo once', function() {
                fireEvent("A", false, ["1", "1,2,3"]);
                undo(1);
                assert.equal(currentValue(), "1");
                ops.clear();
            });
            it('should undo more than once', function() {
                fireEvent("A", false, ["1", "1,2,3", "1,2,3,4", "1,2,3,4,5"]);
                undo(2);
                assert.equal(currentValue(), "1,2,3");
                ops.clear();
            });

            // it('should undo to head', function() {
            //     fireEvent("A", false, ["1", "1,2,3"]);
            //     undo(2);
            //     assert.equal(currentValue(), "1");
            // });

            it('should undo within a workflow once', function () {
                fireEvent("A", true, ["1", "1,2,3"]);
                undo(1);
                assert.equal(currentValue(), "1");
                ops.clear();
            });

            it('should undo within a workflow more than once', function () {
                fireEvent("A", true, ["1", "1,2,3", "1,2,3,4", "1,2,3,4,5"]);
                undo(2);
                assert.equal(currentValue(), "1,2,3");
                ops.clear();
            });
            // it('should undo within a workflow to head', function () {
            //     fireEvent("A", true, ["1", "1,2,3"]);
            //     undo(3);
            //     assert.equal(currentValue(), "1");
            // });

            it('should undo within a workflow to a prevous finished action', function () {
                fireEvent("A", false, ["A"]);
                fireEvent("B", true, ["1"]);
                finish();
                undo(1);
                assert.equal(currentValue(), "A");
                ops.clear();
            });

            // it('should undo within a workflow to a prevous finished action more than once', function () {
            //     fireEvent("A", false, ["A"]);
            //     fireEvent("B", true, ["1", "1,2,3", "1,2,3,4", "1,2,3,4,5"]);
            //     finish();
            //     undo(1);
            //     assert.equal(currentValue(), "A");
            //     ops.clear();
            // });

            // it('should undo within a workflow to a prevous finished action to head', function () {
            //     fireEvent("A", false, ["A"]);
            //     fireEvent("B", true, ["1", "1,2,3"]);
            //     finish();
            //     undo(3);
            //     assert.equal(currentValue(), "A");
            // });

            it('should undo to the previous finished action after finishing a workflow once', function () {
                fireEvent("A", false, ["A"]);
                fireEvent("B", true, ["1"]);
                undo(1);
                assert.equal(currentValue(), "A");
                ops.clear();
            });
            // it('should undo to the previous finished action after finishing a workflow more than once', function () {
            //     fireEvent("A", false, ["A"]);
            //     fireEvent("B", true, ["1", "1,2,3", "1,2,3,4", "1,2,3,4,5"]);
            //     finish();
            //     fireEvent("B", true, ["1", "1,2,3", "1,2,3,4", "1,2,3,4,5"]);
            //     finish();
            //     undo(4);
            //     assert.equal(currentValue(), "A");
            //     ops.clear();
            // });
            // it('should undo to the previous finished action after finishing a workflow to head', function () {
            //     fireEvent("A", false, ["A"]);
            //     fireEvent("B", true, ["1", "1,2,3"]);
            //     undo(3);
            //     assert.equal(currentValue(), "A");
            // });
        });


        describe('#redo', function() {
            // it('should undo and redo all finished actions', function () {
            //     fireEvent("A", false, "A");
            //     fireEvent("B", false, "B");
            //     fireEvent("C", false, "C");
            //     fireEvent("D", false, "D");

            //     undo(1);
            //     redo(1);
            //     assert.equal(currentValue(), "D");

            //     undo(4);
            //     redo(2);
            //     assert.equal(currentValue(), "C");

            //     undo(4);
            //     redo(4);
            //     assert.equal(currentValue(), "D");
            // });

            it('should undo and redo all finished actions during a workflow', function () {
                fireEvent("A", false, "A");
                fireEvent("B", true, ["1", "1,2", "1,2,3"]);
                finish();
                fireEvent("C", false, "C");
                fireEvent("D", true, ["X", "X,Y", "X,Y,Z"])
                undo(1);
                redo(1);
                assert.equal(currentValue(), "X,Y,Z");

                undo(5);
                redo(2);
                assert.equal(currentValue(), "C");
                redo(3);
                assert.equal(currentValue(), "X,Y,Z");
            });

        });

        describe('#undoWithClear()', function () {
            it('should clear after action undo', function () {
                fireEvent("A", false, "A");
                fireEvent("B", false, "B");
                undo(1);
                assert.equal(currentValue(), "A");
                fireEvent("C", false, "C");
                assert.equal(ops._stack.length, 2);
                assert.equal(currentValue(), "C");
                undo(1);
                assert.equal(currentValue(), "A");
            });
            it('should clear after many action undo', function () {
                fireEvent("A", false, "A");
                fireEvent("B", false, "B");
                fireEvent("C", false, "C");
                fireEvent("D", false, "D");
                undo(2);
                fireEvent("X", false, "X");
                assert.equal(ops._stack.length, 3);
                assert.equal(currentValue(), "X");
                undo(1);
                assert.equal(currentValue(), "B");
                undo(1);
                assert.equal(currentValue(), "A");
            });
            it('should clear after workflow undo', function () {
                fireEvent("A", false, "A");
                fireEvent("B", true, ["1"]);
                undo(1);
                assert.equal(currentValue(), "A");
                fireEvent("C", false, "B");
                assert.equal(ops._stack.length, 2);
                assert.equal(currentValue(), "B");
                undo(1);
                assert.equal(currentValue(), "A");
            });
            it('should clear after many workflow undo and 1 workflow start', function () {
                fireEvent("A", false, "A");
                fireEvent("B", true, ["1"]);
                undo(1);
                assert.equal(currentValue(), "A");
                fireEvent("C", true, ["X"]);
                assert.equal(ops._stack.length, 2);
                assert.equal(currentValue(), "X");
                undo(1);
                assert.equal(currentValue(), "A");
            });
            it('should clear after many workflow undo and multiple workflow actions', function () {
                fireEvent("A", false, "A");
                fireEvent("B", true, ["1"]);
                undo(1);
                assert.equal(currentValue(), "A");
                fireEvent("C", true, ["1", "1,2"]);
                assert.equal(ops._stack.length, 3);
                assert.equal(currentValue(), "1,2");
                undo(1);
                assert.equal(currentValue(), "1");
                undo(1);
                assert.equal(currentValue(), "A");
            });
        });

        describe('#cancel()', function() {

            // it('should keep previous finished actions ', function () {
            //     fireEvent("A", false, "A");
            //     fireEvent("B", false, "B");
            //     fireEvent("C", true, ["1", "1,2", "1,2,3"]);
            //     finish();
            //     fireEvent("D", false, "D");
            //     fireEvent("B", true, ["1"]);
            //     cancel();
            //     assert.equal(ops._stack.length, 4);
            //     undo(4);
            //     assert.equal(currentValue(), "A");
            //     redo(4);
            //     assert.equal(currentValue(), "D");
            // });
        })
    })
});
