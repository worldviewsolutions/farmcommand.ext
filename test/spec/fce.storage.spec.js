//FC Editor Control test specs
describe('L.fce.storage', function () {
    var name = "L.FCE.storage ";
    // reset 
    fce.removeData();

    describe(name + 'add() ', function () {
        it('should add to fce layer stack.', function () {
            fce.loadData(window.layers); //calls storage.add
            assert.isObject(fce.storage.getById(0));
        });
    });

    describe(name + 'getById() ', function () {
        it('should return a layer if added.', function () {
            assert.isObject(fce.storage.getById(1));
        });

        it('should return null if layer not added.', function () {
            assert.isNull(fce.storage.getById(666));
        });
    });

    describe(name + 'delete() ', function () {
        it('should return null if removed.', function () {
            var gLyr = fce.data.create(999);
            fce.storage.add(gLyr);
            fce.storage.delete(999);
            assert.isNull(fce.storage.getById(999));
        });
    });


    describe(name + 'setTargetData() ', function () {

        it('should add the layer as the target layer.', function () {
            fce.storage.setTargetData(fce.storage.getById(1));
            assert.equal(fce.targetData.options.storeId, 1);
        });

    });
    describe(name + '#selection ', function () {
        fce.removeData();
        fce.loadData(window.layers);
        var first_layer = fce.storage.getById(0);
        var second_layer = fce.storage.getById(1);
        
        var getOneGroupId = function(layer) {
            var groupId;
            var layer = first_layer.eachLayer(function(layer) {
                groupId = layer.options.group_id;
            });
            return groupId;
        }
        var firstGroupId = getOneGroupId(first_layer);
        var secondGroupId = getOneGroupId(second_layer);
        var getSelectedIds = function() {
            return Object.keys(fce.selectionMap);
        }

        it('should remove layer if already selected.', function () {
            assert.equal(getSelectedIds().length, 0);
            fce.storage.updateSelected(0, firstGroupId, true);
            assert.equal(getSelectedIds().length, 1);
            fce.storage.updateSelected(0, firstGroupId, false);
            assert.equal(getSelectedIds().length, 0);
        });
        it('should add a layer to the selection stack on selection.', function () {
            fce.storage.updateSelected(0, firstGroupId, true);
            assert.equal(getSelectedIds().length, 1);
            assert.equal(fce.selectionMap[0][0], firstGroupId);
            fce.storage.updateSelected(1, secondGroupId, true);
            assert.equal(getSelectedIds().length, 2);
            assert.equal(fce.selectionMap[1][0], secondGroupId);
            });
        it('should remove all selected layers.', function () {
            assert.equal(L.Util.isEmpty(fce.selectionMap), false);
            fce.storage.clearSelection();
            assert.equal(getSelectedIds().length, 0);
        });
    });


});