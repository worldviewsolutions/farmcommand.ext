describe('L.FCE.Controller', function () {
    var name = 'L.FCE.Controller';
    var controller = fce.controller;

    var toolbarName = Object.keys(fce.options.toolbars)[0]; // test against first toolbar in config

    describe(name, function () {
        it(' should exist.', function () {
            assert.isObject(controller);
        })

        it(' should have toolbars prop.', function () {
            assert.isObject(controller.toolbars);
            //expect(controller.toolbars.length).gte(0);
    })
    });

    var toolbar = controller.loadToolbar(toolbarName);
    var toolbarDoesNotExist = controller.loadToolbar('Undefined');
    describe(name + ' loadToolbar() ', function() {
        it('should return a toolbar object.', function () {
            assert.isOk(toolbar);
        });
        it('should add toolbar to toolbars prop.', function(){
            assert.isOk(controller.toolbars[toolbarName])
        });
        it ('should not load when toolbar does not exist.', function() {
            assert.isNotOk(toolbarDoesNotExist);
        })
    });


    describe(name + ' createToolbar() ', function() {
        it('should create a toolbar.', function() {
            assert.isObject(controller.createToolbar(toolbarName));
        }) 

        it('should be empty if toolbar does not exist.', function() {
             expect(controller.createToolbar('undefined')).to.be.empty;
        })
    });

    describe(name + ' getToolbar() ', function() {
        it('should return a toolbar.', function() {
            assert.isOk(controller.getToolbar(toolbarName))
        })

        it('should be null if name DNE.', function() {
            assert.isNull(controller.getToolbar('undefined'));
        })
    });

    describe(name + ' getToolbarOptions() ', function() {
        it('should get toolbar options.', function() {
            assert.isObject(controller.getToolbarOptions(toolbarName))
        })

        it('should be empty if no toolbar options.', function() {
            expect(controller.getToolbarOptions('undefined')).to.be.empty;
        })
    })

});