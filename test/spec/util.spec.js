describe('L.Util', function () {
    var name = 'L.Util';
    describe(name + ' isEmpty(obj) ', function() {
        it('should return true if empty.', function() {
            assert.isTrue(L.Util.isEmpty({}));
        })
        it('should return false if not empty.', function() {
            assert.isFalse(L.Util.isEmpty({test:'test'}));
        })
    })
})