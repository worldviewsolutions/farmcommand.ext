describe('L.FCE', function () {
    var name = 'L.FCE.core ';
    var toolbarName = Object.keys(L.FCE.options.toolbars)[0]; // test against first toolbar in config
    var validateResults = function(results){
        assert.isArray(results);
        assert.isTrue(results.length > 0);
    };

    describe(name, function () {
        it(' should exist.', function () {
            assert.isObject(L.FCE);
        });

        it(' should have options', function () {
            assert.isObject(L.FCE.options);
        });

        it(' should have action classes', function () {
            expect(L.FCE.Actions).to.not.be.empty;
        });

        it(' should have base classes', function () {
            expect(L.FCE._Base).to.not.be.empty;
        });

    });

    describe(name + 'addTo() ', function () {
        it('should be able to load controller.', function () {
            assert.isOk(fce.controller);
        });

        it('should have access to map.', function () {
            assert.isObject(fce.map);
        });
    });

    describe(name + 'loadToolbar() ', function () {
        it('should call controller and load toolbar.', function() {
            assert.isObject(fce.loadToolbar(toolbarName))
        });
    });
    describe(name + 'unloadToolbar() ', function () {
        it('should call controller method.', function () {

        });
    });
    describe(name + 'call() ', function () {
        it('should call controller method.', function () {

        });
    });
    
    //add
    describe(name + 'addData() ', function () {
        it('should create new groupLayer if uid not in store.', function () {
            var results = fce.addData(111);
            validateResults(results);
            assert.isObject(results[0]);
            assert.isObject(results[0].data);

        });

        it('should create new groupLayers from an array of groupLayer data.', function () {
            var results = fce.addData([222,333,444]);
            validateResults(results);
            results.forEach(function(layer){
                assert.isObject(layer);
                assert.isObject(layer.data);
            });
            fce.removeData();
        });

        //todo should return error for invalid uids
    });
    //load
    describe(name + 'loadData() ', function () {
        it('should return a layer if not already loaded.', function () {
            var results = fce.loadData(window.layers[0]);
            validateResults(results);
            assert.isObject(results[0].data);
        });

        it('should load groupLayer data.', function () {
            assert.isFalse(L.Util.isEmpty(L.FCE.store));
        });

        it('should return obj if uid already present in store.', function () {
            var results = fce.loadData(window.layers[0]);
            validateResults(results);
            assert.isObject(results[0].data);
        });

        it('should load an array of groupLayer data.', function () {
            fce.removeData();
            var results = fce.loadData(window.layers);
            validateResults(results);
            results.forEach(function(layer){
                assert.isObject(layer.data);
            });
        });
    });

    //export
    describe(name + 'exportData() ', function () {
        fce.removeData();
        it('should export a groupLayer geoJSON if uid is in the store.', function () {
            fce.loadData(window.layers[0]);
            var results = fce.exportData(0);
            validateResults(results);
            assert.isObject(results[0].data);
        });

        it('should export array of groupLayers geoJSON if uids are in the store.', function () {
            fce.loadData(window.layers[1]);
            var results = fce.exportData([0,1]);
            validateResults(results);
            results.forEach(function(geoJson){
                assert.isObject(geoJson.data);
            });
        });
    });
    
    //remove 
    // do this last, clean up for other tests
    describe(name + 'removeData() ', function () {
        it('should remove groupLayer if uid is in store.', function () {
            assert.isObject(fce.storage.getById(0));
            fce.removeData(0);
            var result = assert.isNull(fce.storage.getById(0));
        });

        it('should remove all layers if no ids are supplied.', function () {
            fce.removeData()
            var result = assert.equal(fce.storage.getIds(), 0);
        });
    });
});