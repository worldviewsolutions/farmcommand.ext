//Map object test specs
describe('L.FCE.Control', function () {
    var name = 'L.FCE.Control';
    var toolbarName = Object.keys(L.FCE.options.toolbars)[0]; // test against first toolbar in config
    var toolbar = fce.loadToolbar(toolbarName);
    var control = toolbar.control;

    describe(name + ' setVisibility() ', function () {

        it('should set visibility to true when false.', function () {
            control.options.isVisible = false;
            control.setVisibility(true);
            assert.isTrue(control.options.isVisible);
        })
        it('should set visibility to false when true.', function () {
            control.options.isVisible = true;
            control.setVisibility(false);
            assert.isFalse(control.options.isVisible);
        })
        it('should do nothing visibility equivalent.', function () {
            control.options.isVisible = true;
            control.setVisibility(true);
            assert.isTrue(control.options.isVisible);
            control.options.isVisible = false;
            control.setVisibility(false);
            assert.isFalse(control.options.isVisible);
        })
    })
});