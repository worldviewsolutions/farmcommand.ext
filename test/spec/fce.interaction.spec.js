//FC Editor Control test specs
describe('L.fce.interaction', function () {
    var name = "L.FCE.interaction ";

    var getFeatures = function (id) {
        return fce.exportData(id)[0].data.features;
    }

    var getAction = function (toolbarName, actionName) {
        var toolbar = fce.controller.getToolbar(toolbarName);
        return toolbar.getBaseAction(actionName);
    };

    var getSubAction = function(toolbarName, actionName, subactionName) {
        var toolbar = fce.controller.getToolbar(toolbarName);
        return toolbar.getSubAction(actionName, subactionName);
    }

    var activateDrawing = function(drawingType) {
        var drawTool = getAction('draw', 'drawTool');
        var selectPolygon = getAction('draw', 'drawMenu', drawingType);
        selectPolygon.addHooks();
        drawTool.addHooks();  
    }

    var addMarker = function(coords) {
        L.marker(coords).addTo(map);
    }
    describe(name + '#features', function () {


        it('should add a polygon', function () {
        });
        it('should create a hole', function() {
        })

        it('should add a polygon', function () {
        });

        it('should draw a polyline', function() {
        });

        it('should draw a marker', function() {
        });

        it('should create multiple features', function() {
        });

        it('should extend a feature', function() {
        });

        it('should export extended geometries correctly', function() {

        })
    });


    describe(name + '#selectionInteraction', function() {
        beforeEach(function() {
        });
        afterEach(function() {
        });
        it('should select and unselect a feature', function() {
        })
    });
});