//FC Editor Control test specs
describe('L.FCE._Base.baseAction', function () {
    var name = 'L.FCE.baseAction';
    var toolbarName = Object.keys(L.FCE.options.toolbars)[0]; // test against first toolbar in config
    var toolbar = fce.loadToolbar(toolbarName);
    var baseActionName = Object.keys(toolbar.baseActions)[0];
    var action = toolbar.getBaseAction(baseActionName);

    describe(name, function () {
        it(' should exist.', function(){
            assert.isObject(action);
        });
    });

    describe(name + ' clearMenu() ', function () {
        it('should set menuActive to false.', function () {
            assert.isFalse(action.clearMenu());
        })
    })

    describe(name + ' setMenuStyle() ', function () {
        it('should change left, right styles if right.', function () {
            action.parentToolbar.options.control.position = 'topright';
            action.setMenuStyle();
            var el = action.options.subToolbar._ul;
            assert.equal(el.style.right, '26px');
        });
        it('should do noting if style is left.', function () {
            action.parentToolbar.options.control.position = 'topleft';
            assert.isUndefined(action.setMenuStyle())
        })
    })
});