//FC Editor Control test specs
describe('L.fce.data', function () {
    var name = "L.FCE.data ";
    var gl1 = fce.storage.getById(0);
    var gl2 = fce.storage.getById(1);

    describe(name + 'addData() ', function () {

        it('should return a list of layers', function () {
            var layerList = fce.addData(777);
            assert.equal(layerList.length, 1);
            var layer = layerList[0];
            assert.equal(layer.id, 777);
        });
        it('each layer in list should be of type featuregroup', function () {
            var layerList = fce.addData([778, 779]);
            layerList.map(function(layer) {
                assert.equal(L.Util.typeOf(layer.data), 'L.FeatureGroup')
            })
        });
        it('should have default properties.', function () {
            var layerList = fce.addData(888);
            assert.isTrue('storeId' in layerList[0].data.options);
            assert.isTrue(('editable' in layerList[0].data.options));
        });
    });

    // uses data loaded via storage.spec.js
    describe(name + 'exportData() ', function () {
        it('should return geoJSON if geoJSON added.', function () {
            assert.isObject(fce.exportData(777)[0]);
        });

        it('should return undefined if no geoJSON added.', function () {
            assert.equal(fce.exportData({}).length, 0);
        });
    });

    describe(name + 'validateGeoJSON() ', function () {

        it('should return true when valid.', function () {
            assert.isTrue(fce.data.validateGeoJSON(window.layers[1]));
        });

        it('should return false when invalid.', function () {
            assert.isFalse(fce.data.validateGeoJSON({}));
            assert.isFalse(fce.data.validateGeoJSON({ type: 'FeatureCollection' }));
        });
    });
});