//FC Editor Control test specs
describe('L.FCE._Base.Toolbar', function () {
    name = 'L.FCE.Toolbar';
    var toolbarName = Object.keys(L.FCE.options.toolbars)[0]; // test against first toolbar in config
    var toolbar = fce.loadToolbar(toolbarName, {name:'test'});
    var baseActionName = Object.keys(toolbar.baseActions)[0];
    var subBaseActionName = Object.keys(toolbar.subActions)[0];

    // for loading and unloading
    var loadToolbarName = Object.keys(L.FCE.options.toolbars)[1];
    var loadToolbar = fce.loadToolbar(loadToolbarName);
    describe(name, function () {
        it(' should exist.', function(){
            assert.isObject(toolbar);
        });
        it(' should have a name.', function () {
            assert.isString(toolbar.name);
        });
    });

    describe(name + ' load() ', function () {
        it('should load the toolbar.', function () {
            assert.isFalse(L.Util.isEmpty(loadToolbar));
            assert.isTrue(loadToolbar.loaded);
            assert.isFalse(L.Util.isEmpty(loadToolbar.control));
        });

    })
    describe(name + ' unload() ', function () {
        it('should unload the toolbar.', function() {
            var unloadedToolbar = fce.unloadToolbar(loadToolbarName);
            assert.isFalse(unloadedToolbar.loaded);
            assert.isTrue(L.Util.isEmpty(unloadedToolbar.control));
        });
    })



    // commented out till state implementation complete
    describe(name + ' setState(state) ', function () {
        // it('should toggle all actions to off', function () {
        //     toolbar.changeActionState(false);
        //     Object.keys(toolbar.baseActions).map(function (key) {
        //         var action = toolbar.getBaseAction(key);
        //         assert.isFalse(action.options.state);
        //     })
        // })
        // it('should toggle all actions to on', function () {
        //     toolbar.changeActionState(true);
        //     Object.keys(toolbar.baseActions).map(function (key) {
        //         var action = toolbar.getBaseAction(key);
        //         assert.isTrue(action.options.state);
        //     })
        // })
    });

    describe(name + ' getBaseAction() ', function () {
        it('should get an action.', function () {
            assert.isOk(toolbar.getBaseAction(baseActionName));
        });
        it('should return null when no action exists.', function () {
            assert.isNull(toolbar.getBaseAction('null'));
        });
    });

    describe(name + ' getBaseActionClasses()', function () {
        it('should return list of action classes.', function () {
            assert.isFunction(toolbar.getBaseActionClasses()[0]);
        });
    });

    describe(name + ' getSubActions() ', function () {
        it('should get subactions.', function () {
            assert.isOk(toolbar.getSubActions(subBaseActionName));
        });
        it('should return null when no subaction exists.', function () {
            assert.isNull(toolbar.getSubActions('null'));
        });
    });

    describe(name + ' getSubAction() ', function () {
        var subActionName = Object.keys(toolbar.getSubActions(subBaseActionName))[0];
        it('should get a subaction.', function () {
            assert.isOk(toolbar.getSubAction(baseActionName, subActionName));
        });
        it('should return null when no subaction exists.', function () {
            assert.isNull(toolbar.getSubAction(baseActionName, 'null'));
        });
    });

    // do after injects dummy action into toolbar action list
    describe(name + ' addbaseAction() ', function () {
        it('should add a baseaction.', function () {
            toolbar.addBaseAction({ name: 'test' });
            assert.isOk(toolbar.getBaseAction('test'));
        });
        it('should not add a baseaction if previously added.', function () {
            assert.isUndefined(toolbar.addBaseAction({ name: 'test' }));
        });
    });

    describe(name + ' addSubAction() ', function () {
        it('should add a subaction.', function () {
            toolbar.addBaseAction({ name: 'test' });
            toolbar.addSubAction({ name: 'test' }, { name: 'subTest' });
            assert.isOk(toolbar.getSubAction('test', 'subTest'));
        })
        it('should not add a subaction if previously added.', function () {
            assert.isUndefined(toolbar.addSubAction({ name: 'test' }, { name: 'subTest' }));

        })
    });
});