var _ = require('lodash');
/**
 * farmcommand.leaflet gulp build configuration
 */
//Config Options
var paths = {
    src: {
        name: 'source code',
        dir: 'src/',
        js: { root: 'src/js/', dir: 'js/', entry: 'fce.js' },
        config: 'fce.config.js',
        libs: 'src/libs/',
        css: 'src/less/',
        images: 'src/images/'
    },
    dev: {
        name: 'development',
        dir: 'dev/',
        js: { root: 'dev/js/', dir: 'js/', out: 'fce.js' },
        libs: 'dev/libs/',
        css: 'dev/css/',
        images: 'dev/images/',
        files: ['README.md']
    },
    dist: {
        name: 'dist',
        isProd: true,
        dir: 'dist/',
        js: { root: 'dist/', dir: '', out: 'fce.min.js' },
        libs: 'dist/libs/',
        css: 'dist/',
        images: 'dist/images/',
        files: ['src/README.md', 'src/example.html']
    }
};

//override the config values from default only.
var configClass = { pref: '// Generate Config File \nvar config = ', suf: ';\nmodule.exports = config;' };//{pref: 'var config = function(){\nreturn ', suf: ';\n}();'}; //'L.FCE.config';
var configs = {
    default: {
        ops:{
            max: 10           
        },
        data:{
            uid: "uid"
        },
        toolbars: {
            general: {
                toolbarName: 'general',
                className: 'fce-general-toolbar',
                control: {
                    position: 'topright',
                    isVisible: true
                },
                actions: {
                    settingsMenu: {
                        state: 'active',
                        //exlcudeOnLoad: true
                        subActions: {
                            toggleSnapping: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            toggleTopology: {
                                state: 'inactive'
                                //exlcudeOnLoad: true
                            }
                        }
                    },
                    end: {
                        state: 'inactive'
                        //exlcudeOnLoad: true
                    },
                    redo: {
                        state: 'inactive'
                        //exlcudeOnLoad: true
                    },
                    undo: {
                        state: 'inactive'
                        //exlcudeOnLoad: true
                    },
                    cancel: {
                        state: 'inactive'
                        //exlcudeOnLoad: true
                    }
                }
            },
            draw: {
                toolbarName: 'draw',
                className: 'fce-draw-toolbar',
                control: {
                    position: 'topright',
                    isVisible: true
                },
                actions: {
                    drawMenu: {
                        state: 'active',
                        //exlcudeOnLoad: true
                        subActions: {
                            selectMarker: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            selectPolyline: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            selectPolygon: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            }
                        }
                    },
                    drawTool: {
                        state: 'active'
                        //exlcudeOnLoad: true
                    }
                }
            },
            edit: {
                toolbarName: 'edit',
                className: 'fce-edit-toolbar',
                control: {
                    position: 'topright',
                    isVisible: true
                },
                actions: {
                    reshapeMenu: {
                        state: 'active',
                        //exlcudeOnLoad: true
                        subActions: {
                            selectClone: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            selectSplit: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            selectMerge: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            },
                            selectClip: {
                                state: 'active'
                                //exlcudeOnLoad: true
                            }
                        }
                    },
                    reshapeTool: {
                        state: 'active'
                        //exlcudeOnLoad: true
                    },
                    deleteTool: {
                        state: 'active'
                        //exlcudeOnLoad: true
                    }
                }
            }
        },
        layers: {
            modes: {
                default: {
                    style: 'default',
                    editable: false,
                    visible: true,
                    movable: false
                },
                target: {
                    style: 'target',
                    editable: true,
                    visible: true,
                    movable: true
                }
            },
            styles:{
                edit: {
                    marker: {
                        class:'fce-marker-edit'
                    }
                },
                hidden: {
                    polygon: {
                        opacity: 0,
                        fillOpacity: 0
                    },
                    polyline: {
                        opacity: 0,
                        fillOpacity: 0
                    },
                    marker: {
                        class:''
                    }
                },
                default: {
                    polygon: {
                        color: '#2166ac',
                        weight: 2,
                        opacity: 1,
                        fillColor: '#4393c3',
                        fillOpacity: 0.5
                    },
                    polyline: {
                        color: '#2166ac',
                        weight: 2,
                        opacity: 1,
                    },
                    marker: {
                        class:''
                    }
                },
                target: {
                    polygon: {
                        color: '#b2182b',
                        weight: 2,
                        opacity: 1,
                        fillColor: '#d6604d',
                        fillOpacity: 0.5
                    },
                    polyline: {
                        color: '#b2182b',
                        weight: 2,
                        opacity: 1
                    },
                    marker: {
                        class:''
                    }
                },
                selected: {
                    polygon: {
                        weight: 4,
                        color: '#00FFFF'
                    },
                    polyline: {
                        weight: 4,
                        color: '#00FFFF'
                    },
                    marker: {
                        class:'fce-marker-selected'
                    }
                }
            }
        },
        modes: {
            hidden:{
                layers: {
                    all: 'default',
                    target: ''
                },
            },
            default: {
                layers: {
                    all: 'default',
                    target: ''
                },
                general:{allActions: 'default'}
            },
            drawAll: {
                layers: {
                    all: 'default',
                    target: ''
                },
                draw:{allActions: 'default'},
                general:{allActions: 'default'}
            },
            drawPolygon: {
                layers: {
                    all: 'default',
                    target: ''
                },
                draw: {
                    allActions: 'default',
                    actions: {
                        drawMenu: {
                            allActions: 'inactive',
                            subActions: {
                                selectPolygon: {
                                    state: 'select'
                                }
                            }
                        },
                        drawTool: {
                            state: 'active'
                        }
                    }
                },
                general: {allActions: 'default'}
            },
            target: {
                layers: {
                    all: 'default',
                    target: 'target'
                },
                draw: {
                    allActions: 'default',
                    actions: {
                        drawMenu: {
                            allActions: 'active',
                            subActions: {
                                selectPolygon: {
                                    state: 'select'
                                }
                            }
                        }
                    }
                },
                edit:{allActions: 'default'},
                general:{allActions: 'default'}
            }
        }
    },
    development: {
        DEBUG: true
    },
    production: {
        DEBUG: false
    }
};

//update config defaults for all env
configs.development = _.defaultsDeep(configs.development, [configs.default]);
configs.production = _.defaultsDeep(configs.development, [configs.default]);

var run = {
    development: {
        js: {
            uglify: false
        },
        css: {
            compress: false
        }
    },
    staging: {
        js: {
            uglify: true
        },
        css: {
            compress: true
        }
    },
    production: {
        js: {
            uglify: true
        },
        css: {
            compress: true
        }
    },
    default: {

        js: {
            uglify: false
        },
        css: {
            compress: false
        }
    }
};

var plugin = {
    default: {
        js: {
            uglify: {
                mangle: true
            }
        }
    },
    development: {
        js: {
            uglify: {
                mangle: false
            }
        }
    },
    staging: {
        js: {
            uglify: {
                mangle: true
            }
        }
    },
    production: {
        js: {
            uglify: {
                mangle: true
            }
        }
    }
};

module.exports.getConfig = function (env) {
    var isDev = env === 'development';
    var runOpts = _.merge({}, run.default, run[env]);
    var pluginOpts = _.merge({}, plugin.default, plugin[env]);
    var configsOpts = _.merge({}, configs.default, configs[env]);
    module.exports.paths = paths;
    module.exports.configs = configsOpts;
    module.exports.configClass = configClass;
    module.exports.run = runOpts;
    module.exports.plugin = pluginOpts;
    var config = module.exports;
    config.devtool = (isDev) ? 'eval' : null;
    return config;
};