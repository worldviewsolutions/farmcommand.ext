# UI Integration for farmcommand.leaflet   

Scaffolding and test interfaces for the farmcommand.leaflet plugin.  

# Installation

- Dependencies are installed through parent package.json

# Builds

#### Development 
- Run:  `gulp build-dev`
    > - builds dev directory.  files are browsified, concatenated, and source-mapped. No minification.
      
# History
TODO: Write history

# Credits
TODO: Write credits

# License
TODO: Write license