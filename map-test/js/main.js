"use strict";
//debugger;

(function () {
    //Globals
    window._ = require('lodash');
    window.React = require('react');
    window.ReactDOM = require('react-dom');
    window.RSify = require('react-semantify');
    window.EventEmitter = require('events').EventEmitter;

    window._Config = require('./config.js');

    var TileLayer = L.tileLayer;
    var map = new L.Map('map-container').setView([37.532855, -77.431561], 13);
    TileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var fce = L.FCE.addTo(map);
    window.map = map;

    //Setup sidebar
    var sidebarOpts = { map: map, fce: fce };
    var Sidebar = require('./Sidebar/sidebar.jsx')(sidebarOpts);
    Sidebar.Render();

    window.sidebar = Sidebar;
})();
