module.exports = {
    map: {
        style: {
            marker: { default: { "opacity": 0.5 }, selected: { "opacity": 0.5 } },  //uses L.Icon for symbol
            line: { default: { "opacity": 0.5, "weight": 5, "color": "#ff3300" }, selected: { "opacity": 0.5, "color": "#3333cc", "fillColor#": "6699ff" } },
            polygon: { default: { "opacity": 0.5, "color": "#ff3300", "fillColor": "#ff9999" }, selected: { "opacity": 0.5, "color": "#3333cc", "fillColor#": "6699ff" } }
        }
    },
    options: {
        features: { title: "Manage GeoJSON Features" },
        events: { title: "Plugin Events" },
        tasks: {
            title: "Plugin Tasks",
            items: [
                {
                    title: "Define Image Offset",
                    component: "ImageOffset"
                },
                {
                    title: "Create Zonemap",
                    component: "Zonemap"
                },
                {
                    title: "Batch Zonemaps",
                    component: "Batch"
                }
            ]
        }
    },
    api: {
        props: {

        },
        methods: {

        },
        actions: [
            { label: 'Draw Polygon', action: 'DrawPolygon' },
            { label: 'Draw Polyline', action: 'DrawPolyline' }
        ],
        states: {
            // drawEnable: {label: "Enable draw tool", call: fce.enableControl('draw')},
            // drawDisable: {label: "Disable draw tool", call: fce.disableControl('draw')},
        }
    },
    data: {
        features: [
            {
                label: 'Line Test',
                feature: { "type": "Feature", "properties": {}, "geometry": { "type": "LineString", "coordinates": [[-77.44331359863281, 37.582541440297746], [-77.44331359863281, 37.57369848161073], [-77.4342155456543, 37.57383453508316]] } }
            },
            {
                label: 'Polygon Test',
                feature: { "type": "Feature", "properties": {}, "geometry": { "type": "Polygon", "coordinates": [[[-77.3763656616211, 37.57424269400915], [-77.3906135559082, 37.57505900514994], [-77.39198684692383, 37.562949471857564], [-77.3770523071289, 37.56226910259021], [-77.3763656616211, 37.57424269400915]]] } }
            }
        ],
        editing: [
            {
                "type": "FeatureCollection",
                "properties": {
                    "uid": 0,
                    "label": "Ashcake Road Farm"
                },
                "features": [
                    {
                        "type": "Feature",
                        "properties": {                        
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [-77.51163482666016, 37.75069733359449],
                                    [-77.50905990600586, 37.75090092768723],
                                    [-77.50897407531738, 37.750765198354316],
                                    [-77.50751495361327, 37.75293683780512],
                                    [-77.51146316528319, 37.75449766429222],
                                    [-77.51283645629883, 37.752665386359325],
                                    [-77.51317977905273, 37.752122480480075],
                                    [-77.51300811767578, 37.75137597839186],
                                    [-77.51163482666016, 37.75069733359449]
                                ]
                            ],
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {

                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [-77.52313613891602, 37.741806511935934],
                                    [-77.51867294311522, 37.739702420761745],
                                    [-77.5166130065918, 37.74228162102051],
                                    [-77.51721382141113, 37.743231830040905],
                                    [-77.51489639282227, 37.7434354446725],
                                    [-77.51446723937988, 37.74404628520656],
                                    [-77.51584053039551, 37.74676107103159],
                                    [-77.51678466796875, 37.74723614831276],
                                    [-77.51978874206543, 37.74730401624683],
                                    [-77.52064704895018, 37.744589250338024],
                                    [-77.52313613891602, 37.741806511935934]
                                ]
                            ],
                        }
                    }
                ]
            },

            {
                "type": "FeatureCollection",
                "properties": {
                    "uid": 1,
                    "label": "Elmont Road Farm"
                },
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                    [
                                    [-77.51901626586913, 37.72741604925924],
                                    [-77.50914573669432, 37.72361432799384],
                                    [-77.50845909118652, 37.72381799658007],
                                    [-77.5056266784668, 37.72592253917786],
                                    [-77.50399589538574, 37.72721239056709],
                                    [-77.50150680541992, 37.727619707391334],
                                    [-77.5004768371582, 37.7274839353655],
                                    [-77.50021934509277, 37.729656257909326],
                                    [-77.50133514404297, 37.729724141962045],
                                    [-77.5030517578125, 37.73271097867418],
                                    [-77.50785827636719, 37.73651223296987],
                                    [-77.5125789642334, 37.73345766902749],
                                    [-77.51867294311522, 37.728841644422836],
                                    [-77.51893043518066, 37.72829856378686],
                                    [-77.51901626586913, 37.72741604925924]
                                ],
                                [
                                    [-77.50914573669432, 37.73328796733567],
                                    [-77.51352310180664, 37.73009750313965],
                                    [-77.51176357269286, 37.726567467680496],
                                    [-77.50918865203857, 37.726058314066165],
                                    [-77.5074291229248, 37.727076617794516],
                                    [-77.50524044036865, 37.73036903735918],
                                    [-77.5070858001709, 37.73274491930814],
                                    [-77.50914573669432, 37.73328796733567]
                                ]
                            ]
                        }
    
                    },
                    {
                        "type": "Feature",
                        "properties": {
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                // Polygon 2
                                [
                                    [-77.52691268920897, 37.72992779374682],
                                    [-77.50845909118652, 37.7226638671866],
                                    [-77.50940322875977, 37.721509719810015],
                                    [-77.51172065734863, 37.72144182822783],
                                    [-77.51429557800293, 37.72361432799384],
                                    [-77.51798629760742, 37.724904219581596],
                                    [-77.52030372619629, 37.725311549100226],
                                    [-77.52193450927734, 37.726194088705576],
                                    [-77.5246810913086, 37.72700873131488],
                                    [-77.52596855163574, 37.7280949075216],
                                    [-77.52725601196289, 37.72890952922229],
                                    [-77.52691268920897, 37.72992779374682]
                                ]
                            ],
                        },
                    },
                    {
                        "type": "Feature",
                        "properties": {
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates":
                            [
                                [
                                    [-77.4964427947998, 37.72205285022732],
                                    [-77.49292373657227, 37.72191706799633],
                                    [-77.49000549316406, 37.73135334055843],
                                    [-77.49163627624512, 37.7321000446025],
                                    [-77.49506950378418, 37.73257521598276],
                                    [-77.49515533447266, 37.72985990988079],
                                    [-77.49575614929199, 37.727280276860036],
                                    [-77.4964427947998, 37.72205285022732]
                                ]
                            ]


                        },
                    },
                    {
                        "type": "Feature",
                        "properties": {
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates":
                            [
                                [
                                    [-77.4970006942749, 37.73074239528529],
                                    [-77.49558448791504, 37.72982596792443],
                                    [-77.49468326568602, 37.72985990988079],
                                    [-77.49412536621094, 37.7324394530424],
                                    [-77.4951982498169, 37.732541275271],
                                    [-77.49579906463623, 37.732541275271],
                                    [-77.49592781066895, 37.73359343010089],
                                    [-77.49742984771729, 37.73362737033036],
                                    [-77.4970006942749, 37.73142122305542],
                                    [-77.49691486358643, 37.73114969269413],
                                    [-77.4970006942749, 37.73074239528529]
                                ]
                            ]


                        }
                    }
                ]
            }

        ],

        ops: {
            strings: [
                { source: { name: "changeStr" }, message: "Change String 1", data: { value: "Lorem ipsum", actionStart: true } },
                { source: { name: "changeStr" }, message: "Change String 2", data: { value: "Lorem ipsum dolor sit amet" } },
                { source: { name: "changeStr" }, message: "Change String 3", data: { value: "Lorem ipsum dolor sit amet,\n consectetur adipiscing elit" } },
                { source: { name: "changeStr" }, message: "Change String 4", data: { value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", actionEnd: true } },
                { source: { name: "changeNum" }, message: "Change Num 1", data: { value: "1", actionStart: true } },
                { source: { name: "changeNum" }, message: "Change Num 2", data: { value: "2" } },
                { source: { name: "changeNum" }, message: "Change Num 3", data: { value: "3", actionEnd: true } },
                { source: { name: "changeStr" }, message: "(2) Change String 1", data: { value: "I stand amid the roar", actionStart: true } },
                { source: { name: "changeStr" }, message: "(2) Change String 2", data: { value: "I stand amid the roar\nOf a surf-tormented shore," } },
                { source: { name: "changeStr" }, message: "(2) Change String 3", data: { value: "I stand amid the roar\nOf a surf-tormented shore,\nAnd I hold within my hand", actionEnd: true } }
            ],
            actions: [
                ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
                ["Jack", "and", "Jill", "went", "up", "the", "hill"],
                ["And Jill came tumbling after"],
            ],
            features: [
                {
                    "type": "FeatureCollection",
                    "properties": { "assetId": 1 },
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 1,
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    // ring 1
                                    [
                                        [-77.3763656616211, 37.57424269400915],
                                        [-77.3906135559082, 37.57505900514994],
                                        [-77.39198684692383, 37.562949471857564],
                                        [-77.3770523071289, 37.56226910259021],
                                        [-77.3763656616211, 37.57424269400915]
                                    ]
                                ],
                            }
                        },
                    ]
                },
                {
                    "type": "FeatureCollection",
                    "properties": { "assetId": 1 },
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 1
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-77.35593795776367, 37.548388213333965],
                                        [-77.34254837036133, 37.53327960206065],
                                        [-77.32606887817383, 37.55614549958639],
                                        [-77.33413696289062, 37.56240517694074],
                                        [-77.35593795776367, 37.548388213333965]
                                    ],
                                ],
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 2
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-77.3763656616211, 37.57424269400915],
                                        [-77.3906135559082, 37.57505900514994],
                                        [-77.39198684692383, 37.562949471857564],
                                        [-77.3770523071289, 37.56226910259021],
                                        [-77.3763656616211, 37.57424269400915]
                                    ]
                                ],
                            }
                        }
                    ]
                },
                {
                    "type": "FeatureCollection",
                    "properties": { "assetId": 1 },
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 1
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-77.35593795776367, 37.548388213333965],
                                        [-77.34254837036133, 37.53327960206065],
                                        [-77.32606887817383, 37.55614549958639],
                                        [-77.33413696289062, 37.56240517694074],
                                        [-77.35593795776367, 37.548388213333965]
                                    ],
                                ],
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 2
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-77.3763656616211, 37.57424269400915],
                                        [-77.3906135559082, 37.57505900514994],
                                        [-77.39198684692383, 37.562949471857564],
                                        [-77.3770523071289, 37.56226910259021],
                                        [-77.3763656616211, 37.57424269400915]
                                    ]
                                ],
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "featureId": 3
                            },
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-77.35868453979492, 37.503461742362994],
                                        [-77.38409042358398, 37.484530070872005],
                                        [-77.32400894165038, 37.48943369807407],
                                        [-77.32229232788086, 37.49311120724059],
                                        [-77.35868453979492, 37.503461742362994]
                                    ]
                                ],
                            }
                        }
                    ]
                },
            ]
        }
    }
};