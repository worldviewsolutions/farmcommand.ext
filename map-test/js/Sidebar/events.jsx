"use strict";
//dependencies
var AccordionItem = require('./accordionItem.jsx');

//private

var Events = React.createClass({
    render: function () {
        var title = "Events";
        var content =  (<div>Content</div>);
        return (<AccordionItem title={title} content={content} isActive={this.props.isActive}/>);
    }
});

module.exports = Events;