"use strict";
//dependencies
var Clipboard = require('react-copy-to-clipboard');
var Container = RSify.Container;
var Title = RSify.Title;
var Button = RSify.Button;
var Header = RSify.Header;
var Segment = RSify.Segment;
var Icon = RSify.Icon;

var LogDrop = require('./Console/logDrop.jsx');
var LogView = require('./Console/logView.jsx');
var LogStore = require('./Console/logStore.jsx');
var logStore = new LogStore();
//internals

var testLogs = [
    {title:"String Test", value: "Literally dreamcatcher cred, mixtape selvage shoreditch selfies artisan kale chips. Tacos cliche pork belly, lo-fi scenester heirloom food truck DIY put a bird on it freegan sartorial blog cronut seitan. Blog kinfolk gastropub, pork belly street art twee small batch vinyl letterpress pabst kickstarter knausgaard. Health goth stumptown slow-carb, man bun tattooed wayfarers pour-over dreamcatcher four loko vegan. Polaroid organic single-origin coffee, knausgaard brunch kogi cold-pressed everyday carry humblebrag umami selvage kombucha hella scenester. Occupy cronut butcher, wayfarers before they sold out raw denim sartorial semiotics hella tacos. YOLO roof party fingerstache master cleanse, narwhal kitsch vegan small batch heirloom waistcoat."},
    {title:"Object Test", value:{foo:{bar:"baz"}}},
    {title: "Number Test", value: 3.141597},
    {title: "JSON Test", value: {
            "glossary": {
                "title": "example glossary",
                "GlossDiv": {
                    "title": "S",
                    "GlossList": {
                        "GlossEntry": {
                            "ID": "SGML",
                            "SortAs": "SGML",
                            "GlossTerm": "Standard Generalized Markup Language",
                            "Acronym": "SGML",
                            "Abbrev": "ISO 8879:1986",
                            "GlossDef": {
                                "para": "A meta-markup language, used to create markup languages such as DocBook.",
                                "GlossSeeAlso": ["GML", "XML"]
                            },
                            "GlossSee": "markup"
                        }
                    }
                }
            }
        }
    },
    {title: "GeoJSON Test", value: {"type":"Feature","properties":{},"geometry":{"type":"LineString","coordinates":[[-77.44331359863281,37.582541440297746],[-77.44331359863281,37.57369848161073],[-77.4342155456543,37.57383453508316]]}}}
];

var CopyButton = React.createClass({
    getInitialState: function () {
        return {
            visible: true,
            showCopied: false,
            classNames: ["grey"]
        };
    },
    componentWillMount: function() {
        logStore.subscribe(this.storeUpdated);
    },
    componentWillUnmount: function() {
        logStore.unsubscribe();
    },
    storeUpdated: function () {
        var logs = (logStore && logStore.getLogs)?logStore.getLogs(): [];
        this.setState({visible: logs.length > 0})
    },
    onCopy: function () {
        this.setState({visible: false, showCopied: true});
        setTimeout(function(){
            this.setState({visible: true, showCopied: false});
        }.bind(this), 1500);
        console.log('copied to clipboard');
    },
    render: function() {
        var btn = (<Clipboard text={this.props.copyText} onCopy={this.onCopy}>
            <Button className="grey vertical animated">
                <div className="hidden content"><Icon className="copy white" /></div>
                <div className="visible content">Copy</div>
            </Button>
        </Clipboard>);
        if (this.props.showCopy){
            return (
                <Container className="log-btn">
                    {this.state.visible ? btn :<noscript />}
                    {this.state.showCopied ?<Icon className="log-btn-response checkmark green"/>:<noscript />}
                </Container>
            );
        }
        else{
            return (<noscript />);
        }
    }
});

var ClearButton = React.createClass({
    getInitialState: function () {
        return {
            visible: false
        };
    },
    componentWillMount: function() {
        logStore.subscribe(this.storeUpdated);
    },
    componentWillUnmount: function() {
        logStore.unsubscribe();
    },
    clickHandler: function () {
        clear();
    },
    storeUpdated: function () {
        var logs = (logStore && logStore.getLogs)?logStore.getLogs(): [];
        this.setState({visible: logs.length > 0})
    },
    render: function() {
        if (this.state.visible){
            return (
                <Container className="log-btn">
                    <Button className="grey vertical animated" onClick={this.clickHandler}>
                        <div className="hidden content"><Icon className="remove red" /></div>
                        <div className="visible content">Clear</div>
                    </Button>
                </Container>
            );
        }
        else{
            return (<noscript />);
        }

    }
});

var add = function(log){
    if (log && log.value && log.title){
        var id = logStore.addLog(log);
        return id;
    }
    else{
        return ({error: 'Error - Log must have title and value properties'});
    }
};

var clear  = function(){logStore.clearLogs();};

var logConsole = React.createClass({
    getInitialState: function() {
        return {
            logs: logStore.getLogs(),
            selected: logStore.getSelected()
        };
    },
    componentWillMount: function() {
        logStore.subscribe(this.updateLogs);
    },
    componentDidMount: function() {
        // //load test data
        // _.forEach(testLogs, function(li){
        //     add(li);
        // });
    },
    componentWillUnmount: function() {
        logStore.unsubscribe();
    },
    updateLogs: function() {
        this.setState({
            logs: logStore.getLogs(),
            selected: logStore.getSelected()
        });
    },
    getLogText: function(){
        var text = (this.state.selected && this.state.selected.view)?this.state.selected.view:'';
        return text;
    },
    render: function() {
        var log= this.state.selected || null;
        var itemId = (log)?log.key || null : null;
        var text = (log && log.view)?log.view.toString():'';
        return (
            <Segment className="log-console">
                <Header>Testing Console</Header>                
                <LogDrop logs={this.state.logs || []} store={logStore} itemId={itemId}/>
                <LogView log={log}/>
                <CopyButton className="log-copy" showCopy={true} copyText={text}/>
                <ClearButton className="log-clear" />
            </Segment>
        );
    }
});
//Public methods
logConsole.add = add;
logConsole.clear = clear;
module.exports = logConsole;
