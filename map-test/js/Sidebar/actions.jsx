"use strict";
//dependencies
var AccordionItem = require('./accordionItem.jsx');
var ActionDropdown = require('./Actions/actionDropdown.jsx');

//private

var Actions = React.createClass({    
    render: function () {
        var title = "Actions";
        //optionally -- he he - use the options to pass in the global config if probs w/ window._Config...
        var content = (<ActionDropdown actions={_Config.api.actions} options={this.props.options}/>);
        return (<AccordionItem title={title} content={content} isActive={this.props.isActive}/>);
    }
});

module.exports = Actions;