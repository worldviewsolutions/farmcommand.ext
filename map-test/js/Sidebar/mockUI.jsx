"use strict";
//dependencies
var Container = RSify.Container;
var AccordionItem = require('./accordionItem.jsx');
var LayerList = require('./MockUI/layerList.jsx');
var PluginTasks = require('./MockUI/pluginTasks.jsx');
var CreateLayer = require('./MockUI/createLayer.jsx');
var Button = RSify.Button;
//private

var MockUI = React.createClass({
    getInitialState: function () {
        //define state vars here
        return {
            layers: _Config.data.editing,
            save: true,
            layerActive: false
        }
    },
    componentWillMount: function () {
        var layers = {};
        _Config.data.editing.map(function (feature) {
            var layer = L.FCE.loadData(feature)[0].data;
            layers[layer.options.storeId] = layer;
        }, this);
        this.setState({ layers: layers });
    },

    componentWillUnmount: function () {
        //before disable/unload - disable event handlers/watchers/subscriptions
    },

    componentDidMount() {
        L.FCE.on(L.FCE.events.layer.new.type, this.layerHandler);
    },

    layerHandler: function (e) {
        var layer = e.data.value;
        var layers = this.state.layers;
        layers[layer.options.storeId] = layer;
        this.setState({ layers: layers });
    },

    removeLayer: function () {
        var uiLayers = this.refs.layerList.refs;
        var layerToRemove = Object.keys(uiLayers).find(function (storeId) {
            return uiLayers[storeId].state.active === true;
        });
        var layers = this.state.layers;
        delete layers[layerToRemove];
        L.FCE.removeData(layerToRemove);
        uiLayers[layerToRemove].setState({active:false});
        this.setState({ layers: layers, layerActive: false });
    },


    saveLayer: function() {
        var uiLayers = this.refs.layerList.refs;
        // Find active layer in layer list
        var layerToSave = Object.keys(uiLayers).find(function (storeId) {
            return uiLayers[storeId].state.active === true;
        });
        var dataToSave = L.FCE.exportData(layerToSave);
        // If something is targeted
        if (dataToSave) {
            // On save true, reset data, log geojson, clear state
            if (this.state.save === true) {
                L.FCE.setMode('default');
                window.sidebar.AddLog({title: layerToSave.toString(), value: dataToSave});
                this.refs.layerList.refs[layerToSave].setState({active:false});
                this.setState({save: false, layerActive:false});
            }
            // Every other time mock as if data could not be saved
            else {
                window.sidebar.AddLog({title: 'Error', value: 'Data could not be saved'});
                this.setState({save:true})
            }
        }
        else {
            window.sidebar.AddLog({title: 'Error', value: 'No target data selected to save'});
        }

    },

    clearEdits: function() {
        L.FCE.clearEdits();
    },

    toggleButtons:function(state) {
        this.setState({layerActive: state});
    },

    render: function () {
        var title = "UI Mockup";
        var content = (
            <Container>
                <LayerList onActivate={this.activateHandler} toggleButtons={this.toggleButtons} layers={this.state.layers} ref="layerList" options={this.props.options} />
                <Container>
                    <CreateLayer/>
                    <Button onClick={this.removeLayer}>Remove Layer</Button>
                </Container>
                <Container>
                    <Button onClick={this.saveLayer} disabled={!(this.state.layerActive)}>Save Layer</Button>
                    <Button onClick={this.clearEdits} disabled={!(this.state.layerActive)}>Clear Edits</Button>
                </Container>
            </Container>
        );

        // Add back in when complete
        // <PluginTasks tasks={_Config.options.tasks} />
        return (<AccordionItem title={title} content={content} isActive={this.props.isActive}/>);
    }
});

module.exports = MockUI;