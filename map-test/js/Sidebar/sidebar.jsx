"use strict";
//dependencies
var Text = RSify.Text;
var Button = RSify.Button;
var Accordion = RSify.Accordion;
var Menu = RSify.Menu;
var Container = RSify.Container;
var Item = RSify.Item;
var Title = RSify.Title;
var Content = RSify.Content;
var Dropdown = RSify.Dropdown;
var Icon = RSify.Icon;
var Grid = RSify.Grid;
var Column = RSify.Column;
var Row = RSify.Row;

//internals
var MockUI = require('./mockUI.jsx');
// var Actions = require('./actions.jsx');
// var Events = require('./events.jsx');
var Modes = require('./modes.jsx');
var MockStack = require('./mockStack.jsx');
var Console = require('./console.jsx');

var sidebar = (function(options){
    var map = options.map || null;
    var fce = options.fce || null;

    if (!map || !fce) {return;}
    //Main Container
    var TestPanelOptions = React.createClass({
        getInitialState: function() {
            return {}
        },
        render: function() {
            return (
                <Accordion className="testoptions" init={true}>
                    <MockUI options={options} isActive={true} />
                    <Modes options={options}/>
                    <MockStack options={options}/>
                </Accordion>
            );
        }
    });
    
    var Render = function(data){
            var inputData = data|| _Config.options;
            ReactDOM.render(
                <Container>
                    <TestPanelOptions />
                    <Console />
                </Container>,
                document.getElementById('sidebar')
            );
    };
    
    return {
        Config: _Config,
        Render: Render,
        AddLog: Console.add,
        ClearLogs: Console.clear
    }
});
module.exports = sidebar;