"use strict";
//dependencies
var Item = RSify.Item;
var Title = RSify.Title;
var Content = RSify.Content;

var AccordionItem = React.createClass({
    getInitialState: function() {
        return {};
    },
    render: function() {
        var className = (this.props.isActive)?"active":"";
        return (
            <Item>
                <Title className={className}>
                    <i className="dropdown icon"/>
                    {this.props.title}
                </Title>
                <Content className={className}>
                    {this.props.content}
                </Content>
            </Item>
        );
    }
});

module.exports = AccordionItem;