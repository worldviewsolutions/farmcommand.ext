"use strict";
//dependencies
var React = require('react');
var RSify = require('react-semantify');
var Text = RSify.Text;
var Button = RSify.Button;
var Menu = RSify.Menu;
var Container = RSify.Container;
var Item = RSify.Item;
var Dropdown = RSify.Dropdown;
var Icon = RSify.Icon;

//internals
var map, fce;
var ActionDropdown = React.createClass({
    // These actions are completely self contained
    // TODO: Connect to feature component to hit an action against a layer, not just the map
    action: {},

    getInitialState: function() {
        map = this.props.options.map;
        fce = this.props.options.fce;

        return {
            active: false, // do not start actions till menu item set
            value: '' // value selected from menu
        }
    },

    // Item selected from menu
    _select: function(event) {
        if (this.state.active === false) this.state.active = true;
        this.setState({value: event.target.getAttribute('data-value')})
    },

    _startAction: function (e) {
        // Initiate action from list of actions (idx of menu items = idx of action list)
        this.action = new L.FCE.Actions[this.props.actions[this.state.value].action](map, {});
        this.action.addHooks();
    },

    _clear: function (e) {
        map.editTools.stopDrawing();
        var layers = map.editTools.featuresLayer._layers;
        Object.keys(layers).map(function(layerId) {
            var layer = layers[layerId];
            map.removeLayer(layer); // Remove layer from Map object
            delete layers[layerId]; // Must also remove layer from Editable
        });
        map.editTools.featuresLayer._layers = [];
    },

    _stopAction: function (e) {
        // This is where Editable Tracks all edited layes on a map, use until layer management figured out
        var layers = map.editTools.featuresLayer._layers;
        Object.keys(layers).map(function(layerId) {
            var layer = layers[layerId];
            var layerGeoJson = layer.toGeoJSON(); // Example of how to get GeoJson of layer
            // TODO: do something with layer like report or save
        });
        // Call Editable to stop drawing, could bind into BaseAction action inherits from (e.g. this.action.stopDrawing())
        map.editTools.stopDrawing();
    },

    _save: function (e) {
        // Do something with drawn layers
    },

    render: function () {
        return (
            <Container className="centered">
                <Dropdown className ='selection fluid' init={true}>
                    <Text>Actions</Text>
                    <Icon className="dropdown"/>
                    <Menu onClick={this._select} value={this.state.value}>
                        {this.props.actions.map(function (action, i) {
                            return <Item value={i} key={i}>{action.label}</Item>
                        })
                        }
                    </Menu>
                </Dropdown>
                <Container className="vertical buttons">
                    <Button onClick={this._startAction} disabled={!(this.state.active)} className="mini green">Start Action</Button>
                    <Button onClick={this._stopAction} disabled={!(this.state.active)} className="mini red">Stop Action</Button>
                    <Button onClick={this._clear} disabled={!(this.state.active)} className="mini yellow">Clear Drawing</Button>
                </Container>
            </Container>
        )
    }
});

module.exports = ActionDropdown;