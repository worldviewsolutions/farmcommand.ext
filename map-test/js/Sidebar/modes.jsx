"use strict";
//dependencies
var AccordionItem = require('./accordionItem.jsx');
var ModeDropdown = require('./Modes/modeDropdown.jsx');

//private

var Modes = React.createClass({
    getInitialState: function () {
        //define state vars here
        return null;
    },
    componentWillMount: function () {
        //before init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //before disable/unload - disable event handlers/watchers/subscriptions
    },

    render: function () {
        var title = "Modes";
        var modes = Object.keys(L.FCE.options.modes);
        var content = (<ModeDropdown modes={modes} options={this.props.options}/>);
        return (<AccordionItem title={title} content={content} isActive={this.props.isActive}/>);
    }
});

module.exports = Modes;
