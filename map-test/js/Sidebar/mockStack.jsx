"use strict";
//dependencies
var AccordionItem = require('./accordionItem.jsx');
var Button = RSify.Button;
var Container = RSify.Container;
var Item = RSify.Item;

var Op = React.createClass({
    getInitialState: function () {
        return {
            active: true,
            currentIndex: 0,
            words: ""
        }
    },
    componentWillMount: function() {
    },
    // restart action wherever undo/redo left off
    updateAction: function (action) {
        var actionLastWord = action.split(" ").pop();
        var lastWordIdx = this.props.actionList.indexOf(actionLastWord);
        var words = this.props.actionList.slice(0); // copy array so as not to mutate
        var newWords = words.splice(0, lastWordIdx + 1).join(" ");
        var active = (this.props.actionList.length !== lastWordIdx);
        this.setState({ currentIndex: lastWordIdx + 1, words: newWords, active: active})
    },
    updateWords: function (action) {
        this.setState({ words:action});
    },
    _activateButton: function () {
        var action = this.props.actionList[this.state.currentIndex];
        var word = this.state.currentIndex === 0 ? action : this.state.words.concat(" " + action);
        var indexIncrease = this.state.currentIndex + 1 ;
        var active = (this.props.actionList.length !== indexIncrease);
        this.setState({ currentIndex: indexIncrease, words:word, active: active});
        this.props.onClick(word);
    },
    reset: function () {
        this.setState({active: true, currentIndex:0, words: ""})
    },
    render: function () {
        var title = this.props.title;
        return (
            <Button className="mock-stack-add" onClick={this._activateButton} disabled={!(this.state.active) }>{title}</Button>
        )
    }
});

var Feature = React.createClass({
    getInitialState: function () {
        //define state vars here
        return {
            active:true,
            currentIndex: 0
        };
    },
    componentWillMount: function () {
        //before init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //before disable/unload - disable event handlers/watchers/subscriptions
    },

    _activateButton: function() {
        var feature = this.props.featureList[this.state.currentIndex];
        var idxIncrease = this.state.currentIndex + 1;
        var active = (this.props.featureList.length !== idxIncrease);
        this.setState({ currentIndex: idxIncrease, active: active});
        this.props.onClick(feature, this.state.currentIndex);

    },
    reset: function () {
        this.setState({active: true, currentIndex:0})
    },
    render: function () {
        var title = "Add Feature";
        return (<Button onClick={this._activateButton} disabled={!(this.state.active)}>{title}</Button>);
    }
});


var OpList = React.createClass({
    getInitialState: function () {
        //define state vars here
        return {
            index: -1, // received from L.FCE
            currentIndex: -1,
			data: null,
            wordTarget: null,
            featureTarget: null
        }
    },

    // send value to log console
    updateConsole: function(title, value){
        window.sidebar.AddLog({title: title, value: value});
    },
    
    // Do things for writing words on screen
    setWordTarget: function() {
        // handle fired updated event
        L.FCE.on(L.FCE.events.ops.updated.type, this.opsHandler);
        // set L.FCE.ops target
        var targetHandler = function(e){
            e.callback();
        };
        L.FCE.on(L.FCE.events.ops.reset.type, targetHandler);
        var target = new L.FCE._Base.Event({source: {name:"target"}, callback: this.opsHandler,
            type: "action", message: "edit target default", data:{value:"[Edits]"}});  //create event
        L.FCE.ops.load(target);
        var value = target.data.value.toString() + '\n(' + target.message + ')';
        this.updateConsole(target.source.name, value);
        this.setState({wordTarget: target});
    },
    // handles responses from L.FCE.controller.operations
    opsHandler: function (e) {
        if (!e.message || !e.data || !e.data.value) { return; }
        var value = e.data.value.toString() + '\n(' + e.message + ')';
        this.updateConsole(e.source.name, value);
        e.callback(true); //let ops know revert success
        if (+e.source.name === 0 ) {  this.refs[+e.source.name].updateWords(e.data.value); return;} // do not update in first demo
        if (e.source.name === 'target') { return; }
        this.refs[+e.source.name].updateAction(e.data.value);
    },
    opClick: function (opRef, isWorkflow, action) {
        if (this.state.wordTarget === null) {this.setWordTarget();}
        var e = new L.FCE._Base.Event({source: {name:opRef.toString(), workflow: {active:isWorkflow}, end:this.refs[+opRef].reset}, callback: this.opsHandler,
            type: "action", message: "add word", data: {value:action}});
        L.FCE.fire(L.FCE.events.ops.new.type, e);
        this.updateConsole(opRef.toString(), action); //todo: stack collapse works, but output invalid.  action should actually be built from op stack, not source data.
        this.setState({featureTarget: null});
    },

    // Do things for adding features to map
    setFeatureTarget: function() {
        // handle fired updated event
        L.FCE.on(L.FCE.events.ops.updated.type, this.featureHandler);
        // set L.FCE.ops target
        var targetHandler = function(e){
            e.callback();
        };
        L.FCE.on(L.FCE.events.ops.reset.type, targetHandler);
        var target = new L.FCE._Base.Event({source: {name:"target"}, callback: this.featureHandler,
            type: "action", message: "edit target default", data:{value:"[Feature]"}});  //create event
        L.FCE.ops.load(target);
        var value = target.data.value.toString() + '\n(' + target.message + ')';
        this.updateConsole(target.source.name, value);
        this.setState({featureTarget: target});
    },
    featureHandler: function (e) {
        if (!e.message || !e.data || !e.data.value) { return; }
        this.updateConsole(e.source.name, e.data.value);
        e.callback(true); //let ops know revert success
        if (e.data.value === "[Feature]") { this.state.data.clearLayers(); return}
        this.state.data.options.updateData(e.data.value); //todo: set style? e.g,  L.FCE.storage.styles.selected);
    },
    featureClick: function(geoJson, idx) {
        if (this.state.featureTarget === null) {this.setFeatureTarget();}
        var data = L.FCE.newData;
        if (!data) {
            data = L.FCE.createNewData();
        }
        var e = new L.FCE._Base.Event({source: {name:0, workflow: false}, callback: this.featureHandler,
            type: "action", message: "add feature", data: {value:geoJson}});
        L.FCE.fire(L.FCE.events.ops.new.type, e);
        data.options.updateData(geoJson);  //todo: set style? e.g,  L.FCE.storage.styles.selected);
        this.setState({data: data, wordTarget: null});
        this.updateConsole("Feature", geoJson);
    },



    render: function () {
        var features = this.props.features;
        return (
            <Container className="">
                {this.props.actions.map(function (action, i) {
                    var title = i;
                    // rough way to title the action options
                    switch(i){
                        case(0):
                            title = "Count to 10";
                            break;
                        case(1):
                            title = "Finish the sentence";
                            break;
                        case(2):
                            title = "Say a sentence";
                            break;
                    }
                    var isWorkflow = true; //todo: set isWorkflow value = false for single action buttons
                    var childClick = this.opClick.bind(this, i, isWorkflow);
                    return (<Op actionList={action} onClick={childClick} title={title} key={i} ref={i}/>)
                }, this) }
                <Feature className="mock-stack-add-feature" featureList={features} onClick={this.featureClick}/>
            </Container>);
    }
});

var MockStack = React.createClass({
    getInitialState: function () {
        //define state vars here
        return {};
    },
    componentWillMount: function () {
        //before init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //before disable/unload - disable event handlers/watchers/subscriptions
    },

    render: function () {
        var title = "Edit Ops Test";
        var actions = _Config.data.ops.actions;
        var features = _Config.data.ops.features;
        var content = (<OpList actions={actions} features={features} />);
        return (<AccordionItem title={title} content={content} isActive={this.props.isActive}/>);
    }
});

module.exports = MockStack;


