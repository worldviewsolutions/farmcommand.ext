"use strict";
//dependencies
var Button = RSify.Button;

var AddOp = React.createClass({
    getInitialState: function () {
        //define state vars here
        return null;
    },
    componentWillMount: function () {
        //after init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //on disable/unload - disable event handlers/watchers/subscriptions
    },
//     clickHandler: function () {
//         //sendNext();
//         alert('clicked'); //onClick={this.clickHandler}
//     },
//     render: function() {
//         var btnTitle = "+";
//         return (<Button className="mock-stack-add">{btnTitle}</Button>);
//     }
    render: function () {
        var title = "+";
        return ( <Button>{title}</Button>);
    }
});

module.exports = AddOp;