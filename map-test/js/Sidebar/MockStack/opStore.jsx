// var OpStore = function(ops, callback){   
//     this.items = ops.map(function(item){
//         item.callback = callback;
//         return new L.FCE._Base.Event(item);
//     });
// };
var OpStore = function(){}();
OpStore.prototype = {
    position: 0,
    items: [],
    constructor: OpStore,
    getItem: function(index){ //get item at index
        index = (index)? index: this.position;
        return this.items[index];
    },
    nextItem: function(){ //inc position and get that item from queue
        this.move();
        return this.getItem();
    },
    setPosition: function(i){ //set current position
        this.position = i;
    },
    move: function(){ //increment current position
        this.position = this.position + 1;
    }
};

module.exports = OpStore;