"use strict";
//dependencies
var React = require('react');
var RSify = require('react-semantify');
var Text = RSify.Text;
var Button = RSify.Button;
var Menu = RSify.Menu;
var Container = RSify.Container;
var Item = RSify.Item;
var Dropdown = RSify.Dropdown;
var Icon = RSify.Icon;

//internals
var map, fce;
var ModeDropdown = React.createClass({
    // These actions are completely self contained
    // TODO: Connect to feature component to hit an action against a layer, not just the map
    action: {},

    getInitialState: function() {
        map = this.props.options.map;   
        fce = this.props.options.fce;
        return {
            active: false, // do not start actions till menu item set
            value: '' // value selected from menu
        }
    },

    // Item selected from menu
    _select: function(event) {
        var eventValue = event.target.getAttribute('data-value');
        this.setState({value: eventValue});
        fce.setMode(eventValue);
    },

    render: function () {
        return (
            <Container className="centered">
                <Dropdown className ='selection fluid' init={true}>
                    <Text>Modes</Text>
                    <Icon className="dropdown"/>
                    <Menu onClick={this._select} value={this.state.value}>
                        {this.props.modes.map(function (mode, i) {
                            return <Item value={mode} key={i}>{mode}</Item>
                        })
                        }
                    </Menu>
                </Dropdown>
            </Container>
        )
    }
});

module.exports = ModeDropdown;