"use strict";
//dependencies

var Dropdown = RSify.Dropdown;
var Text = RSify.Text;
var Icon = RSify.Icon;
var Menu = RSify.Menu;
var Item = RSify.Item;

//internals

var logDrop = React.createClass({
    getInitialState: function () {
        return {
            value: this.props.itemId
        }
    },
    componentWillMount: function() {
        this.props.store.subscribe(this._update);
    },
    _update: function(){
        var log = this.props.store.getSelected();
        this.setState({value: log.key});
    },
    _select: function(event) {
        var id = event.target.getAttribute('data-value');
        this.setState({value: id});
        var log = this.props.store.getLog(id);
        this.props.store.selectLog(log);
    },
    render: function () {
        var itemsList = (this.props.logs.length > 0)?(
            <Dropdown className ='selection fluid' init={true}>
                <Text>Logs</Text>
                <Icon className="dropdown"/>
                <Menu onClick={this._select} value={this.state.value}>
                    {this.props.logs.map(function(log){
                        var label = '[' + log.key + ']: ' + log.title;
                        return (<Item key={log.key} value={log.key} selected={this.state.value === log.key}  >{label}</Item>);
                    }, this)
                    }
                </Menu>
            </Dropdown>
        ):(<noscript />);
        return (
            <div>
                {itemsList}
            </div>
        )
    }
});
module.exports = logDrop;

