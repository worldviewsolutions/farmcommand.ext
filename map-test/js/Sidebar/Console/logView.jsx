"use strict";
//dependencies
var Header = RSify.Header;
var Container = RSify.Container;
var Button = RSify.Button;
var Icon = RSify.Icon;
var Segment = RSify.Segment;

//internals

var logView = React.createClass({
    getInitialState: function () {
        return null;
    },
    render: function(){
        var title = "";
        var content = (<div><i>No Logs Created</i></div>);
        if (this.props.log){
            title = this.props.log.title || 'No Data';
            var view = this.props.log.view || <noscript />;
            var formatted = (<pre className={this.props.log.format}>{view}</pre>);
            content = (this.props.log.format)?formatted:view;
        }
        return (
            <Container>
                <Segment>
                    <Header>{title}</Header>
                    <div className="log-content">{content}</div>
                </Segment>
            </Container>
        );
    }
});

module.exports = logView;