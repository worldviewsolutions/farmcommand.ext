"use strict";
//dependencies

//internals
var emitter = new EventEmitter();
var selected = null;
var logs = [];
var pubsubText = 'logs-updated';

var log = function(id, args){
    var format = false;
    var _view = function(obj){
        var v = "";

        if ( _.isString(obj)){v = obj;}
        if ( _.isDate(obj)){v = new Date(obj).toDateString();}
        if (_.isObject(obj)) {
            v = JSON.stringify(obj, null, '   ');
            format = "object";
        }
        else if (_.isNumber(obj) || _.isInteger(obj)){v = obj;}
        return v;
        //todo: improve preproc value output by object type (string, obj, json, etc)
    };
    return {key: id, title: args.title, selected: false, value: args.value, view: _view(args.value), format: format};
};

var LogStore = function(){};
LogStore.prototype = {
    constructor: LogStore,
    subscribe: function(callback) {
        emitter.on(pubsubText, function(){callback()}.bind(this));
    },
    unsubscribe: function(callback) {
        emitter.off(pubsubText, callback);
    },
    getLogs: function() {
        return logs.concat();
    },
    getLog: function(id){
        var log = logs[id];
        var filtered = _.filter(logs, function(item){
            return item.key === id;
        });
        return filtered[0] || log;
    },
    clearLogs: function(){
        logs = [];
        this.selectLog({});
        return true;
    },
    addLog: function(value) {
        var newLog = log(logs.length, value);
        logs.push(newLog);
        this.selectLog(newLog);
        return newLog.key;
    },    
    selectLog: function(log){
        selected = log;
        emitter.emit(pubsubText);
        return true;
    },
    getSelected: function(){
        return selected;
    }
};

module.exports = LogStore;
