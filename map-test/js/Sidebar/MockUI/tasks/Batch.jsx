"use strict";
//dependencies
var Button = RSify.Button;

var Batch = React.createClass({
    getInitialState: function () {
        //define state vars here
        return null;
    },
    componentWillMount: function () {
        //after init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //on disable/unload - disable event handlers/watchers/subscriptions
    },

    render: function () {
        var title = "Batch Content...";
        return ( <Button>{title}</Button>);
    }
});

module.exports = Batch;