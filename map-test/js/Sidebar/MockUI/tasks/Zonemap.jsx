"use strict";
//dependencies
var Button = RSify.Button;

var Zonemap = React.createClass({
    getInitialState: function () {
        //define state vars here
        return null;
    },
    componentWillMount: function () {
        //after init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //on disable/unload - disable event handlers/watchers/subscriptions
    },

    render: function () {
        var title = "Zonemap Content...";
        return ( <Button>{title}</Button>);
    }
});

module.exports = Zonemap;