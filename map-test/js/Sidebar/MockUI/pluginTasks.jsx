"use strict";
//dependencies
var Container = RSify.Container;
var Header = RSify.Header;
var Segment = RSify.Segment;

// var Batch = require('./tasks/Batch.jsx');
// var ImageOffset = require('./tasks/ImageOffset.jsx');
// var Zonemap = require('./tasks/Zonemap.jsx');


// Feel free to change, idea is that each workflow has a different component with different specs
// Use this factory method to create each component by config string
var WorkflowFactory = React.createClass({
    components: {
        "Batch": require('./tasks/Batch.jsx'),
        "Zonemap":  require('./tasks/Zonemap.jsx'),
        "ImageOffset": require('./tasks/ImageOffset.jsx')
    },
    render: function() {
        return React.createElement(this.components[this.props.component], null);
    }
});

var TaskPanel = React.createClass({
    getInitialState: function () {
        //define state vars here
        return null;
    },
    componentWillMount: function () {
        //after init complete - enable event handlers/watchers/subscriptions
    },

    componentWillUnmount: function () {
        //on disable/unload - disable event handlers/watchers/subscriptions
    },

    render: function () {
        return (
            <Container>
                <Header className="top attached">{this.props.task.title}</Header>
                <Segment className="attached">
                    <WorkflowFactory component={this.props.task.component}/>
                </Segment>
            </Container>
        )
    }
});

var PluginTasks = React.createClass({
    render: function () {
        return (
            <Container>
                <Header className="block">{this.props.tasks.title}</Header>
                {this.props.tasks.items.map(function (item, i) {
                    return <TaskPanel key={i} task={item} />
                })
                }
            </Container>
        )
    }
});

module.exports = PluginTasks;