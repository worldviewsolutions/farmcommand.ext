"use strict";
//dependencies
var List = RSify.List;
var Layer = require('./layer.jsx');

// Layers component servs as the middleman between layer and map, adding and removing layers
// If a layer is selected it is added to map and removed when unselected
// Most likely this will be migrated to datastore as medium to communicate with workflow tasks, actions, etc.

var fce;
var map;
var LayerList = React.createClass({
    // Idx of map layers
    layers: {
        // 1: Leaflet Layer (converted from geoJson on mount),
        // 2: Leaflet Layer
    },
    getInitialState: function () {
        //define state vars here
        fce = this.props.options.fce;
        map = this.props.options.map;
        return {
            activeStoreId: -1
        };
    },
    _activateLayer: function (storeId, state) {
        var targetLayer = this.refs[storeId];
        var activeLayer = this.state.activeStoreId !== -1 ? this.refs[this.state.activeStoreId] : null;
        // If a layer in the layer list is active
        if ((activeLayer !== null && activeLayer !== undefined) &&  activeLayer.state.active === true) {
            var hasEdits = L.FCE.hasEdits();
            // Do nothing if data exists in stack
            if (hasEdits === true) {
                window.sidebar.AddLog({title: 'Error', value: 'Save or clear edits before changing layers'});
                return;
            }
            // Otherwise changing to another layer, set current active layer to false
            activeLayer.setState({active: false});
        }
        // If state of target layer is false, edit and activate buttons
        if (state === false) {
            fce.setMode('target', storeId);   
            this.props.toggleButtons(true); // toggle save/reset on parent
        }
        // Otherwise target layer is being turned off, turn buttons off
        else {
            fce.setMode('default');
            this.props.toggleButtons(false);
        }
        targetLayer.toggleState();
        this.setState({activeStoreId: storeId});
    },

    render: function () {
        return (
            <List className="celled selection">
                {Object.keys(this.props.layers).map(function (storeId, i) {
                    var layer = this.props.layers[storeId];
                    var key = layer.options.storeId;
                    var boundClick = this._activateLayer.bind(this, key);
                    return (<Layer ref={key} onClick={boundClick} key={i} label={layer.options.storeId} />)

                }, this)
                }
            </List>
        )
    }
});

module.exports = LayerList;