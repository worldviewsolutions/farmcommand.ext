"use strict";
//dependencies
var Container = RSify.Container;
var Button = RSify.Button;

// Layer just manages it's state of active/inactive (selected/unselected)
var CreateLayer = React.createClass({
    getInitialState: function () {
        return {
            active: true,
            layerId: 999
        }
    },    
    componentDidMount: function () {
        //after init complete - enable event handlers/watchers/subscriptions
    },

    onLayerFinish: function() {
        this.setState({active: true});
    },

    createLayer: function() {
        L.FCE.addData(this.state.layerId);
        var layerIdIncrement = this.state.layerId + 1;
        this.setState({active: true, layerId: layerIdIncrement});
    },

    render: function () {
        return (
            <Container className="new-data">
                <Button onClick={this.createLayer} disabled={!(this.state.active)}>Create Layer</Button>
            </Container>
        )
    }
});

module.exports = CreateLayer;