"use strict";
//dependencies
var Item = RSify.Item;

// Layer just manages it's state of active/inactive (selected/unselected)
var Layer = React.createClass({
    getInitialState: function () {
        return {
            active: false
        }
    },

    toggleState: function() {
        this.state.active ? this.setState({active: false}) : this.setState({active: true})
    },
    _activateLayer: function() {
        this.props.onClick(this.state.active);
    },
    render: function () {
        return (
            <Item onClick={this._activateLayer} className={this.state.active ? 'layer-active' : ''}>{this.props.label}</Item>
        )
    }
});

module.exports = Layer;