"use strict";
var fs = require("fs");
var path = require('path');
var del = require('del');
var source = require('vinyl-source-stream');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')(); //auto-requires all "gulp-*" dependencies.  Access through $.*
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var karma = require('karma');
var exec = require('child_process').exec; //used to call cmd procs


// set variable via $ gulp --type production
var env = $.util.env.type || 'development';
//var isProduction = env === 'production';
var config = require('./gulpfile.config.js').getConfig(env);
var dev = config.paths.dev;
var target = dev;
var src = config.paths.src;
var srcAll = [src.js.root, src.html];
var specOpts = {}; //todo: set defaults

// https://github.com/ai/autoprefixer
var autoprefixerBrowsers = [
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'chrome >= 34',
    'safari >= 6',
    'opera >= 23',
    'ios >= 6',
    'android >= 4.4',
    'bb >= 10'
];

// convert jsx ==> js, move *.js
gulp.task('main-transform', function(){
    return browserify({
        entries: [src.js.root + src.js.entry],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(target.js.out))
        .pipe(gulp.dest(target.js.root));
});

// convert jsx ==> js, move *.js
gulp.task('scripts-transform', function(){
    return browserify({
        entries: [src.js.root + src.js.entry],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    })
        .bundle().on('error', function (err) {
            console.log(err.message);
            return;
        })
        .pipe(source(target.js.out))
        .pipe(gulp.dest(target.js.root));
});

// copy html
gulp.task('html-copy', function () {
    return gulp
        .src(src.html)
        .pipe($.rename(target.html))
        .pipe(gulp.dest(target.dir));
});

gulp.task('html-replace', function () {
    return gulp
        .src(target.html)
        .pipe($.htmlReplace({
            'js': target.js.outTest
        }))
        .pipe(gulp.dest(target.dir));
});

gulp.task('html', ['html-copy'], function () {
    gulp.start(['html-replace']);
});

// convert less to css
gulp.task('css-less', function () {
    return gulp
        .src(src.css + '**/*.less')
        .pipe($.less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest(target.css));
});

// watch css, html and scripts file changes
gulp.task('watch', function() {
    gulp.watch(src.css + "**/*.less", ['css-less']);
    gulp.watch(src.html, ['html-replace']);
    var watcher  = watchify(browserify({
        entries: [src.js.root + src.js.entry, src.js.libs],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    }));

    return watcher.on('update', function () {
        watcher
            .bundle()
            .pipe(source(target.js.out))
            .pipe(gulp.dest(target.js.root)) //target.dir
        console.log('Updated');
    })
        .bundle().on('error', function (err) {
            console.log(err.message);
            return;
        })
        .pipe(source(target.js.out))
        .pipe(gulp.dest(target.js.root)); //target.dir
});

// run karma tests
gulp.task('test-karma', function(){
    var done = function(result){
        if (result) {console.log(result.toString());}
        console.log("Karma tests finished.");
    };
    var filePath = path.join(__dirname, '../', 'karma.conf.js');
    new karma.Server({
        configFile: filePath,
        singleRun: true
    }, function(exitCode){
        done(exitCode);
    }).start();
});

// add livereload on the given port
// not tested
gulp.task('test-serve', function () {
    var serv = config.development;
    $.connect.server({
        root: target.dir,
        port: serv.port,
        base: serv.base,
        livereload: serv.liveReload
    });
});

// remove bundels
gulp.task('test-clean-dev', function (cb) {
    //delete build files
    var files = [target.js.root + target.js.out, target.js.root + target.js.outTest, target.dir + target.html, target.css + '*.css'];
    return del(files);
});

// by default build project and then watch files in order to trigger livereload
gulp.task('default', ['watch']);

// waits until clean is finished then assembles the project for dev testing
gulp.task('build-dev', ['test-clean-dev'], function () {
    target = config.paths.dev;  //'scripts-transform',
    gulp.start(['html-copy', 'main-transform', 'css-less']);
});

// clean and copy files to remote directory
// waits until clean is finished then assembles the project for dev testing
gulp.task('publish-dev', function () {
    var publish = function(){
        var delay = config.paths.staging.delay * 1000;
        console.log("Delay - waiting " + delay.toString() + " ms for karma, ./main.js build completion");
        setTimeout(function(){
            console.log("Publishing FCE Tests to " + config.paths.staging.hostname +" Server");
            var dest = "\\\\" +  config.paths.staging.hostname +  config.paths.staging.dir;
            var execTask = 'publish.bat ' + dest;
            exec(execTask, function (err, stdout, stderr) {
                console.log(stdout);
                console.log(stderr);
            });
        },delay);
    };
    var build = function(){
        console.log("Building FCE for Dev");
        gulp.start(['build-dev'], publish);
    };
    console.log("Running Karma Tests");
    gulp.start(['test-karma'], build);
});