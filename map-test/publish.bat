@echo off
set target=%1
echo Cleaning dev build
del /s /q "%target%\dev\*.*"
echo Syncing dev build
xcopy %~dp0..\dev\* %target%\dev\* /s /d /y /c /exclude:publish.exclude.txt
echo Cleaning karma tests
del /s /q "%target%\test\*.*"
echo Syncing karma tests
xcopy %~dp0..\test\* %target%\test\* /s /d /y /c /exclude:publish.exclude.txt
echo Cleaning map-test build
del /s /q "%target%\map-test\*.*"
echo Syncing map-test build
xcopy "%~dp0*" "%target%\map-test\*" /s /d /y /c /exclude:publish.exclude.txt
rem echo Syncing semantic files
rem xcopy %~dp0..\semantic\* %target%\semantic\* /s /d /y /c
rem echo Syncing node_modules files
rem xcopy %~dp0..\node_modules\* %target%\node_modules\* /s /d /y /c
echo Syncing project json files
xcopy %~dp0..\karma.conf.js %target%\karma.conf.js* /Y /C
xcopy %~dp0..\package.json %target%\package.json* /Y /C
xcopy %~dp0..\semantic.json %target%\semantic.json* /Y /C