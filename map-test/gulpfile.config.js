//Config Options
var paths = {
    src: {
        dir: 'map-test/',
        js: {root:'js/', dir:'', entry: 'main.js'},
        libs: 'libs/',
        html: 'source.html',
        css: 'style/'
    },
    dev: {
        dir: './',
        js: {root:'./', dir: './', out:'main.js'},
        html: 'index.html',
        css: 'style/'
    },
    staging: {
        dir: '\\c$\\applications\\FC_Demo',
        hostname: 'vm101',
        delay: 5, //delay copy execution for build & test tasks, in seconds
        paths:{
            test:  ['map-test', 'test','karma.conf.js'],
            dev: ['dev','package.json'],
            dependencies: ['semantic','semantic.json'] //['../node_modules']
        }
    }
};

var run = {
    default: {

        js: {
            uglify: false
        },
        css: {
            compress: false
        }
    }
};

var plugin = {
    default: {
        js: {
            uglify: {
                mangle: true
            }
        }
    }
};

module.exports.getConfig = function(env) {
    var _ = require('lodash');
    var isDev = env === 'development';
    var runOpts = _.merge( {}, run.default, run[ env ] );
    var pluginOpts = _.merge( {}, plugin.default, plugin[ env ] );
    module.exports.paths = paths;
    module.exports.run = runOpts;
    module.exports.plugin = pluginOpts;
    var config = module.exports;
    config.devtool = (isDev)? 'eval' : null;
    return config;
};