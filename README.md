# farmcommand.leaflet   

Leaflet plugin supporting geometry manipulation and api for server-side analysis.
This project is composed of the following sub projects with divergent builds/stacks:
      
  1. Plugin Source code (./src) 
  
    > - builds to dev (./dev) and (./dist), see builds below
    > - owner of ./package.json, ./gulpfile*
  
  2. TDD (./test)

    > - see details in ./test/README.md
    
  3. UI Integration (./map-test)
  
    > - build outputs to ./map-test/
    > - run karma tests, render html of results
    > - publish uses batch file to move 
    > - owner of ./map-test/package.json, ./map-test/gulpfile*
    > - see details in ./map-test/README.md
  
# Installation

- Run:  `npm install`

# Builds

#### Development 
- Run:  `gulp build-dev`
    > - builds dev directory.  files are browsified, concatenated, and source-mapped. No minification.

#### Distribution
- Run: `gulp build-dist`
    > - builds dist directory.  files are browsified, uglified, and source-mapped.  Only images have sub-dir: css and js root files only.
      
# History
TODO: Write history

# Credits
TODO: Write credits

# License
TODO: Write license