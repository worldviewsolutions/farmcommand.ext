// Karma configuration
// Generated on Tue Jul 12 2016 15:12:43 GMT-0400 (Eastern Daylight Time)
"use strict";
module.exports = function (config) {
  config.set({
    plugins: [
      "karma-browserify",
      "karma-chrome-launcher",
      "karma-phantomjs-launcher",
      "karma-firefox-launcher",
      "karma-mocha",
      'karma-htmlfile-reporter'
    ],
    frameworks: ["browserify", "mocha"],
    files: [
      "test/globals.js",
      "test/index.js",
      "test/libs/leaflet/leaflet.css",
      "test/test.css",
      "dist/fce.css",
      "dev/libs/leaflet.pip/leaflet-pip.js"
    ],
    exclude: [
    ],

    // Push all commonJS files through browserify before running 
    preprocessors: {
      "test/*.js": ["browserify"]
    },
    client: {
      mocha: {
        reporter: "html"
      }
    },
    browserify: {
      debug: true
    },
    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'html'],
    htmlReporter: {
      outputFile: 'test/index.html'
    },
    port: 9876,// web server port
    colors: true,
    logLevel: config.LOG_INFO, // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    autoWatch: true,
    usePolling: true,  //for .idea
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  });
};
