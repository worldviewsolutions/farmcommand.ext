FCE Code Architecture Notes
===========================

# FCE
    ## Overview
        - loader
    ## Dependencies
    ## Properties
    ## Methods
# Controller
    ## Overview
    - AP Interfaces
    - NO UI    
    ## Dependencies
    - leaflet.class
    ## Properties
    ## Methods
    - Init (map, options)
        + Override Defaults from options arg
        + Sets Toolbar Defaults
        + Inits Toolbars 
    - LoadToolbar
        + Public method
    - UnloadToolbar
        + Destroys toolbar    
    - Call 
        + Ajax Wrapper
     
# Toolbar (Control Container Base Class)
    ## Overview
    - Leaflet.Toolbar.Control wrapper class
    - NO UI
    - Manages Action states via modes (from config definitions)
    ## Dependencies
    - Leaflet.Toolbar.Control
    ## Properties
    - mode
    - activeMode
    -.workflow
    ## Methods
    - Load
        + Override Defaults from options arg (e.g., initial mode, actions to exclude)
    - Unload
    - setMode
    - createControl
    - destroyControl
    - eventHandler
# Control (BaseControl class extends Leaflet.Toolbar.Control)
    Overview
        - UI container for Actions
        - Wrapper used in Toolbar Base Class's createControl method
    Dependencies
    - Leaflet.Toolbar.Control
    Properties
    - isVisible
    Methods
    - init
        + load actions 
    - _destroy
        + remove from ui
# Action (BaseAction class extends Leaflet.ToolbarAction)
    ## Overview
    - Actions are a control's UI + behavior definition.  
    - They are either a:
        + Tool (map interaction)
        + Menu (select opt/state)
        + Command (onclick perform a Func)
    - Actions have no load/unload method, as they are initialized only when toolbar.load is called
    - Actions do not explicitly handle logic - addHook handler calls parent toolbar onClick Method
    ## Dependencies
    - L.ToolbarAction
    ## Properties
    - isActive
    - isEnabled    
    ## Methods
    - init
        + set handler callback
    - enable
    - disable
    - addHook
    - _setState(enable)

# Menu (BaseMenu class extends Leaflet.Toolbar)
    ## Overview
    - Wrapper class container for Sub Actions
    - Submenu onClick handler calls parent Action addHook
    ## Dependencies
    - Leaflet.Toolbar
    ## Properties
    - isVisible
    - selectedAction 
    ## Methods
    - onShow
    - onHide
    - onClick
# Menu Action
    ## Overview
    - Subactions 
    ## Dependencies
    - L.ToolbarAction
    ## Properties
    - isEnabled
    - parentAction?
    ## Methods
    - enable
    - disable
    - onClick handler
    - _setState(enable)



