
/**
 * farmcommand.leaflet gulp build file
 */
"use strict";
var _ = require('lodash');
var path = require('path');
var del = require('del');
var source = require('vinyl-source-stream');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')(); //auto-requires all "gulp-*" dependencies.  Access through $.*
var browserify = require('browserify');
var watchify = require('watchify');
var karma = require('karma');

// set variable via $ gulp --type production
var env = $.util.env.type || 'development';
//var isProduction = env === 'production';
var gulpConfig = require('./gulpfile.config.js').getConfig(env);

var target;
var dev = target = gulpConfig.paths.dev;
var dist = gulpConfig.paths.dist;
var src = gulpConfig.paths.src;
var srcAll = [src.js.root, src.html];
var specOpts = {}; //todo: set defaults

// https://github.com/ai/autoprefixer
var autoprefixerBrowsers = [
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'chrome >= 34',
    'safari >= 6',
    'opera >= 23',
    'ios >= 6',
    'android >= 4.4',
    'bb >= 10'
];

var buildConfig = function(){
    var parseArray = function(arr, indent){
        indent += 6;
        var v;
        var items = _.forEach(arr, function(item) {
            var result = "";
            if (_.isObject(item)) {
                result = parseObj(item, indent);
            }
            if (_.isString(item)) {
                result = item;
            }
            return result;
        });
        v = _.padEnd("[\n", indent);
        var delim = _.padEnd(",\n", indent);
        var list = items.map(function(i){
            if (_.isString(i) && i.indexOf("L.") === -1) {return "'" + i + "'";}
            return i;
        }).join(delim);
        v += list + '\n';
        indent += 6;
        v += _.pad("]", indent);
        return v;
    };
    var parseObj = function(obj, indent){
        var o = '';
        indent += 4;
        var pad = _.padEnd("", indent);
        _.forOwn(obj, function(value, key){
            var v = null;
            if ( _.isObject(value)) {v= parseObj(value, indent);}
            if ( _.isArray(value)) {v= parseArray(value, indent);}
            else{
                if ( _.isString(value)){v = '\'' + value + '\'';}
                if ( _.isDate(value)){v = new Date(value).toDateString();}
                //catch all other types explicitly
                else if (_.isNumber(value) || _.isInteger(value) || _.isBoolean(value)){v = value;}
            }
            if (v || v == false) {
                //console.log(key + ': ' + v);
                o += '\n' + pad + key + ': ' + v + ',';
            }
        });
        o = o.substring( 0, o.length - 1 );// Remove the last comma
        indent -= 4;
        pad = _.padEnd("", indent);
        o ='{' + o + '\n' + pad +'}';
        return o;
    };
    gulpConfig.configs = _.omit(gulpConfig.configs, '0');
    var parsed = parseObj(gulpConfig.configs, 0);
    //var result = gulpConfig.configClass + ' = ' + parsed + ';';
    var result = gulpConfig.configClass.pref + parsed + gulpConfig.configClass.suf;
    return result;
};

// generate config json file
gulp.task( 'config', function() {
    var codeString = buildConfig();
    return $.file( src.config, codeString, { src: true } )
        .pipe( gulp.dest( src.js.root) );
});

// move scripts
gulp.task('scripts-copy', function() {
    return gulp
        .src(src.js.root + '**/*.js', src.js.root + '**/*.jsx')
        .pipe(gulp.dest(target.js.root));
});

// convert jsx ==> js, move *.js
gulp.task('scripts-transform', function(){
    return browserify({
        entries: [src.js.root + src.js.entry],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(target.js.out))
        .pipe(gulp.dest(target.js.root)); //target.dir
});

// concat/min and move scripts
gulp.task('scripts-dist', function() {
    return browserify({
        entries: [src.js.root + src.js.entry], //path 
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(dist.js.out)) //file
        .pipe($.streamify($.uglify(dist.js.out))) //file
        .pipe(gulp.dest(dist.js.root)); //dir

});


// convert less to css
gulp.task('css-less', function () {
    return gulp
        .src(src.css + '**/*.less')
        .pipe($.less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest(target.css));
});

// copy images
gulp.task('images-copy', function(cb) {
    return gulp
        .src(src.images +  '**/*.{png,jpg,jpeg,gif}')
        .pipe($.size({ title : 'images' }))
        .pipe(gulp.dest(target.images));
});

// copy libs to out
gulp.task('libs-copy', function(){
    return gulp
        .src(src.libs + '**/*.*')
        .pipe(gulp.dest(target.libs));
});

// copy misc files out
gulp.task('files-copy', function(){
    return gulp
        .src(target.files)
        .pipe(gulp.dest(target.dir));
});

// zip directory
gulp.task('dir-zip', function(){
    return gulp
        .src(target.dir + '**')
        .pipe($.zip('fce-' + target.name + '.zip'))
        .pipe(gulp.dest(target.dir));
});

// run karma tests
gulp.task('test-karma', function(){
    var done = function(result){
        if (result) {console.log(result.toString());}
        console.log("Karma tests finished.");
    };
    var filePath = path.join(__dirname, '', 'karma.conf.js');
    new karma.Server({
        configFile: filePath,
        singleRun: true
    }, function(exitCode){
        done(exitCode);
    }).start();
});

// watch css, html and scripts file changes
gulp.task('watch', function() {
    gulp.watch(src.css + "**/*.less", ['css-less']);
    gulp.watch(src.html, ['html-replace']);
    var watcher  = watchify(browserify({
        entries: [src.js.root + src.js.entry, src.js.libs],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    }));

    return watcher.on('update', function () {
        watcher
            .bundle()
            .pipe(source(src.js.entry))
            .pipe(gulp.dest(target.js.root));
        console.log('Updated');
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(src.js.entry))
        .pipe(gulp.dest(target.js.root)); //target.dir
});

// add livereload on the given port
// TODO: not tested
gulp.task('_serve', function() {
    var serv = gulpConfig.development;
    $.connect.server({
        root: target.dir,
        port: serv.port,
        base: serv.base,
        livereload: serv.liveReload
    });
});

// remove bundels
gulp.task('_clean-dev', function(cb) {
    target = gulpConfig.paths.dev;
    if (target.dir.indexOf("src") > -1 ) {return;} //never delete source code
    return del([target.dir], cb);
});
gulp.task('_clean-dist', function(cb) {
    target = gulpConfig.paths.dist;
    if (target.dir.indexOf("src") > -1 ) {return;} //never delete source code
    return del([target.dir], cb);
});


// by default build project and then watch files in order to trigger livereload
gulp.task('default', ['watch']);

// waits until clean is finished then assembles the project for dev testing
gulp.task('build-dev', ['_clean-dev'], function(){
    target = gulpConfig.paths.dev;
    gulp.start(['config', 'libs-copy', 'scripts-transform', 'css-less', 'files-copy']); //'images-copy', step 2, removed until sprites built
});

// waits until clean is finished then builds the project distribution
gulp.task('build-dist', ['_clean-dist'], function(){
    target = gulpConfig.paths.dist;
    gulp.start(['config', 'libs-copy', 'scripts-dist', 'css-less', 'files-copy']);//'images-copy' ...
});

gulp.task('build-zip-dist', function(){
    target = gulpConfig.paths.dist;
    gulp.start(['dir-zip']);
});



