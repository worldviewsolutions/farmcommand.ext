!function (e) { if ("object" == typeof exports && "undefined" != typeof module) module.exports = e(); else if ("function" == typeof define && define.amd) define([], e); else { var f; "undefined" != typeof window ? f = window : "undefined" != typeof global ? f = global : "undefined" != typeof self && (f = self), f.leafletPip = e() } } (function () {
var define, module, exports; return (function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); throw new Error("Cannot find module '" + o + "'") } var f = n[o] = { exports: {} }; t[o][0].call(f.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, f, f.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++)s(r[o]); return s })({
1: [function (_dereq_, module, exports) {
var pip = _dereq_('point-in-polygon');

function getLls(l) {
    var lls = l.getLatLngs(), o = [];
    for (var i = 0; i < lls.length; i++) o[i] = [lls[i].lng, lls[i].lat];
    return o;
}
var leafletPip = {
    bassackwards: false,
    pointInLayer: function (p, layer, first) {
        'use strict';
        if (p instanceof L.LatLng) p = [p.lng, p.lat];
        if (leafletPip.bassackwards) p.reverse();

        var results = [];
        layer.eachLayer(function (l) {
            if (first && results.length) return;
            // multipolygon
            var lls = [];
            if (l instanceof L.MultiPolygon) {
                var featureGroup = l.toGeoJSON();
                var featureCoords = l.feature.geometry.coordinates;
                // Hacked up implementation
                // Since each polygon layer in the multipolygon doesn't store coords, run through all the coords
                // in the feature, check if point is in it, and then find the layer with the matching coordinate pair
                // Loop through each polygon in multipolygon e.g. [Poly1 ring array, poly2 ring array]
                featureCoords.forEach(function (coords) {
                    // Loop through each ring
                    for (var i = 0; i < coords.length; i++) {
                        // if point in ring
                        if (pip(p, coords[i])) {
                            // Find the layer with matching polygon coordinates
                            l.eachLayer(function (layer) {
                                var feature = layer.toGeoJSON();
                                var layerCoords = feature.geometry.coordinates;
                                if (JSON.stringify(coords) == JSON.stringify(layerCoords)) {
                                    results.push(layer);
                                    return; // return once layer found
                                }
                            })
                            // Stop loop once first ring matches in polygon
                            return;
                        }
                    }
                })
            } else if (l instanceof L.Polygon) {
                lls.push(getLls(l));
            }
            for (var i = 0; i < lls.length; i++) {
                if (pip(p, lls[i])) results.push(l);
            }
        });
        return results;
    },
    pointInMulti: function () {

    }
};


module.exports = leafletPip;

    }, { "point-in-polygon": 2 }], 2: [function (_dereq_, module, exports) {
        module.exports = function (point, vs) {
            // ray-casting algorithm based on
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

            var x = point[0], y = point[1];

            var inside = false;
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                var xi = vs[i][0], yi = vs[i][1];
                var xj = vs[j][0], yj = vs[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        };

    }, {}]
}, {}, [1])
    (1)
});