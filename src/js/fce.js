// Require external dependencies
require('../libs/leaflet.editable/leaflet.editable.js');
require('../libs/leaflet.toolbar/leaflet.toolbar.js');

// Initialize with fce.config
require('./core/fce.core.js');

// Load extensions
require('./exts/fce.util.js');
require('./exts/fce.editable.js');

// Load base classes
require('./base/fce.baseAction.js');
require('./base/fce.baseMenuAction.js');
require('./base/fce.baseControl.js');
require('./base/fce.baseToolbar.js');
require('./base/fce.baseEvent.js');

// Load actions
require('./actions/fce.cancelAction.js');
require('./actions/fce.deleteToolAction.js');
require('./actions/fce.drawMenuAction.js');
require('./actions/fce.drawToolAction.js');
require('./actions/fce.editToolAction.js');
require('./actions/fce.redoAction.js');
require('./actions/fce.reshapeMenuAction.js');
require('./actions/fce.reshapeToolAction.js');
require('./actions/fce.endAction.js');
require('./actions/fce.settingsMenuAction.js');
require('./actions/fce.undoAction.js');

// Load toolbars
require('./toolbars/fce.toolbar.drawToolbar.js');
require('./toolbars/fce.toolbar.generalToolbar.js');
require('./toolbars/fce.toolbar.editToolbar.js');

// Load remaining core
require('./core/fce.events.js');
require('./core/fce.call.js');
require('./core/fce.operations.js');
require('./core/fce.storage.js');
require('./core/fce.data.js');
require('./core/fce.controller.js');
require('./core/fce.cursor.js');

