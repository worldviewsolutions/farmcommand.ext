"use strict";
L.FCE._Base.edit = L.FCE._Base.Control.extend({});
L.FCE.EditToolbar = L.FCE._Base.Toolbar.extend({
    options: {},
    initialize: function(options){
        var self = this;
        L.FCE._Base.Toolbar.prototype.initialize.call(this, options);
        //set action handlers
        this.addHandler("action",{self: self, type: "editTool", handler: function(e){e.source.toggleCursor();}});
        this.addHandler("action",{self: self, type: "reshapeMenu", handler: function(e){this.testAlert(e);}});
        this.addHandler("action",{self: self, type: "reshapeTool", handler: function(e){this.testAlert(e);}});
        this.addHandler("action",{self: self, type: "deleteTool", handler: function(e){this.testAlert(e);}});
        
        //set event handlers
        //this.addHandler(this, "event", {self: self, type:L.FCE.events... , handler:function(e){this.testAlert(e);}});
        this.startEventHandlers(this.eventHandlers);
     }
});