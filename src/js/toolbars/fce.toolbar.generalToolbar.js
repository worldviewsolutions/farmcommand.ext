L.FCE._Base.general = L.FCE._Base.Control.extend({});
L.FCE.GeneralToolbar = L.FCE._Base.Toolbar.extend({
    options: {},
    initialize: function(options){
        var self = this;
        this.allowActiveActions = false;  // only allow indirect workflow overrides from general toolbar (no click events will set ops.activeAction)
        L.FCE._Base.Toolbar.prototype.initialize.call(this, options);
        //Action Handlers        
        this.addHandler("action", {self: self, type: "settings", 
            handler: function(e){this.testAlert(e);
            }
        });
        this.addHandler("action", {self: self, type: "toggleSnapping", 
            handler: function(e){this.testAlert(e);
            }
        });
        this.addHandler("action", {self: self, type: "toggleTopology", 
            handler: function(e){this.testAlert(e);
            }
        });
        this.addHandler("action", {self: self, type: "undo",
            handler: function(e){
                L.FCE.events.editUndo();

            }
        });
        this.addHandler("action", {self: self, type: "redo",
            handler: function (e) {
                L.FCE.events.editRedo();
            }
        });
        this.addHandler("action", {self: self, type: "cancel",
            handler: function(e){
                L.FCE.events.editCancel();
            }
        });
        this.addHandler("action", {self: self, type: "end",
            handler: function(e){
                L.FCE.events.editEnd(e);
            }
        });
        
        //set event handlers
        // Event: New Ops
        this.addHandler("event", {self: self, type:L.FCE.events.ops.new.type,
            handler:function(e){
                //e.source=== fce.ops
                this.setState("undo", "active");
                this.setState("redo", "inactive");
            }
        });
        // Event: Ops Updated
        this.canDo = function(e, source){
            var curOpItem = e.data.current; // current stack item
            var notEmpty = (!!e.data.count && e.data.count > 0); // event has data with property count (stack length)
            var curWorkflow = curOpItem && curOpItem.source && curOpItem.source.inWorkflow(); // stack current operation exists and current stack item is in an active workflow

            var canUndo = notEmpty && e.data.index > -1; // stack filled and index not at start
            var canRedo = notEmpty && e.data.index < e.data.count - 1; // stack filled and index not at end
            var canEnd = notEmpty && curWorkflow; // stack  filled and current stack item is in an active workflow
            var canCancel = L.FCE.ops.inWorkflow();
            if (curOpItem && curWorkflow){
                switch (curOpItem.source.name){
                    case "drawTool":
                        canUndo = curOpItem.data.coordinates.length > 1;
                        canRedo = curOpItem.data.redoBuffer.length > 0;
                        canEnd = (curOpItem.data.valid && curOpItem.data.validateDraw(curOpItem.data));
                        break;
                }
            }
            // assemble response
            var states = {};
            var addState = function(state, name){
                state = (state)?"active" : "inactive";
                states[name] = state;
            };
            if (source === L.FCE.events.ops.updated.type) {addState(canUndo, "undo");}
            if (source === L.FCE.events.ops.updated.type) {addState(canRedo, "redo");}
            if (source === L.FCE.events.ops.workflow.type) {addState(canCancel, "cancel");}
            if (source === L.FCE.events.ops.workflow.type || (curOpItem && curWorkflow)) {addState(canEnd, "end");}
            return states;
        };
        this.addHandler("event", {self: self, type:L.FCE.events.ops.updated.type,
            handler:function(e){
                var states = this.canDo(e, L.FCE.events.ops.updated.type);
                if (states.end){this.setState("end", states.end);}
                if (states.undo){this.setState("undo", states.undo);}
                if (states.redo){this.setState("redo", states.redo);}

            }
        });
        // Event: Ops Workflow Changed
        this.addHandler("event", {self: self, type: L.FCE.events.ops.workflow.type,
            handler:function(e) {
                var states = this.canDo(e, L.FCE.events.ops.workflow.type);
                if (states.cancel){this.setState("cancel", states.cancel);}
                if (states.end){this.setState("end", states.end);}
            }
        }, this);
        this.startEventHandlers(this.eventHandlers);
    }
});

