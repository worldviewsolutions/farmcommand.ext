"use strict";
L.FCE._Base.draw = L.FCE._Base.Control.extend({});
L.FCE.DrawToolbar = L.FCE._Base.Toolbar.extend({
    options: {},
    initialize: function (options) {
        var self = this;
        L.FCE._Base.Toolbar.prototype.initialize.call(this, options);
        //set action handlers
        this.addHandler("action", { self: self, type: "drawMenu", handler: function (e) { return; } });
        this.addHandler("action", { self: self, type: "selectMarker", handler: function (e) { return; } });
        this.addHandler("action", { self: self, type: "selectPolyline", handler: function (e) { return; } });
        this.addHandler("action", { self: self, type: "selectPolygon", handler: function (e) { return; } });
        this.addHandler("action", { self: self, type: "selectCircle", handler: function (e) { return; } });
        this.addHandler("action", {self: self, type: "drawTool", handler: function (e) {
            if (!!e.data && !!e.data.value && !!e.source){
                var menu = e.data.value;
                L.FCE.fire(L.FCE.events.modes.draw.enable.type, {geometryType:menu.value});
            }
        }});
        //this.addHandler({self: self, type: "", handler: function(e){this.testAlert(e);}});

        //set event handlers
        //this.addHandler(this, "event", {type:L.FCE.events... , handler:function(e){this.testAlert(e);}});
        this.startEventHandlers(this.eventHandlers);
    }
});
