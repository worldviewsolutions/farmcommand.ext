L.FCE.Editable = L.Editable.extend({
    initialize: function (map, options) {
        L.Editable.prototype.initialize.call(this, map, options);
    },
    drawnCoordinates: function(){
        return this._drawingEditor._drawnLatLngs;
    }
});