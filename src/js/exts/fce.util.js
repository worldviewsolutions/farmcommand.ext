L.Util.deepcopy = require('../../libs/deepcopy/deepcopy.min.js');
L.Util.leafletPip = require('../../libs/leaflet.pip/leaflet-pip.js');

// From MDN Object assign polyfill, needed to support phantomjs
Object.assign = function(target) {
    'use strict';
    if (target == null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }

    target = Object(target);
    for (var index = 1; index < arguments.length; index++) {
      var source = arguments[index];
      if (source != null) {
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
    }
    return target;
};

L.Util.clone = function(cloneMe, deep){
    if (deep){return L.Util.deepcopy(cloneMe);}
    return Object.assign({}, cloneMe);
};

L.Util.typeOf = function(layer) {
    if (layer instanceof L.Polygon) {
        return 'polygon'
    }
    else if (layer instanceof  L.Polyline) {
        return 'polyline'
    }
    else if (layer instanceof  L.Marker) {
        return 'marker'
    }
    else if (layer instanceof  L.FeatureGroup) {
        return 'FeatureGroup'
    }
    else if (layer instanceof  L.LayerGroup) {
        return 'LayerGroup'
    }
    return 'none';
}

L.Util.deepMergeOptions = function (inputObj, mergeObj) {
    var customizer = function (objValue, srcValue, key, obj, source) {
        // Return obj property if in input
        if (obj.hasOwnProperty(key) && !source.hasOwnProperty(key)) {
            return objValue
        }
        // Set property to null if not in input (possible to skip?)
        else if (!obj.hasOwnProperty(key) && source.hasOwnProperty(key)) {
            return null
        }
    };
    var mergedObj = L._.mergeWith(inputObj, mergeObj, customizer)
    // Discard any properties set to null when merged
    mergedObj = (function filter(obj) {
        var filtered = L._.omitBy(obj, L._.isNull); // Remove properties set to null
        return L._.cloneDeepWith(filtered, function (v) {
            return v !== filtered && L._.isPlainObject(v) ? filter(v) : undefined;
        }
        );
    })(mergedObj);

    return mergedObj;
};

L.Util.isEmpty = function(obj) {
    return !!(Object.keys(obj).length === 0 && obj.constructor === Object);
};

// From Leaflet Storage
L.Util.copyJSON = function (geojson) {
    return JSON.parse(JSON.stringify(geojson));
};

L.Util.usableOption = function (options, option) {
    return typeof options[option] !== 'undefined' && options[option] !== '' && options[option] !== null;
};

L.DomUtil.classIf = function (el, className, bool) {
    if (bool) L.DomUtil.addClass(el, className);
    else L.DomUtil.removeClass(el, className);
};


L.DomUtil.element = function (what, attrs, parent) {
    var el = document.createElement(what);
    for (var attr in attrs) {
        el[attr] = attrs[attr];
    }
    if (typeof parent !== 'undefined') {
        parent.appendChild(el);
    }
    return el;
};

L.DomUtil.before = function (target, el) {
    target.parentNode.insertBefore(el, target);
    return el;
};

L.DomUtil.after = function (target, el)
{
    target.parentNode.insertBefore(el, target.nextSibling);
    return el;
};