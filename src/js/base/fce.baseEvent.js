"use strict";
L.FCE._Base.Event = L.Class.extend({
    /**
     *
     * @param args : target, type, message, callback,  data
     */
    initialize: function (args) {
        this.source = args.source || null;
        this.type = args.type || "";
        this.message = args.message || "";
        this.callback = args.callback  || null;
        this.data = args.data || null;
    }
});
