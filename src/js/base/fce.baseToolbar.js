// Manage events and state of toolbars, i.e. drawtoolbar
"use strict";
L.FCE._Base.Toolbar = L.Class.extend({
    //****Defaults****//
    includes: [L.Mixin.Events],

    initialize: function (configOptions) {
        L.setOptions(this, configOptions);
        this.name = this.options.name;
        this.allowActiveActions = true;
        this.activeAction = {};
        this.baseActions = {}; // all base actions by action name property
        this.subActions = {}; // all subactions by base action name property[sub action name property]
        this.control = {}; // ui element that scaffolds and contains actions
        this.loaded = false; // control loaded or not
        this.actionHandlers = {};
        this.eventHandlers = {};
    },

    //****Extensions****//
    load: function (customOptions) {
        if (!!this.loaded) { return this; }
        L.setOptions(this, customOptions); // override with custom toolbar options
        L.FCE.currentToolbar = this; // hacky, set toolbar Globally so actions can bind to toolbar when created
        var actions = this.getBaseActionClasses(); // get Action Classes (classes are not instantiated)
        this.control = new L.FCE._Base[this.name](this.options.control, actions).addTo(L.FCE.map);
        this.control.onLoad(); // set UI after render (visibility)
        this.afterControlLoad(); // do these after control, actions rendered into ui
        this.loaded = true;
        return this;
    },

    unload: function () {
        this.setActiveAction();
        this.stopEventHandlers(this.eventHandlers);
        L.FCE.map.removeLayer(this.control);
        this.baseActions = {};
        this.subActions = {};
        this.control = {};
        this.loaded = false;
    },

    //****Methods****
    afterControlLoad: function () {
        // Call each each and subaction, bind custom options, and do things now action is fully built
        Object.keys(this.baseActions).forEach(function (baseActionName) {
            this.baseActions[baseActionName].onLoad(this.options.actions[baseActionName]);
        }, this);
        Object.keys(this.subActions).forEach(function (baseActionName) {
            Object.keys(this.subActions[baseActionName]).forEach(function(subActionName) {
                var subAction = this.getSubAction(baseActionName, subActionName);
                var subActionOptions = this.options.actions[baseActionName].subActions[subActionName];
                subAction.onLoad(subActionOptions);
            }, this);
        }, this);
    },

    setState: function (actionName, state) {
        var action = this.getBaseAction(actionName);
        if (action) action.setState({state:state});
    },

    setActiveAction: function(action){
        if (action && action.name && action.parentToolbar){
           if (action.parentToolbar.name === this.name){
               this.activeAction = action;
               return;
           }
        }
        this.activeAction = null;
    },

    // Call an action from UI after toolbar loaded
    callAction: function (actionName) {
        if (this.loaded) {
            this.getBaseAction(actionName).addHooks();
        }
    },

    // Called from actions on render to be added to toolbar
    addBaseAction: function (baseAction) {
        var baseActionName = baseAction.name;
        if (this.getBaseAction(baseActionName) !== null) { return }
        this.baseActions[baseActionName] = baseAction;
        this.subActions[baseActionName] = {};
        return baseAction;
    },

    addSubAction: function (baseAction, subAction) {
        var baseActionName = baseAction.name;
        var subActionName = subAction.name;
        var subActions = this.getSubActions(baseActionName)
        if (subActions === null) { this.addBaseAction(baseAction); }
        if (this.getSubAction(baseActionName, subActionName) !== null) { return }
        this.subActions[baseActionName][subActionName] = subAction;
        return subAction;
    },

    getBaseActionClasses: function () {
        return Object.keys(this.options.actions).map(function (actionName) {
            var action = L.FCE.Actions[actionName];
            return action || null;
        }, this);
    },

    getBaseAction: function (baseActionName) {
        return baseActionName in this.baseActions ? this.baseActions[baseActionName] : null;
    },
    getSubActions: function (baseActionName) {
        return baseActionName in this.subActions ? this.subActions[baseActionName] : null;
    },

    getSubAction: function (baseActionName, subActionName) {
        var subActions = this.getSubActions(baseActionName);
        if (subActions === null) { return null; }
        return subActionName in subActions ? subActions[subActionName] : null;
    },

    //****End-Methods****
    //****EventHandlers****

    eventHandler: function (e) {
        if (e.type === "action" && this.actionHandlers[e.source.name] != null) {
            if (this.allowActiveActions && !(e.source.parentAction && e.source === e.source.parentAction.menuSelection)){
            //     L.FCE.ops.setWorkflow(e.source); // disabled this: should only set active workflow IF operation is pushed into opStack
                this.setActiveAction(e.source);
            }
            var actionHandler = this.actionHandlers[e.source.name];
            actionHandler.handler.call(this, e); //.bind(actionHandler.self);
        }
        else if (!!e.type) {
            this.testAlert(e);
        }
    },
    addHandler: function(type, e){
        var hType = type + "Handlers";
        this[hType][e.type] = e;
    },
    startEventHandlers: function(events){
       Object.keys(events).map(function (type) {
           var e = events[type];
           L.FCE.on(type, e.handler, e.self);
        });
    },
    stopEventHandlers: function(events){
        Object.keys(events).map(function (type) {
            var e = events[type];
            L.FCE.off(type, e.handler, e.self);
        });
    },
    //****End-EventHandlers****
    testAlert : function(e, value){
        var msg = (value)?" Selection: " + value: "";
        alert(e.message + msg);
    }
});
