"use strict";
L.FCE._Base.Action = L.ToolbarAction.extend({
    options: {
        state: 'active' // Action on/off 
    },
    defaultIcon: '', // Keep track of default icon if changed later via menu selection
    menuActive: false, // Menu open or closed
    bubbles: true,
    menuSelection: null, // Active menu selection
    workflow: false,
    initialize: function () {
        this.parentToolbar = L.FCE.currentToolbar; // hacky workaround to bind action to toolbar
        L.ToolbarAction.prototype.initialize.call(this);
        this.parentToolbar.addBaseAction(this); // make action callable from parent for onLoad;
    },

    //****Methods****
    isActive: function () {
        return this.options.state === 'active';
    },
    
    setWorkflowState: function(state, id){
       if(this.workflow){
           this.workflow = {active: state, id: id};
       }  
    },
    
    inWorkflow: function(){
        if(this.workflow){
            return !!this.workflow.active;
        }
    },
   
    // force onEnd event
    end: function(){
        this.setWorkflowState(false);
        this.removeHooks();
        (this.parentAction) ? this.parentAction.end() : this.onEnd();
        if (this.parentToolbar) {this.parentToolbar.setActiveAction();}
    },

    // propagate to parent toolbar, optionally fire global notice
    bubble: function(type, e){
        this.parentToolbar.eventHandler(e);  // sets active action, and calls target evt handler
        if (e.source.bubbles) {L.FCE.fire(type, e);} //global notice of action
    },

    // removes open menu
    clearMenu: function () {
        this.disable();
        this.menuActive = false;
        return this.menuActive;
    },

    // trigger disable from
    disable: function () {
        L.ToolbarAction.prototype.disable.call(this);
    },

    // must be called after initialize (from onLoad)
    setMenuStyle: function () {
        var el = this.options.subToolbar._ul;
        // toolbarPos, should check if "left" style exists on element, but seems to be limited to Jquery?
        var toolbarPos = this.parentToolbar.options.control.position.indexOf('right') > 0 ? 'right' : 'left';
        if (!el || toolbarPos === 'left') { return; }
        el.style.left = 'auto';
        el.style.right = '26px';
    },

    // change icon via menu selection
    setMenuIcon: function (selection) {
        var selectionClassName = this.getIconClassName(selection);
        var menuClassName = this.getIconClassName(this);
        var menuIcon = this._link.childNodes[0];
        L.DomUtil.removeClass(menuIcon, menuClassName);
        L.DomUtil.addClass(menuIcon, selectionClassName);
    },

    // revert icon
    setDefaultIcon: function() {
        var currentIcon = this.getIconClassName(this);
        var menuIcon = this._link.childNodes[0];
        L.DomUtil.removeClass(menuIcon, currentIcon);
        L.DomUtil.addClass(menuIcon, this.defaultIcon);

    },

    // get icon's class name from action
    getIconClassName: function (action) {
        var icon = action._link.childNodes[0]; // icon is only element under button
        return icon.className.split(" ")[1]; // i.e. fa fa-icon-name
    },

    // set action state
    setState: function(options){
        var state = options.state;
        // Currently this function only sets the action class via options.state, but could be used to apply other property changes
        var el = this._link;
        switch (options.state){
            case "enable": //fall through as active
            case "active":
                L.DomUtil.removeClass(el, 'fce-action-disabled');
                state = 'active';
                break;
            case "disable":
            case "inactive":
                L.DomUtil.addClass(el, 'fce-action-disabled');
                break;
        }
        if (options.state === 'enable') { this.addHooks();} // fire click event
        this.options.state = state;

    },

    // get icon info from style style sheet
    getIconSprite: function (options) {
        // programmatically get the icon desired icon's css class name
        // css class name references the position of the desired icon
        //https://css-tricks.com/css-sprites/
    },

    //****End-Methods****
    //****EventHandlers****

    // configure Action after L.Toolbar renders it, called from FCE.Toolbar
    onLoad: function (options) {
        L.setOptions(this, options || {});
        this.setMenuStyle();
        this.defaultIcon = this.getIconClassName(this);
        this.setState(options);
    },

    // Plain Event Action -- Could come from subaction and menu action
    onClick: function (action) {
        if (action.isActive()) {
            var e = new L.FCE._Base.Event({source: action, type: "action", message: action.name + " click event"});
            this.bubble(L.FCE.events.action.start.type, e);
            this.clearMenu();
        }
    },

    // If action bound to another action
    onBoundClick: function () {
        // transform evt object using menuSelection;
        var selection = this.parentToolbar.getBaseAction(this.link);
        var data = (selection)? {value: selection.menuSelection}:null;
        if (selection && this.isActive()) {
            var e = new L.FCE._Base.Event({source: this, type: "action", message: this.name + " click event.", data: data});
            this.bubble(L.FCE.events.action.start.type, e);
        }
    },

    // Menu Button Click Action
    onMenuClick: function () {
        if (!this.menuActive && this.isActive()) {
            this.menuActive = true;
            return;
        }
        this.menuActive = false;
        // remove the open menu need to trigger subtoolbar close
        if (this.options.subToolbar.removeHooks) {
            this.options.subToolbar.removeHooks();
        }
        //else{
            //this.options.subToolbar._hide();  //todo hide menu here
        //}
    },

    // On Menu Selection Click
    onMenuSelection: function (selection) {
        if (selection.isActive()) {
            this.setMenuIcon(selection);
            this.menuSelection = selection;
            var e = new L.FCE._Base.Event({source: selection, type: "action", message: this.name + " click event.", data: {value: selection.menuSelection}});
            this.bubble(L.FCE.events.action.select.type, e);
            this.clearMenu();
        }
    },

    // Event Action closing out
    onEnd: function () {
        if (this.isActive()) {
            var e = new L.FCE._Base.Event({source: this, type: "action", message: this.name + " end"});
            this.bubble(L.FCE.events.action.end.type, e);
            this.clearMenu();
        }
    }
    //****End-EventHandlers****
});