// ui container for Actions
// wrapper used in baseToolbar's createControl method
L.FCE._Base.Control = L.Toolbar.Control.extend({

    //****Defaults****//
    options: {
        isVisible: true
    },

    //****Base Class****//
    initialize: function (options, actions) { //may be forced to use ext class init pattern
        L.setOptions(this, options);
        this.options.actions = actions;
        L.Toolbar.Control.prototype.initialize.call(this, this.options);
    },

    // Modify after render
    onLoad: function() {
        if (this.options.isVisible === false) {
            this.options.isVisible = true
            this.setVisibility(!this.options.isVisible);
        }
    },

    setVisibility:function (visible) {
        if (visible === this.options.isVisible) return;
        var setKey = (visible)?'removeClass':'addClass';
        L.DomUtil[setKey](this._ul,  'fce-toolbar-disabled');
        this.options.isVisible = visible;
    },

    toggleVisibility: function () {
        this.setVisibility(!this.options.isVisible);
    }

});