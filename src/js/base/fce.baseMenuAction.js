// Aka, immediateSubAction
L.FCE._Base.MenuAction = L.ToolbarAction.extend({
    //****Defaults****//
    options: {
        state: 'active' // Subaction on/off
    },
    //****Base Class****//  
    initialize: function (map, parentAction) {
        this.parentAction = parentAction;
        L.ToolbarAction.prototype.initialize.call(this, this.options);
        this.parentAction.parentToolbar.addSubAction(this.parentAction, this);
    },
    addHooks: function () {
        this.parentAction.clearMenu();
    },
    //****Extensions****//

    onLoad: function (options) {
        L.setOptions(this, options || {});
        this.setState(this.options);
    },
    isActive: function () {
        return this.options.state === 'active';
    },
    setState: function (options) {
        var state = options.state;
        var el = this._link;
        switch (options.state) {
            case "select":
            case "active":
                //set the  selected value on this menu or this actions linked menu
                if (options.state === 'select') {
                    this.parentAction.onMenuSelection(this);
                }
                else if (this.parentAction.menuSelection === this) {
                    this.parentAction.setDefaultIcon();
                }
                state = 'active';
                L.DomUtil.removeClass(el, 'fce-subaction-disabled');
                break;
            case "inactive":
                L.DomUtil.addClass(el, 'fce-subaction-disabled');
                break;
        }
        this.options.state = state;
    },
});