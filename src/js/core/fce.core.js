
// Bind toolbar to single object eg L.FCE
L.FCESingleton = L.Class.extend({

    includes: L.Mixin.Events,// Bind events, on, off, fire
    options: {},
    //todo: move _Base, rename/move _Actions, rename/move _SubActions to subClass ==> only expose options and public methods on L.FCE.
    _Base: {},
    Actions: {},
    SubActions: {},

    // data Storage
    store: {}, // all layers added through plugin
    targetData: null, // singular layer target (e.g. editing)
    selectionMap: {},
    newData: null, // not sure if needed


    initialize: function () {
        var _config = require('../fce.config.js');
        L.setOptions(this, _config);
    },

    // Instantiate L.FCE.controller and all the toolbars here
    addTo: function (map) {
        this.map = map; // Bind map to root level
        map.editTools = new L.FCE.Editable(map); // Bind Editable at root level
        this.events = new L.FCE.Events();
        this.controller = new L.FCE.Controller(map);
        this.storage = new L.FCE.Storage();
        this.data = new L.FCE.Data();
        this.ops = new L.FCE.Ops();
        this.ops.start();

        this.loadToolbar('general');
        this.setMode('default');
        return this;
    },

    // public methods route to controller
    loadToolbar: function (toolbarName, options) {
        return this.controller.loadToolbar(toolbarName, options);
    },
    unloadToolbar: function (toolbarName) {
        return this.controller.unloadToolbar(toolbarName);
    },
    call: function () {
        return { result: "function not yet built" };
    },
    setMode: function (modeName, storeId) {
        var modeSet = this.controller.setMode(modeName, storeId);
        return (!!modeSet);
    },
    addData: function(data, style){
        style = (style)? style : this.options.layers.styles.default;
        var datasets = (Array.isArray(data))?data:[data];
        var results = [];
        datasets.forEach(function(uid){
            if (isNaN(uid) || uid < 0) return;  //todo: add meaningful error response
            console.log("Creating new data for uid = " + uid.toString());
            var groupLayer = L.FCE.newData = this.data.create(uid);
            var eventType = L.FCE.events.layer.new;
            var e = new L.FCE._Base.Event({source: this, type: eventType.type, message: eventType.message, data:{value: groupLayer, style: style}});
            L.FCE.fire(e.type, e);
            results.push({id:uid, data: this.storage.add(groupLayer, style)});
        }, this);
        return results;
    },
    loadData: function(data, style){
        style = (style)? style : this.options.layers.styles.default;
        var datasets = (Array.isArray(data))?data:[data];
        var results = [];
        datasets.forEach(function(geoJson){
            var uid = geoJson.properties[L.FCE.options.data.uid];
            if (isNaN(uid) || uid < 0) return;  //todo: add meaningful error response
            console.log("Loading data for uid = " + uid.toString());
            var groupLayer = this.data.load(geoJson);
            if (!!groupLayer){
                var eventType = L.FCE.events.layer.load;
                var e = new L.FCE._Base.Event({source: this, type: eventType.type, message: eventType.message, data:{value: groupLayer, style: style}});
                L.FCE.fire(e.type, e);
                console.log("Storing data for uid = " + uid.toString());
                groupLayer = this.storage.add(groupLayer, style);
            }
            results.push({id: uid, data: groupLayer});
        }, this);
        return results ;  //handled by data.create
    },

    removeData: function(data){
        var ids = (data)?(Array.isArray(data))?data:[data]:[];
        if (ids.length === 0) {
            Object.keys(L.FCE.store).map(function(id) {
                this.storage.delete(id);
            }, this);
            this.setMode('default');
            return;
        }
        ids.map(function(id) {
            this.storage.delete(id);
        }, this);
        this.setMode('default');

    },
    exportData: function(data){
        var datasets = (Array.isArray(data))?data:[data];
        var results = [];
        datasets.forEach(function(uid) {
            if (isNaN(uid) || uid < 0) return;  //todo: add meaningful error response
            var groupLayer = this.storage.getById(uid);
            if (!!groupLayer) {
                console.log("Converting leaflet group layer to geoJSON for uid = " + uid.toString());
                results.push({id: uid, data: this.data.export(groupLayer)});
            }
        }, this);
        return results; //todo: add meaningful error response
    },

    hasEdits: function() {
        if (this.ops._stack.length > 0) {
            return true;
        }
        return false;
    },

    clearEdits: function() {
        if (this.ops._stack.length > 0) {
            this.targetData.options.updateData(this.ops._targetDataDefault, this.options.layers.styles.target);
            this.ops.clear();
            this.ops.load(L.FCE.data.reformatGeoJSON(L.FCE.targetData), "geoJson");
        }
        L.FCE.events.clearEventModes();
    }
});
L.FCE = new L.FCESingleton();
