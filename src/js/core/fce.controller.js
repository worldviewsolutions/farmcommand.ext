
L.FCE.Controller = L.Class.extend({

    includes: [
        L.Mixin.Events // add on, off, and fire
    ],

    // Modes
    currentMode: '', // track current set mode

    initialize: function () {

        // Set internal variables
        this.toolbars = {};
        // Define Toolbars
        Object.keys(L.FCE.options.toolbars).forEach(function (toolbarName) {
            this.toolbars[toolbarName] = this.createToolbar(toolbarName);
        }, this);
        return this;
    },

    // load toolbar into ui
    loadToolbar: function (toolbarName, options) {
        options = options || {};
        var toolbar = this.getToolbar(toolbarName);
        if (toolbar === null) return; // if no toolbar to load
        return toolbar.load(options);
    },

    // remove toolbar from ui and reset any custom options
    unloadToolbar: function (toolbarName) {
        var toolbar = this.getToolbar(toolbarName);
        toolbar.unload();
        L.Util.setOptions(toolbar, this.getToolbarOptions(toolbarName));
        return toolbar;
    },

    setMode: function (modeName, storeId) {
        L.FCE.fire('fce-mode-' + this.currentMode + '-disable');
        var isValidMode = this.validateMode(modeName, storeId);
        if (isValidMode === false) {
            return false;
        }
        var options = L.FCE.options.modes[modeName];
        // set toolbar + actions states
        var toolbar = {};
        var setToolbarState = function (options) {
            toolbar.setActiveAction();
            var defaultOptions = L.FCE.options.toolbars[toolbar.name];
            if (options.allActions) {
                Object.keys(defaultOptions.actions).forEach(function (baseActionName) {
                    var baseAction = toolbar.getBaseAction(baseActionName);
                    if (baseAction === null) { return; }
                    setActionState(baseAction, options, defaultOptions);
                }, this);
            }
        };

        // set action + subActions states
        var setActionState = function (action, options, defaultOptions) {
            //action defaults if options express, else just uses the state supplied by allActions
            var defaultState = defaultOptions.actions[action.name].state;
            var actionDefaults = (options.allActions && options.allActions === 'default') ? defaultOptions.actions[action.name] : {};
            // action options come from custom options if exist, else use action default
            var actionOptions = (options.actions && options.actions[action.name]) ? options.actions[action.name] : actionDefaults;
            actionOptions.state = (actionOptions.state) ? actionOptions.state : defaultState;
            if (!!actionOptions.state) {
                action.setState(actionOptions);
                // if active && has subActions
                if (actionOptions.state != 'inactive' && actionOptions.subActions) {
                    var subActionState = actionOptions.allActions || actionOptions.state;
                    var subActionList = defaultOptions.actions[action.name].subActions;
                    Object.keys(subActionList).forEach(function (baseSubActionName) {
                        var baseSubAction = toolbar.getSubAction(action.name, baseSubActionName);
                        if (baseSubAction === null) { return; }
                        var subActionDefaults = defaultOptions.actions[action.name].subActions[baseSubActionName];
                        subActionDefaults.state = subActionState; // get default subaction state
                        var subActionOptions = (actionOptions.subActions[baseSubActionName]) ? actionOptions.subActions[baseSubActionName] : subActionDefaults;
                        subActionOptions.state = (subActionOptions.state) ? subActionOptions.state : (subActionState) ? subActionState : {}; //use subAction opts over parent actionOptions, else null
                        if (!!subActionOptions) baseSubAction.setState(subActionOptions);
                    }, this);
                }
            }
        };
        // apply settings to each toolbar in options
        Object.keys(this.toolbars).forEach(function (toolbarName) {
            toolbar = this.getToolbar(toolbarName);
            toolbar.load();
            if (!(toolbarName in options)) {
                toolbar.control.setVisibility(false);
                return;
            }
            var toolbarOptions = options[toolbarName];
            setToolbarState(toolbarOptions);
            toolbar.control.setVisibility(true);
        }, this)


        // Always unload target data, new target may or may not be set
        L.FCE.storage.unloadTargetData();

        // apply modes to each layer in layer stack
        Object.keys(L.FCE.store).forEach(function(layerId) {
            var layer = L.FCE.store[layerId];
            var layerModeName = options.layers.all;
            if (+layerId === +storeId) {
                layerModeName = options.layers.target;
                layer.options.editable = true;
            }
            var layerMode = L.FCE.options.layers.modes[layerModeName];
            var layerStyle = L.FCE.options.layers.styles[layerMode.style];
            L.FCE.storage.setLayerMode(layer, layerMode, layerStyle);
        }, this);
        this.currentMode = modeName;
        L.FCE.fire('fce-mode-' + this.currentMode + '-enable');
        return true;
    },

    validateMode: function(modeName, storeId) {
        var isValid = true;
        switch(modeName) {
            case('create'):
                isValid = true;
                break;
            case('target'):
                if (typeof storeId === undefined && L.FCE.storage.getData(storeId) === null) {
                    isValid = false;
                }
                break;
        }
        return isValid;
    },

    // instantiate toolbar classes, nothing loaded into ui
    createToolbar: function (toolbarName) {
        if (toolbarName in this.toolbars) return this.getToolbar(toolbarName);
        var toolbar = {};
        var options = this.getToolbarOptions(toolbarName);
        options.name = toolbarName;
        switch (toolbarName) {
            case 'draw':
                toolbar = new L.FCE.DrawToolbar(options);
                break;
            case 'edit':
                toolbar = new L.FCE.EditToolbar(options);
                break;
            case 'general':
                toolbar = new L.FCE.GeneralToolbar(options);
                break;
        }
        return toolbar;
    },

    getToolbar: function (toolbarName) {
        return toolbarName in this.toolbars ? this.toolbars[toolbarName] : null;
    },

    getToolbarOptions: function (toolbarName) {
        return toolbarName in L.FCE.options.toolbars ? L.FCE.options.toolbars[toolbarName] : {};
    },

    activeActions: function(){
        var results = {};
        // apply settings to each toolbar in options
        Object.keys(this.toolbars).forEach(function (toolbarName) {
            var toolbar = this.toolbars[toolbarName];
            var action = toolbar.activeAction;
            if (!!action) {
                results[action.name] = action;
            }
        }, this);
        return results;
    }

});
