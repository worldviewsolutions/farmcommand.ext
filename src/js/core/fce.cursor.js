/*
 * L.Crosshairs displays a crosshair mouse cursor on the map.
 */

L.FCE.Cursor = L.LayerGroup.extend({
    includes: L.Mixin.Events,// Bind events, on, off, fire
	options: {
		style: {
			opacity: 1,
			fillOpacity: 0,
			weight: 2,
			color: '#333',
			radius: 20
		}
	},
	state:'inactive',

	initialize: function (options) {
		L.LayerGroup.prototype.initialize.call(this);
		L.Util.setOptions(this, options);
	},

	set: function(state) {
		switch(state) {
			case 'active':
				this.add();
				this.state = 'active';
				break;
			case 'inactive':
				this.remove();
				this.state = 'inactive';
				break;
		}
	},
	add: function () {
		this.crosshair = L.circleMarker([0, 0], this.options.style);
		this.addLayer(this.crosshair);
		this._oldCursor = this.getContainer === undefined ? 'default' : this.getContainer;
		this.getContainer = 'none';
		L.FCE.map.on('mousemove', this._moveCrosshairs.bind(this))
		L.FCE.map.on('zoomend', this._moveCrosshairs.bind(this))
		L.FCE.map.on('mouseout', this._hide.bind(this))
		L.FCE.map.on('mouseover', this._show.bind(this))
        this.crosshair.on('click', function(e) {L.FCE.fire(L.FCE.events.layer.selection.type, e)})
		L.FCE.map.addLayer(this);
	},

	toggle: function() {
        this.state == 'active' ? this.set('inactive') : this.set('active');
	},
	getContainer: function() {
		return L.FCE.map.getContainer().style.cursor
	},

	remove: function () {
		L.FCE.map.off('mousemove', this._moveCrosshairs);
		L.FCE.map.off('zoomend', this._moveCrosshairs);
		L.FCE.map.removeLayer(this);
        this.clearLayers();
		this.getContainer = 'default';
	},

	_show: function() {
        L.FCE.map.addLayer(this)
	},

	_hide: function() {
        L.FCE.map.removeLayer(this)
	},

	_moveCrosshairs: function (e) {
		var latlng = e.latlng ? e.latlng : this.crosshair.getLatLng()
		this.crosshair.setLatLng(latlng)
	}
})

L.FCE.cursor = function (options) {
	return new L.FCE.Cursor(options)
}