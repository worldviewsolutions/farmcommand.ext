"use strict";

// Eventing definitions and external rerouting eg editable events
L.FCE.Events = L.Class.extend({
    modes: {
        target: {
            enable: {type: 'fce-mode-target-enable', message: 'Target mode enabled'},
            disable: {type: 'fce-mode-target-disable', message: 'Target mode dislabed'}
        },
        edit: {
            enable: {type: 'fce-mode-edit-enable', message: 'Edit mode enabled'},
            disable: {type: 'fce-mode-edit-disable', message: 'Edit mode disabled'}
        },
        draw: {
            enable: {type: 'fce-mode-draw-enable', message: 'Draw mode enabled'},
            disable: {type: 'fce-mode-draw-disable', message:' Draw mode disabled'}
        }
    },
    ops: {
        reset: {type: 'fce-ops-reset', message: 'clear existing edit target? Y/N'},
        workflow: {type: 'fce-ops-workflow', message: 'workflow changed.'},
        new: {type: 'fce-ops-new', message: 'new operation.'},
        edit: {type: 'fce-ops-edit', message: 'current operation values changed.'},
        updated: {type:'fce-ops-updated', message: 'operation stack updated.'}
    },
    action: {
        select: {type: 'fce-action-select', message: 'menu select'},
        start: {type: 'fce-action-start', message: 'action start'},
        end: {type: 'fce-action-end', message: 'action end'}
    },
    layer: {
        new: {type:'fce-layer-new', message: 'new layer'},
        load: {type: 'fce-layer-load', message: 'load layer'},
        selection: {type: 'fce-layer-selection', message: 'new selection'},
        finish: {type: 'fce-layer-finish', message: 'finish layer'},
        save: {type: 'fce-layer-save', message: 'save layer'},
        reset: {type: 'fce-layer-reset', message: 'reset layer'}
    },
    edit: {
        enable:{type: 'editable:enable', message: 'editable enable'},
        disable:{type: 'editable:disable', message: 'editable disabled'},
        editing: {type: 'editable:editing', message: 'editable layer edited'},
        //vertexAdd: {type: 'editable:vertex:added', message: 'vertex added'}, //not a real editable event.  just a place-holder
        vertexAdd: {type:'editable:middlemarker:mousedown', message: 'vertex added'},
        vertexDelete: {type: 'editable:vertex:deleted', message: 'vertex deleted'},
        vertexDrag: {type: 'editable:vertex:drag', message: 'vertex dragging'},
        vertexDragStart: {type: 'editable:vertex:dragstart', message: 'vertex drag before start'},
        vertexDragEnd: {type: 'editable:vertex:dragend', message: 'vertex dragging ended'},
        vertexClickCtrl: {type: 'editable:vertex:ctrlclick', message: 'vertex control+click'},
        vertexClickShift: {type: 'editable:vertex:shiftclick', message: 'vertex shift+click'},
        vertexClickAlt: {type: 'editable:vertex:altclick', message: 'vertex alt+click'},
        vertexMenu: {type: 'editable:vertex:contextmenu', message: 'vertex context menu'},
        partAdded: {type: 'editable:vertex:featurePartAdded', message: 'feature part added'},//not a real editable event.  just a place-holder
        partDeleted: {type: 'editable:vertex:featurePartDeleted', message: 'feature part deleted'},//not a real editable event.  just a place-holder
        partMoved: {type: 'editable:vertex:featurePartMoved', message: 'feature part moved'},//not a real editable event.  just a place-holder
        drawStart:{type:'editable:drawing:start' , message: 'editable drawing started'},
        drawClick: {type: 'editable:drawing:click', message: 'editable draw tool click event'},
        drawCommit:{type: 'editable:drawing:commit', message: 'editable drawing committed'}
    },

    getEventByType: function(eType){
        var self = L.FCE.events;
        var evt = null;
        var evtObjs = [self.edit, self.layer, self.action, self.ops, self.modes.edit];
        evtObjs.forEach(function(obj){
            for(var key in obj){
                var evtObj = obj[key];
                if (evtObj.type === eType) {evt = evtObj; return;}
            }
        });
        return evt;
    },

    initialize: function(){
        this.editable = L.FCE.map.editTools;
        this.editMode = false;
        this.drawMode = false;
        //this.selectionMode = false;
        //this.reshapeMode = false;
        this.persistEventListeners();
    },


    persistEventListeners: function() {
        var self = L.FCE.events;
        L.FCE.on(this.modes.target.enable.type, this.onTargetModeStart);
        L.FCE.on(this.modes.target.disable.type, this.onTargetModeStop);
        L.FCE.on(this.modes.edit.enable.type, this.onEditModeStart);
        L.FCE.on(this.modes.edit.disable.type, this.onEditModeStop);
        L.FCE.on(this.modes.draw.enable.type, this.onDrawModeStart);
        L.FCE.on(this.modes.draw.disable.type, this.onDrawModeStop);

        this.editable.on(this.edit.vertexDelete.type, this.onVertex);
        this.editable.on(this.edit.vertexDragEnd.type, this.onVertex); 
    },

    updateOpStack: function(e, caller){
        var self = L.FCE.events;
        // set data type variables
        console.log(caller.message);
        var geoJson = self.editable.currentPolygon.toGeoJSON();
        var geomType = geoJson.geometry.type;
        var callback = function(){return true;};
        var coords, source, message, eType;
        var data = {};
        if (caller.type === self.edit.drawClick.type){
            eType = "action";
            coords = self.editable.drawnCoordinates() || e.latlng;

            //|| !!coords && coords.length ===1
        }
        var eventType = function(){
            switch (caller.type){
                case self.edit.vertexDragEnd.type:
                case self.edit.vertexAdd.type:
                case self.edit.vertexDelete.type:
                case self.edit.partAdded.type:
                case self.edit.partDeleted.type:
                case self.edit.partMoved.type:
                case self.edit.editing.type:
                    eType = (!eType)? caller.type:eType;
                    //source = self.mockAction("mapEdit");
                    return self.ops.new.type;
                default:
                    return self.ops.edit.type;
            }
        }();
        var evt = new L.FCE._Base.Event({source:source, callback:callback, type: eType, message: message, data: data});
        if (caller.type === self.edit.drawClick.type){
            if (!!coords && coords.length>1){
                evt = L.FCE.ops.getById();
            }
            else{ eventType = self.ops.new.type;}
            evt.source = L.FCE.controller.getToolbar("draw").activeAction;
            evt.data.coordinates = coords;
            evt.data.redoBuffer = [];
            evt.data.validateDraw = function(data){
                if (!data.coordinates && data.geoJson) {return true;}
                return L.FCE.data.validateCoords(geomType, data.coordinates.length);
            };
        }
        else{
            evt.callback = self.editEnd;
            evt.message = caller.message;
            evt.data.geoJson = L.FCE.data.reformatGeoJSON(L.FCE.targetData); //geoJson; //self.editable.currentPolygon.toGeoJSON();
        }
        evt.data.valid = L.FCE.data.validateGeometry(geoJson);
        L.FCE.fire(eventType, evt);
    },
    // Undo/Redo event during draw/edit modes
    onKeyDown: function(e) {
        var self = L.FCE.events;
        // TODO add internal event fire for tool updates
        //https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
        var keyCodes = {z: 90};
        var shift = e.shiftKey;
        var control = e.ctrlKey;
        var alt = e.altKey;
        switch (e.keyCode){
            case keyCodes.z:
                if (shift){ this.editRedo();}
                else{this.editUndo();}
                break;
        }
    },

    clearEventModes: function() {
        var self = L.FCE.events;
        if (self.drawMode) {
            self.onDrawModeStop();
        }
        if (self.editMode) {
            self.onEditModeStop();
        }
    },
    //*****TARGET*****
    onTargetModeStart:function(e) {
        var self = L.FCE.events;
        L.DomEvent.addListener(document, 'keydown', self.onKeyDown, self, L.FCE.map);
    },

    onTargetModeStop: function(e) {
        var self = L.FCE.events;
        self.clearEventModes();
        L.DomEvent.removeListener(document, 'keydown', self.onKeyDown.bind(self));
    },

    //*****EDIT*****
    onEditModeStart: function (e) {
        console.log('starting edit mode')
        var self = L.FCE.events;
        self.setDrawingGeometry(e.geometryType, false);
        L.FCE.storage.clearSelection();
        self.editMode = true;
        self.startEditableEditing();
        L.FCE.targetData.options.activeGroupId = e.groupId;
        // declare editable currentpolygon since not using editable event enabled/disabled
        self.editable.currentPolygon = e.targetLayer; 
    },

    onEditModeStop: function (e) {
        console.log('stopping edit mode')
        var self = L.FCE.events;
        self.stopEditableEditing();
        self.editMode = false;
        L.FCE.targetData.options.activeGroupId = null;
        self.setDrawingGeometry(null, true); // reactive actions and activate current geometry
    },

    setDrawingGeometry: function(geometryType, active) {
        geometryType = geometryType || null;
        var drawMenuName = 'drawMenu';
        var drawToolbar = L.FCE.controller.getToolbar('draw');
        // if no geometryType, reset to current active
        if (!(!!geometryType)) {
            var drawMenu = drawToolbar.getBaseAction(drawMenuName);
            geometryType = drawMenu.menuSelection.value
        }
        var subActionList = Object.keys(drawToolbar.getSubActions(drawMenuName));
        subActionList.forEach(function(subActionName)  {
            var subAction = drawToolbar.getSubAction(drawMenuName, subActionName);
            if (subAction.value === geometryType) {
                subAction.setState({state:'active'});
                subAction.addHooks();
                return; 
            }
            var state = !!active ? 'active': 'inactive';
            subAction.setState({state:state});
        })
    },


    onVertex: function(e, latlng, vertex, layer){
        var self = L.FCE.events;
        // ignore vertex interaction on drawmode
        if(self.drawMode) { return; }
        var evt = self.getEventByType(e.type);
        self.updateOpStack(e, evt);
    },
    // onVertexChanged: function(e, latlng, vertex, layer){
    //     var self = L.FCE.events;
    //     var evt = self.getEventByType(e.type);
    //     self.updateOpStack(e, evt);
    // },
    // onVertexEngaged: function(e, latlng, vertex, layer){
    //     var self = L.FCE.events;
    //     //self.updateOpStack(e, self.edit.drawClick.type);
    // },
    // onVertexAdd: function(e) {
    //     var self = L.FCE.events;
    //     var evt = self.getEventByType(e.type);
    //     self.updateOpStack(e, evt);
    // },
    startEditableEditing: function(){
        var self = L.FCE.events;
        // Generic editing event
        //self.editable.on(self.edit.editing.type, self.onEditableEditing);
        // Vertex changed
        //self.editable.on(self.edit.vertexDrag.type, );
        //self.editable.on(self.edit.vertexDragStart.type, );
        // Vertex engaged
        //self.editable.on(self.edit.vertexClickCtrl.type, self.onVertexEngaged);
        //self.editable.on(self.edit.vertexClickShift.type, self.onVertexEngaged);
        //self.editable.on(self.edit.vertexClickAlt.type, self.onVertexEngaged);
        //self.editable.on(self.edit.vertexMenu.type, self.onVertexEngaged);
    },

    stopEditableEditing: function(){
        console.log('stop editable editing')
        var self = L.FCE.events;
        // Generic editing event
        // self.editable.off(self.edit.editing.type);
        // // Vertex changed
        // self.editable.off(self.edit.vertexDelete.type);
        // self.editable.off(self.edit.vertexDragEnd.type);
        //self.editable.off(self.edit.vertexDrag.type);
        //self.editable.off(self.edit.vertexDragStart.type);
        // Vertex engaged
        // self.editable.off(self.edit.vertexClickCtrl.type);
        // self.editable.off(self.edit.vertexClickShift.type);
        // self.editable.off(self.edit.vertexClickAlt.type);
        // self.editable.off(self.edit.vertexMenu.type);
    },
    //*****END - EDITING

    //*****DRAWING*****

    onDrawModeStart: function(e) {
        console.log('starting draw mode')
        var self = L.FCE.events;
        self.editable.on(self.edit.drawStart.type, self.startEditableDrawing);
        self.editable.on(self.edit.drawClick.type, function(e) {
            self.updateOpStack(e, self.edit.drawClick); // pass whole event, not just type
        }, self);
        self.editable.on(self.edit.drawCommit.type, function(e){
            self.editEnd(e);
        }, self);

        self.stopEditableEditing();
        self.drawMode = true;
        L.FCE.storage.clearSelection();
        self.setToolbarDrawing(false);

        switch(e.geometryType) {
            case 'polygon':
                self.editable.startPolygon();
                break;
            case 'polyline':
                self.editable.startPolyline();
                break;
            case 'marker':
                self.editable.startMarker();
                break;
        }
    },

    onDrawModeStop: function() {
        console.log('stopping draw mode')
        var self = L.FCE.events;
        var drawing = self.editable.currentPolygon;
        self.editable.off(self.edit.drawStart.type);
        self.editable.off(self.edit.drawClick.type);
        self.editable.off(self.edit.drawCommit.type);
        if (drawing) {
            drawing.disableEdit();
            L.FCE.map.removeLayer(drawing);
        }
        self.drawMode = false;
        self.setToolbarDrawing(true);
        // Edit mode is active if draw extended feature, must be turned off
        if (self.editMode) {
            // Disable edits calls edit mode stop
            L.FCE.targetData.options.disableEdits(L.FCE.targetData.options.activeGroupId);
        }
    },

    // called when edit mode is started
    // passes in layer created by editable
    startEditableDrawing: function(e){
        var self = L.FCE.events;
        self.editable.currentPolygon = e.layer;
        // do not extend markers, multipoint does not exist
        if (L.Util.typeOf(e.layer) === 'marker') {
            var activeGroupId = L.FCE.targetData.options.activeGroupId;
            L.FCE.targetData.options.disableEdits(activeGroupId);
            activeGroupId = null;
        }
    },

    // turn drawing from toolbar on/off
    setToolbarDrawing: function(active) {
        var drawTools = ['drawMenu', 'drawTool']
        var drawToolbar = L.FCE.controller.getToolbar('draw');
        // if no geometryType, reset to current active
        drawTools.forEach(function(toolName) {
            var action = drawToolbar.getBaseAction(toolName);
            var state = !!active ? 'active': 'inactive';
            action.setState({state:state});
        });
    },
    //*****END - DRAWING*****

    //*****GENERAL TOOLBAR*****
    editUndo: function(){
        var self = L.FCE.events;
        if (L.FCE.ops.inEditableWorkflow()){
            var evt = L.FCE.ops.getById();
            var latlng = this.editable._drawingEditor.pop();
            if (latlng) {
                evt.data.redoBuffer.push(latlng);
                L.FCE.fire(this.ops.edit.type, evt);
            }
            return;
        }
        if (self.editMode) {
            self.onEditModeStop();
        }
        L.FCE.ops.prev();
        
    },

    editRedo: function(){
        if (L.FCE.ops.inEditableWorkflow()){
            var evt = L.FCE.ops.getById();
            this.editable._drawingEditor.push(evt.data.redoBuffer.pop());
            L.FCE.fire(this.ops.edit.type, evt);
            return;
        }
        L.FCE.ops.next();
    },

    editCancel: function(){
        L.FCE.fire(this.modes.draw.disable.type);
        L.FCE.ops.setWorkflow(); //clear and stop
    },

    editEnd: function(e){
        var evt = L.FCE.ops.getById();
        switch (evt.source.name){
            case "drawTool":
                var drawing = this.editable.currentPolygon;
                L.FCE.data.commitDrawing(drawing);
                L.FCE.fire(this.modes.draw.disable.type);
                if (evt.data.coordinates) {delete evt.data.coordinates;}
                if (evt.data.redoBuffer) {delete evt.data.redoBuffer;}
                evt.data.geoJson = L.FCE.data.reformatGeoJSON(L.FCE.targetData);
                break;
            default:
                evt.data.geoJson = L.FCE.data.reformatGeoJSON(L.FCE.targetData);
        }
        L.FCE.ops.updateWorkflow(false);
    }
    //*****END - GENERAL TOOLBAR*****
});

