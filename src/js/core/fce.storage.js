"use strict";

L.FCE.Storage = L.Class.extend({

    initialize: function () {
    },

    // Insert geoJson and add to map
    add: function (groupLayer) {
        var result = false;
        if (groupLayer && groupLayer.options){
            var uid = groupLayer.options.storeId;
            if (!isNaN(uid) && uid > -1 && !(uid in L.FCE.store)) {
                L.FCE.store[uid] = groupLayer;
                groupLayer.addTo(L.FCE.map);
                result = groupLayer;
            }
            else { 
                result = L.FCE.store[uid];
            };
        }
        return result;
    },

    
    delete: function (storeId) {
        var groupLayer = this.getById(storeId);
        if (groupLayer === null) { return; }
        L.FCE.map.removeLayer(groupLayer);
        delete L.FCE.store[storeId];
    },

    clear: function(ids){
        ids = (ids && ids.length > 0)?ids:this.getIds();
        ids.forEach(function(storeId){
            this.delete(storeId);
        }, this);
        return true;
    },

    getById: function (storeId) {
        return storeId in L.FCE.store ? L.FCE.store[storeId] : null;
    },

    getIds: function (){
        var ids = [];
        for (var key in L.FCE.store){
            ids.push(key);
        }
        return ids;
    },

    // Mode setting
    setLayerMode: function (groupLayer, mode, style) {
        // Iterate through each propert and set configuration on groupLayer
        Object.keys(mode).forEach(function (prop) {
            var property = mode[prop];
            groupLayer.options[prop] = property;
            switch (prop) {
                case 'visible':
                    property ? groupLayer.options.setGroupStyle(groupLayer, style) : this.hideData(groupLayer);
                    break;
                case 'editable':
                    property ? this.setTargetData(groupLayer) : null;
                    break;
                case 'movable':
                    break;
            }
        }, this);
    },


    // Target Data manipulation
    setTargetData: function (groupLayer) {
        this.unloadTargetData();
        L.FCE.targetData = groupLayer;
        L.FCE.ops.load(groupLayer.toGeoJSON(), "geoJson");
        var bounds = groupLayer.getBounds();
        if (!(L.Util.isEmpty(bounds))) {
            L.FCE.map.fitBounds(bounds);
        }
        return groupLayer;
    },

    unloadTargetData: function() {
        if (L.FCE.targetData == null) {return;}
        if (L.FCE.targetData._map !== null) {
            L.FCE.targetData.options.unloadAsTarget();
        }
        L.FCE.targetData = null;
        L.FCE.ops.clear();
    },


    // Selection interactions

    clearSelection: function () {
        L.FCE.targetData.options.clearSelection();
        L.FCE.selectionMap = {};
        Object.keys(L.FCE.store).forEach(function (storeId) {
            var groupLayer = L.FCE.store[storeId];
            groupLayer.options.clearSelection();
        })
    },

    updateSelected: function(storeId, groupId, isSelected) {
        var map = L.FCE.selectionMap;
        if (!isSelected) {
            var stack = map[storeId];
            stack.splice(stack.indexOf(groupId), 1);
            if (stack.length === 0) {
                delete map[storeId];
            }
            return;
        }
        if (!(storeId in map)) {
            map[storeId] = [];
        }
        map[storeId].push(groupId);

    },

    // UI interaction

    hideData: function (groupLayer) {
        groupLayer.options.setGroupStyle(groupLayer, L.FCE.options.layers.styles.hidden);
    },

    showAll: function () {
        Object.keys(L.FCE.store).forEach(function (storeId) {
            var groupLayer = L.FCE.store[storeId];
            groupLayer.options.resetStyle();
        }, this);
    },

    hideAll: function () {
        Object.keys(L.FCE.store).forEach(function (storeId) {
            this.hideData(this.getLayer(storeId));
        }, this);
    }
});
