"use strict";
L.FCE.Ops = L.Class.extend({
    includes: [
        L.Mixin.Events // add on, off, and fire
    ],
    // todo:
    
    initialize: function () {
        this.workflow = null; //populated if current action is a workflow
        this._targetDataDefault = null;  //initial state for editing target's data
        this._targetDataType = "";
        this._max = L.FCE.options.ops.max;
        this._stack = [];
        this._current = -1;
        return this;
    },
    //****PublicMethods****
    // begin tracking operations
    start: function(){
        this.clear();        
        L.FCE.on(L.FCE.events.ops.new.type, this._add, this);
        L.FCE.on(L.FCE.events.ops.edit.type, this._edit, this);
    },
    
    // stop tracking operations
    stop:function(){
        this.clear();
        L.FCE.off(L.FCE.events.ops.new.type, this._add, this);
        L.FCE.off(L.FCE.events.ops.edit.type, this._edit, this);
    },

    // set target, optionally override existing target if one already exists
    load: function(targetData, targetType){
        var doLoad = function(){
            this.clear();
            this._targetDataDefault = targetData;
            this._targetDataType = targetType;
        };
        if (this._targetDataDefault){
            //todo: test this when we've got working editing
            var e = new L.FCE._Base.Event({source: this, callback: doLoad.bind(this),
                type: "request", message: "To continue, L.FCE needs permission to discard current edits."});
            L.FCE.fire(L.FCE.events.ops.reset, e);
        }
        else{
            doLoad.call(this);
        }      
    },
    
    // clear operation stack
    clear: function(){
        this.workflow = null;
        this._targetDataDefault = null;
        this._targetDataType = "";
        this._stack = [];
        this._setCurrent(-1);
    },

    // get next stack item, push to item's callback
    next: function(){      
        var index = this._getIndex(this._current + 1);
        this._pushOp(index, "redo");
    },

    // get previous stack item, push to item's callback
    prev: function(){
        var index = this._getIndex(this._current - 1);
        this._pushOp(index, "undo");
    },

    inWorkflow: function(){
        return (!!this.workflow && this.workflow.name != null);
    },

    inEditableWorkflow: function(){
        var index = this._current;
        var current = this._stack[index];
        // if a editable workflow operation
        return (this.inWorkflow() && current.data.coordinates && current.data.redoBuffer);
    },
    
    diffWorkflow: function(action){
        if (!action) {return true;}
        if (!(this.workflow.name != null && action.name != null)){
            console.log("L.FCE.ops - diffWorkflow(): error - workflow action(s) missing property 'name'.");
            return false;
        }
        else if(this.workflow.name !== action.name){
            return (this.workflow.id === action.workflow.id);
        }
        return true;
    },

    // set or replace workflow, stop existing
    setWorkflow: function(action) {
        if (!!action && !action.workflow ) return false;
        if (!action || (this.inWorkflow() && this.diffWorkflow(action))) { //if action null or action different
            // either way, end workflow
            this.updateWorkflow(true, action);  // cancel existing workflow
            return false;
        }
        else {
            this.workflow = action || null;
            this._eventHandler(L.FCE.events.ops.workflow);
            return true;
        }
    },

    // if workflow present, either delete all OR all except last
    updateWorkflow: function(clear, action){
        var index = -1;
        var count = 0;
        for (var i=this._stack.length - 1; i>-1; i--) {
            var a = this._stack[i].source;
            if (!!a && a.inWorkflow() && !this.diffWorkflow(a)){ //stack item's source action (1) is workflow and (2) is active workflow
                count += 1;
                index = i;
            }
            else if (count > 0) break;
        }
        if (clear){
            count = 1;
            // Count holds the number of items in workflow, remove them all using length of stack
            this._stack.splice(this._stack.length - count, 1); //todo: refactor this, and diffWorkflow
        }
        else{
            count = count - 1;
            this._stack.splice(index,count);
        }

        // update workflow and actions
        this._stopWorkflow();
        this.workflow = null;
        index = this._stack.length - 1;
        if (!!action){
            this.workflow = action;
            action.setWorkflowState(true, index);
        }
        this._setCurrent(index);
        this._eventHandler(L.FCE.events.ops.workflow);
        return count > 0;
    },

    getById: function(id){
        id = (isNaN(id) || id < 0)? this._current:id;
        return (id > -1 && id < this._stack.length)?this._stack[id]:null;
    },
    
    //****End-PublicMethods****

    _stopWorkflow: function(){
        this.workflow.end();
    },

    // add operation to operation stack
    _add: function(e){
        delete e.target; // remove leaflet bound event target
        e.data = (e.data != null)? e.data : {};

        // always flush stack if first value added, must be done if new action started at head of stack with
        // finished actions at tail (due to undo)
        if (this._current == -1) {
            this._stack = [];
        }
        else if (this._current > -1){
            var current = this._stack[this._current];
            var last = this._stack.length - 1;
            // prune stack branch when new action started after undo
            if (this._current != last ) {
                var count = Math.abs(last - this._current);
                // clear stack if new action started at head
                this._stack.splice(count);
            }
        }
        // handle new or interupting workflow
        //var updateSourceWorkflow = false;
        //var isMapEvent = (e.source && e.source._leaflet_events && e.source._leaflet_events['editable:enable']);
        //if (e.source.workflow){
        var updateSourceWorkflow = (!!e.source)?this.setWorkflow(e.source):false;
        //}


        // if needed, truncate stack to keep count under max (we will always keep target default state, however)
        if (this._stack.length > this._max) {
            this._stack.shift();
        }
        // add item to stack, update current index, and fire event handler
        this._stack.push(e);
        var newIndex = this._stack.length -1 ;
        if (updateSourceWorkflow){e.source.setWorkflowState(true, newIndex);}
        this._setCurrent(newIndex);
    },

    // replace current event with new event object
    _edit: function(e){
        var current = this._stack[this._current];
        if (!!current.source && current.source.inWorkflow() && current.source.name === e.source.name){
            this._stack[this._current] = e;
            this._eventHandler(L.FCE.events.ops.updated);
        }
    },
    
    // remove single operation
    _remove: function(id){
        this._stack.splice(id,1);
    },

    // push op.data to op.callback
    _pushOp: function(index, sender){
        var item, message;
        item = L.FCE.targetData;
        if (index > -1){
            item = (this._stack)?this._stack[index]:null;
            message = sender + " - " + item.message;
        }
        else if (index === -1) {            
            message = "reset - " + item.message;
        }

        if (!!item){
            switch (this._targetDataType){
                case "geoJson":
                    var data = (index > -1 && item.data.geoJson)?item.data.geoJson:this._targetDataDefault;
                    L.FCE.targetData.options.updateData(data, L.FCE.options.layers.styles.target);
                    this._setCurrent(index);
                    break;
                default:
                    // clone and update original event values
                    var clone = L.Util.clone(item);
                    clone.message = message + " - " + item.message;
                    clone.callback = function(success){
                        if (success){
                            this._setCurrent(index);
                        }
                    }.bind(this);
                    item.callback(clone);
            }
        }
    },

    // // push _targetDataDefault to FCE.targetData
    // _pushDefault: function(){
    //     if(L.FCE.targetData && this._targetDataDefault){
    //         switch (this._targetDataType){
    //             case "geoJson":
    //                 L.FCE.targetData.options.updateData(this._targetDataDefault, L.FCE.storage.styles.edit);
    //                 this._setCurrent(-1);
    //                 break;
    //         }
    //     }
    // },

    // get corrected index based on value provided
    _getIndex: function(index){
        var min = -1;
        var max = (this._stack && this._stack.length)? this._stack.length - 1:min;
        var current = max;  //default is max
        if (index != null && !isNaN(index)){
            if(min <= index || index <= max) {current = index;} // new index is valid
            else if (index < min){current = min;} // new index too small
        }
        return current;
    },

    // set current index
    _setCurrent: function(index){
        if (index != null && !isNaN(index)) {
            this._current = index;
            this._eventHandler(L.FCE.events.ops.updated);
        }
    },

    // fire event of type provided
    _eventHandler: function(event){
        var data, e;
        var current;
        if (this._current > -1) {current = this._stack[this._current];}
        switch (event.type){
            case L.FCE.events.ops.updated.type:
                data = {count: this._stack.length, index: this._current, current:current};
                e = new L.FCE._Base.Event({source: this, type:event.type,message:event.message,data:data});
                break;
            case L.FCE.events.ops.workflow.type:
                data = {value: this.workflow, current:current};
                e = new L.FCE._Base.Event({source: this, type:event.type,message:event.message,data:data}) ;
                break;
            // case L.FCE.events.ops.action.type:
            //     data = {action: this._stack[this._current].source};
            //     e = new L.FCE._Base.Event({source: this, type:event.type,message:event.message,data:data}) ;
            //     break;
        }
        L.FCE.fire(event.type, e);
    }
});
