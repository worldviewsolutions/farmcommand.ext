L.FCE.Data = L.Class.extend({

    // sequences for layer groups, feature layers
    ids: (function(){
        var self = {};
        self.lyrIdSeq =  -1;
        self.groupIdSeq = -1;
        var getNextFeatureLayerId= function(){
            self.lyrIdSeq += 1;
            return self.lyrIdSeq;
        };
        var getNextGroupId = function(){
            self.groupIdSeq += 1;
            return self.groupIdSeq;
        };
        var resetIds = function(){
            self.lyrIdSeq = -1;
            self.groupIdSeq = -1;
        };
        return {
            reset: resetIds,
            nextGroup: getNextGroupId,
            nextFeature: getNextFeatureLayerId
        };
    }()),

    create: function(storeId){
        // Empty layer with default properties, events
        var baseLayer = L.geoJson(null, {

            // properties on layer group
            storeId: storeId,
            editable: false,
            visible: true,
            movable: false,
            activeGroupId: null,
            

            unloadAsTarget: function() {
                var groupLayer = L.FCE.store[this.storeId];
                groupLayer.options.disableEdits();
                groupLayer.options.setGroupStyle(L.FCE.targetData, L.FCE.options.layers.styles.default);
                groupLayer.options.editable = false;
                groupLayer.options.activeGroupId = null;
            },
            // event handling
            isEditable: function () {
                // this is bound to state of event binding, thus need to call current data state
                return L.FCE.store[this.storeId].options.editable;
            },

            offsetMarker: function (icon, offset) {
                var iconMarginTop = parseInt(icon.style.marginTop, 10) - offset,
                    iconMarginLeft = parseInt(icon.style.marginLeft, 10) - offset;

                icon.style.marginTop = iconMarginTop + 'px';
                icon.style.marginLeft = iconMarginLeft + 'px';
            },

            setMarkerStyle: function (marker, styleMode, activate) {
                if (L.Util.typeOf(marker) !== 'marker') { return; }
                var cls = L.FCE.options.layers.styles[styleMode].marker.class;
                var icon = marker._icon;
                if (!!activate) {
                    L.DomUtil.addClass(icon, cls);
                    // Offset as the border will make the icon move.
                    this.offsetMarker(icon, 4);

                } else {
                    L.DomUtil.removeClass(icon, cls);
                    // Offset as the border will make the icon move.
                    this.offsetMarker(icon, -4);
                }
            },

            removeMarkerStyle: function(marker) {
                if (L.Util.typeOf(marker) !== 'marker' || marker.editEnabled()) { return; }
                var icon = marker._icon;
                var markerModes = L.FCE.options.layers.styles;
                var mode = Object.keys(markerModes).find(function(mode) {
                    if (!!markerModes[mode].marker.class) {
                        return L.DomUtil.hasClass(icon, markerModes[mode].marker.class);
                    }
                }, this);
                if (!!mode) {
                    L.DomUtil.removeClass(icon, markerModes[mode].marker.class);
                    this.offsetMarker(icon, -4);
                }
            },

            // Enable/disable edits, update styling, fire edit mode start/stop to events
            setEditMode: function(layersToEnable, layersToDisable, e) {
                var e = e || {};
                layersToEnable = layersToEnable || [];
                layersToDisable = layersToDisable || [];
                var activateEditMode = layersToEnable.length > 0;
                var groupId, geometryType;
                layersToDisable.forEach(function(layer) {
                    layer.disableEdit();
                    this.removeMarkerStyle(layer);
                }, this);
                layersToEnable.forEach(function(layer) {
                    layer.enableEdit();
                    this.setMarkerStyle(layer, 'edit', true);
                    groupId = layer.options.group_id;
                    geometryType = L.Util.typeOf(layer);
                }, this);
                var editModes =  L.FCE.events.modes.edit;
                var mode = activateEditMode ? editModes.enable.type : editModes.disable.type;
                // if edit mode start, track group id and geometryTyp
                if (activateEditMode) {
                    e.groupId = groupId; // used for setting active group id
                    e.geometryType = geometryType; // used for setting geometry type on draw menu
                }
                L.FCE.fire(mode, e);
            },
        
            setEdits: function(e) {
                // do not respond if using selection key
                if (e.originalEvent.ctrlKey) { return;}
                // do not set edits if on another layer if edits already active
                if (L.FCE.events.editMode && e.target.options.group_id !== this.activeGroupId) {
                    return;
                }
                e.targetLayer = e.target; // track target layer when event forwarded to edit mode
                this.toggleEdits(e.target.options.group_id, e);
            },  

            //enable or disable edits on layers by groupid, with event firing via set edit mode
            toggleEdits: function (groupId, e) {
                if (!(!!this.isEditable())) { return;}
                var groupLayer = L.FCE.store[this.storeId];
                var layerMap = {enable:[], disable:[]};
                groupLayer.eachLayer(function (layer) {
                    // Only enable edits if edits not already active
                    if (layer.options.group_id === groupId) {
                        if(!layer.editEnabled()) {
                            layerMap.enable.push(layer);
                            return;
                        }
                    }
                    layerMap.disable.push(layer);
                });
                this.setEditMode(layerMap.enable, layerMap.disable, e);
            },

            // disable edits on layers by groupid, with event firing
            disableEdits: function(groupId) {
                var layerMap = {disable:[]};
                if (!(!!this.isEditable())) { return; }
                var groupLayer = L.FCE.store[this.storeId];
                groupLayer.eachLayer(function (layer) {
                    // if groupId not passed in, disable all
                    if (!!groupId) {
                        // if groupId passed in, apply to all matching 
                        if (layer.options.group_id === groupId) {
                            layerMap.disable.push(layer);
                        }
                    }
                    layerMap.disable.push(layer);
                });
                this.setEditMode([], layerMap.disable);
            },

            setSelection: function(groupLayer, groupId) {
                var self = this;
                var styles = L.FCE.options.layers.styles;
                // get style for group layers if targeted
                var groupLayerStyle = groupLayer.options.editable ? styles.target : styles.default
                groupLayer.eachLayer(function(layer) {
                    if (layer.options.group_id === groupId) {
                        // if selected go to groupLayer style else selection style
                        var layerStyle = layer.options.selected ? groupLayerStyle : styles.selected;
                        var layerType = L.Util.typeOf(layer);
                        if (layerType === 'marker') {
                            layer.options.selected ? self.removeMarkerStyle(layer) : self.setMarkerStyle(layer, 'selected', true)
                        }
                        else {
                            layer.setStyle(layerStyle[layerType]);
                        }
                        // flip selection
                        layer.options.selected = !layer.options.selected;
                        // add or remove selection from stact
                        L.FCE.storage.updateSelected(this.storeId, groupId, layer.options.selected);
                    }
                })
            },

            clearSelection: function() {
                var groupLayer = L.FCE.store[this.storeId];
                if (groupLayer === undefined) {return;}
                var styles = L.FCE.options.layers.styles;
                var groupLayerStyle = groupLayer.options.editable ? styles.target : styles.default;
                groupLayer.eachLayer(function(layer) {
                    var layerType = L.Util.typeOf(layer);
                    if (layerType === 'marker') {
                        groupLayer.options.removeMarkerStyle(layer);
                    }
                    else {
                        groupLayer.options.setLayerStyle(layer, groupLayerStyle);
                    }
                    groupLayer.options.setLayerStyle(layer, groupLayerStyle);
                    layer.options.selected = false;
                })
            },

            addData: function (geoJson, style) {
                style = style || L.FCE.options.layers.styles.target;
                // Call when new feature is created
                L.FCE.targetData.addData(geoJson);
                var groupLayer = L.FCE.targetData.addTo(L.FCE.map);
                groupLayer.options.setGroupStyle(groupLayer, style);
            },

            updateData: function (geoJson, style) {
                style = style || L.FCE.options.layers.styles.default;
                var groupLayer = L.FCE.store[this.storeId];
                groupLayer.clearLayers();
                groupLayer.addData(geoJson);
                groupLayer.options.setGroupStyle(groupLayer, style);
                groupLayer.addTo(L.FCE.map);
                groupLayer.options.activeGroupId = null;
            },


            clickEvent: function(e) {
                var groupLayer = L.FCE.store[this.storeId];
                // if  (e.originalEvent.altKey && e.target.editEnabled()) {
                //     e.target.editor.newHole(e.latlng); 
                // }
                // only select if targetData active
                if (!!L.FCE.targetData && e.originalEvent.ctrlKey && !L.FCE.events.editMode) {
                    var groupId = e.target.options.group_id;
                    groupLayer.options.setSelection(groupLayer, groupId)
                }
            },

            // Bind to each layer events whenever data is added
            onEachFeature: function (feature, fLayer) {
                var f_id = feature.properties.f_id ? feature.properties.f_id : L.FCE.data.ids.nextFeature();
                var group_id = feature.properties.group_id ? feature.properties.group_id : L.FCE.data.ids.nextGroup();
                fLayer.options.f_id = f_id;
                fLayer.options.group_id = group_id;
                fLayer.options.selected = false;
                fLayer.on('dblclick', this.setEdits, this);
                fLayer.on('click', this.clickEvent, this);
            },

            // Called by leaflet when features are added, set style to default
            style: function(feature) {
                var featureType = feature.geometry.type.toLowerCase();
                if (featureType === 'marker') {return;}
                return L.FCE.options.layers.styles.default[featureType];
            },

            setLayerStyle: function (layer, style) {
                var layerType = L.Util.typeOf(layer);
                if (layerType === 'marker') { return; }
                layer.setStyle(style[layerType]);
            },
            setGroupStyle: function(groupLayer, style) {
                groupLayer.eachLayer(function(layer) {
                    groupLayer.options.setLayerStyle(layer, style);
                })
            },
            resetGroupstyle: function(groupLayer) {
                var styles = L.FCE.options.layers.styles;
                var groupLayerStyle = groupLayer.options.editable ? styles.target : styles.default;
                groupLayer.options.setGroupStyle(groupLayer, groupLayerStyle);
            }

        });
        return baseLayer;
    },
    
    // Insert geoJson and add to map
    load: function (geoJson, style) {
        // check for uid, uid dne in store
        var storeId = geoJson.properties[L.FCE.options.data.uid];
        if (isNaN(storeId) || storeId < 0 || storeId in L.FCE.store){
            return L.FCE.store[storeId];
        }
        // Validate for feature collection and correct properties
        if (!this.validateGeoJSON(geoJson)) { console.log("invalid geojson"); return; }

        geoJson = this.formatGeoJSON(geoJson);
        var groupLayer = this.create(storeId);

        groupLayer.addData(geoJson);
        groupLayer.setStyle(style);
        return groupLayer;
    },

    unload: function (storeId) {
 
    },

    // event interaction

    commitDrawing: function (layer) {
        var geoJson = layer.toGeoJSON();
        var groupId = L.FCE.targetData.options.activeGroupId;
        // only add data if not already in group layer, otherwise layer is auto updated
        if (layer.options.f_id == undefined) {
            // if layer being extended
            if (!!groupId) {
                geoJson.properties.group_id = groupId;
            }
            L.FCE.targetData.options.addData(geoJson);
            L.FCE.map.removeLayer(layer);
        }
        return layer;
    },

    reformatGeoJSON: function(groupLayer) {
        return L.FCE.data.formatGeoJSON(L.FCE.data.unFormatGeoJSON(groupLayer));
    },

    // apply tracking ids (group ids, feature ids), and break up multgipart features
    formatGeoJSON: function(geoJson){
        var features = [];
        var isMulti = function(feat){
            var hasType = feat.geometry.type.toLowerCase().lastIndexOf("multi") > -1;
            return hasType;
        };
        geoJson.features.forEach(function(feature){
            var type = feature.geometry.type;
            if (isMulti(feature)){
                var parts = feature.geometry.coordinates;
                var group_id = this.ids.nextGroup();
                var geoType = type.toLowerCase().replace(/multi/g,'').replace(/^[a-z]/, function (x) {return x.toUpperCase();});
                parts.forEach(function(part){
                    // make new feature = feature.properties + part.geometry
                    var featurePart = L.Util.clone(feature);
                    featurePart.geometry = {type: geoType, coordinates: [part]};
                    featurePart.properties.group_id = group_id;
                    featurePart.properties.f_id = this.ids.nextFeature();
                    features.push(featurePart);
                }, this);
                return;
            }
            else{
                feature.properties.group_id = this.ids.nextGroup();
                feature.properties.f_id = this.ids.nextFeature();
                features.push(feature);
                return;
            }
        }, this);
        geoJson.features = features; //replace original array with mapped and tracked values
        return geoJson;
    },

    // rejoin multipart features, and remove tracking ids
    unFormatGeoJSON: function(groupLayer){
        var groups = {};

        groupLayer.eachLayer(function(layer) {
            var feature = layer.toGeoJSON();
            var groupId = layer.options.group_id;
            if (groupId in groups){
                var gType = groups[groupId].geometry.type;
                var isMulti = gType.toLowerCase().lastIndexOf("multi") > -1;
                groups[groupId].geometry.type = (isMulti)? gType : "Multi" + gType;
                // since polygon is array of an array of coordinates, push first
                groups[groupId].geometry.coordinates.push(feature.geometry.coordinates[0]);
                return;
            }
            else{
                groups[groupId] = feature;
                return;
            }
        })
        // for feature groups remove FCE properties;
        var features = [];
        for (var key in groups){
            if (groups.hasOwnProperty(key) && groups[key].hasOwnProperty("properties")){
                // remove FCE properties
                delete groups[key].properties.f_id;
                delete groups[key].properties.group_id;
                features.push(groups[key]);
            }
        }
        var geoJson = groupLayer.toGeoJSON();
        geoJson.features = features;
        return geoJson;
    },
    
    export: function (groupLayer) {
        var geoJson = this.unFormatGeoJSON(groupLayer);
        if (!geoJson.hasOwnProperty("properties")){geoJson.properties = {};}
        geoJson.properties[L.FCE.options.data.uid] = groupLayer.options.storeId;
        return geoJson;
    },

    validateGeoJSON: function (geoJson) {
        if (geoJson.type !== "FeatureCollection" || ('properties' in geoJson && !(L.FCE.options.data.uid in geoJson.properties))) {
            return false;
        }
        if (!('properties' in geoJson)) { return false; }
        geoJson.features.forEach(function (feature) {
            if (!('f_id' in feature.properties) || !('group_id' in feature.properties)) { return false; }
        });
        return true;
    },
    validateGeometry: function (geoJson, polyArrayId){
        if (!geoJson){return false;}
        polyArrayId = polyArrayId || 0;
        var dataCheck = function(){
            if (!geoJson.geometry) {return false;}
            if (!geoJson.geometry.type) {return false;}
            if (!geoJson.geometry.coordinates) {return false;}
            return Array.isArray(geoJson.geometry.coordinates);
        }();
        if (dataCheck){
            var parts = geoJson.geometry.coordinates;
            switch (geoJson.geometry.type){
                case "Point":
                    return parts.length === 2;
                case "LineString":
                    return parts.length > 1;
                case "Polygon":
                    parts = geoJson.geometry.coordinates[polyArrayId];
                    if (!parts) {return false;}
                    if (!Array.isArray(parts)) {return false;}
                    return (parts.length - 1) > 2 ;
            }
        }
        return false;
    },
    validateCoords: function(type, coords){
        switch (type){
            case "Point":
                return coords === 1;
            case "LineString":
                return coords > 1;
            case "Polygon":
                return coords > 2;
        }
    },
    compareEditableGeoJson: function(a,b){
        var diffs = [];
        var diffOrder = -1;
        var addDiff = function(values, msg, order){
            diffOrder = order;
            diffs.push({a:values[0],b:values[1],issue:msg});
        };
        //var deepdiffs = L.Util.deepdiff(a, b);
        var aCoords = a.geometry.coordinates;
        var bCoords = b.geometry.coordinates;
        if (aCoords.length > bCoords.length) {addDiff([aCoords, bCoords],"part deleted", 0);}
        if (aCoords.length < bCoords.length) {addDiff([aCoords, bCoords], "part added", 0);}
        if (aCoords.length === bCoords.length) {
            // compare vertex counts
            for (var i = 0; i < aCoords.length; i++) {
                var aParts = aCoords[i];
                var bParts = bCoords[i];
                if (aParts.length > bParts.length) {
                    addDiff([aParts, bParts], "vertex deleted", 1);
                }
                if (aParts.length < bParts.length) {
                    addDiff([aParts, bParts], "vertex added", 1);
                }
                if (aParts.length === bParts.length) {
                    // compare vertex coordinates
                    for (var j = 0; i < aParts.length - 1; i++) {
                        var aPairs = aParts[i];
                        var bPairs = bParts[i];
                        if (aPairs[0] !== bPairs[0] || aPairs[1] != bPairs[1]) {
                            addDiff([aPairs, bPairs], "vertex moved", 2);
                        }
                    }
                }
            }
        }
        var value = (diffs.length === 0)?"identical":diffs;
        return {identical:(diffs.length === 0), order:diffOrder, values: value, a:a,b:b};
    }
});