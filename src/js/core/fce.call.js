L.FCE.callAction = function(args){
    var toolbar, action, response, result;
    response = {
        result: 'process not started',
        message: 'invalid arguments'
    };
    //set default response  (fail + message)
    var init = function(){
        //check args
        //for valid inputs
        args = {
            toolbar: '',
            action: '',
            options: '',
            callback: function(){}
        };
        // update response
    };
    var cancel = function(){
        // ... removeHooks();
        // ... cleanup data, store, states, etc...
    };
    var start = function(){
        // add cancel callback to response
        // apply options as needed
        // call start
        // update response again
        response.cancel = cancel;
        response.result = 'success';
        response.message = args.action.name + " started.";
    };

    var onFinish = function(results){
        // update result
        var data = function(data){
            // validate result
            // and alter data
            return results.data;
        };
        var result = {
            result: results.result, //'success' || 'failure'
            message: results.msg, //'details of result',
            data: data
        };
        // pass to callback
        callback(result);
    };
    if (init()){
        start();
    }
    return response;

};