"use strict";
var name = 'redo';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: null,
    bubbles: false,
    options: {
        toolbarIcon: {
            tooltip: 'Redo last action',
            html: '<i class="fa fa-mail-forward"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },    
    removeHooks: function () {

    }
});