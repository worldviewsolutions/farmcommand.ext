"use strict";
var name = 'editTool';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    cursor: null,
    link: null,
    workflow: {active:false},
    options: {
        toolbarIcon: {
            tooltip: 'Edit Shapes',
            html: '<i class="fa fa-location-arrow"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },
    toggleCursor: function () {
        this.cursor = !this.cursor ? new L.FCE.Cursor() : this.cursor;
        this.cursor.toggle();
    },
    removeHooks: function () {

    }
});