"use strict";
var name = 'deleteTool';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: null,
    options: {
        toolbarIcon: {
            tooltip: 'Delete',
            html: '<i class="fa fa-trash"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },
    removeHooks: function () {

    }
});