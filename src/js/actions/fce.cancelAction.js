"use strict";
var name = 'cancel';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: null,
    bubbles: false,
    options: {
        toolbarIcon: {
            tooltip: 'Cancel',
            html: '<i class="fa fa-close"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },
    removeHooks: function () {
    
    }
});