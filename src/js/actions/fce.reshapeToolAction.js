"use strict";
var name = 'reshapeTool';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: 'reshapeMenu',
    workflow: {active:false},
    options: {
        toolbarIcon: {
            tooltip: 'Reshape Features',
            html: '<i class="fa fa-edit"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onBoundClick();
    },
    reshapeHooks: function (reshapeType) {
        switch(reshapeType) {
            case 'clone':

                break;
            case 'split':

                break;
            case 'merge':

                break;
            case 'clip':

                break;
        }
    },
    removeHooks: function () {

    }
});