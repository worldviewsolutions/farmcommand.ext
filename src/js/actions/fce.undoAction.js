"use strict";
var name = "undo";
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: null,
    bubbles: false,
    options: {
        toolbarIcon: {
            tooltip: 'Undo last action',
            html: '<i class="fa fa-mail-reply"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },    
    removeHooks: function () {

    }
});