"use strict";
var name = "end";
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: null,
    bubbles: false,
    options: {
        toolbarIcon: {
            tooltip: 'Finish',
            html: '<i class="fa fa-check"></i>', //fa fa-check-circle
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onClick(this);
    },
    removeHooks: function () {

    }
});