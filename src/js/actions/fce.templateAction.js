"use strict";
//****Private****//
var name = 'templateAction';
var saName = 'sampleName';
var cName = 'cancelName';
L.FCE.SubActions[saName] = L.FCE._Base.MenuAction.extend({
    // Custom props here
    name: saName,
    options: {
        toolbarIcon: {
            html: 'Label',
            className: 'leaflet-fce-' + name + '-menu-' + saName,
            tooltip: 'Tooltip Message'
        }
    },
    addHooks: function () {
        this.parentAction.menuSelection(this);
    }
});
L.FCE.SubActions[cName] = L.FCE._Base.MenuAction.extend({
    name: cName,
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-times"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + cName,
            tooltip: 'Cancel'
        }
    }
});

var subActions = [
    L.FCE.SubActions[saName],
    L.FCE.SubActions[cName]
];
var subToolbar = new L.Toolbar({
    className: 'leaflet-fce-' + name + '-menu',
    actions: subActions,
    options: {
    }
});

//****Action Definition****//
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name, // action name
    value: "", // action value
    link: null,  // if bound action, provide name of linked action here
    options: {
        toolbarIcon: {
            tooltip: 'Tooltip message',  // message on button hover
            html: '<i class="fa fa-ban"></i>',
            className: 'leaflet-fce-' + name
        },
        subToolbar: subToolbar || null
    },
    addHooks: function () {
        if (!this.options.subToolbar) {
            this.onBoundClick();
        }
        else {
            this.onMenuClick();
        }
    }
});