"use strict";
var name = 'drawTool';
L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    link: 'drawMenu',
    workflow: {active:false},
    message: "",
    options: {
        toolbarIcon: {
            tooltip: 'Draw a Shape',
            html: '<i class="fa fa-pencil"></i>',
            className: 'leaflet-fce-' + name
        }
    },
    addHooks: function () {
        this.onBoundClick();
    },
    removeHooks: function () {
    }
});