"use strict";
//****Private****//
var name = 'reshapeMenu';
var cName = 'selectClone';
var sName = 'selectSplit';
var mName = 'selectMerge';
var clName = 'selectClip';
//****SubActions****//
L.FCE.SubActions[cName] = L.FCE._Base.MenuAction.extend({
    name: cName,
    reshapeType: 'clone',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-sign-in"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + cName,
            tooltip: 'Clone Feature'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    },
    removeHooks: function () {
        L.FCE.map.editTools.stopDrawing();
    }
});
L.FCE.SubActions[sName] = L.FCE._Base.MenuAction.extend({
    name: sName,
    reshapeType: 'split',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-windows"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + sName,
            tooltip: 'Split Polygon Feature'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    },
    removeHooks: function () {
        L.FCE.map.editTools.stopDrawing();
    }
});
L.FCE.SubActions[mName] = L.FCE._Base.MenuAction.extend({
    name: mName,
    reshapeType: 'merge',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-plus-square"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + mName,
            tooltip: 'Merge Features'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    },
    removeHooks: function () {
        L.FCE.map.editTools.stopDrawing();
    }
});
L.FCE.SubActions[clName] = L.FCE._Base.MenuAction.extend({
    name: clName,
    reshapeType: 'clip',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-scissors"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + clName,
            tooltip: 'Clip Polygon'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    },
    removeHooks: function () {
        L.FCE.map.editTools.stopDrawing();
    }
});


var subActions = [
    L.FCE.SubActions[cName],
    L.FCE.SubActions[sName],
    L.FCE.SubActions[mName],
    L.FCE.SubActions[clName]
];
var subToolbar = new L.Toolbar({
    className: 'leaflet-fce-' + name + '-menu',
    actions: subActions,
    options: {
    }
});

L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    options: {
        toolbarIcon: {
            tooltip: 'Select a Reshape Action',
            html: '<i class="fa fa-cubes"></i>',
            className: 'leaflet-fce-' + name
        },
        subToolbar: subToolbar || null
    },
    addHooks: function () {
        this.onMenuClick();
    }
});