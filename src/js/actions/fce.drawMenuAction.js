"use strict";
//****Private****//
var name = 'drawMenu';
var spName = 'selectPolygon';
var splName = 'selectPolyline';
var smName = 'selectMarker';
var scName = 'selectCircle';
//****SubActions****//
L.FCE.SubActions[smName] = L.FCE._Base.MenuAction.extend({
    name: smName ,
    value:'marker',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-map-marker"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + smName,
            tooltip: 'Draw a Marker'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    },
    removeHooks: function () {
        L.FCE.map.editTools.stopDrawing();
    }
});
L.FCE.SubActions[spName] = L.FCE._Base.MenuAction.extend({
    name: spName ,
    value:'polygon',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-square"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + spName,
            tooltip: 'Draw a Polygon'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    }
});
L.FCE.SubActions[splName] = L.FCE._Base.MenuAction.extend({
    name: splName,
    value: 'polyline',
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-minus"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + splName,
            tooltip: 'Draw a Polyline'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    }
});
L.FCE.SubActions[scName] = L.FCE._Base.MenuAction.extend({
    name: "selectCircle",
    value: 'circle',
    options: {
        toolbarIcon: {
            html: 'Circle',
            tooltip: 'Draw Circle'
        }
    },
    addHooks: function () {
        this.parentAction.onMenuSelection(this);
    }
});

var subActions = [
    L.FCE.SubActions[smName],
    L.FCE.SubActions[splName],
    L.FCE.SubActions[spName],
    // L.FCE.SubActions[scName]
    ];
var subToolbar = new L.Toolbar({
    className: 'leaflet-fce-'+ name +'-menu',
    actions: subActions,
    options:{
    }
});

L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name, 
    options: {
        toolbarIcon: {
            tooltip: 'Select Geometry',
            html: '<i class="fa fa-circle-thin"></i>',
            className: 'leaflet-fce-' + name
        },
        subToolbar: subToolbar || null
    },
    addHooks: function () {
        this.onMenuClick();
    }
});