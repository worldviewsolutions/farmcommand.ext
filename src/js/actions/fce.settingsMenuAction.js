"use strict";
//****Private****//
var name = 'settingsMenu';
var tsName = 'toggleSnapping';
var ttName = 'toggleTopology';
//****SubActions****//
L.FCE.SubActions[tsName] = L.FCE._Base.MenuAction.extend({
    name: tsName,
    value: false,
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-dot-circle-o"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + tsName,
            tooltip: 'Toggle Vertex Snapping'
        }
    },
    addHooks: function () {
        this.parentAction.onClick(this);
    },
    removeHooks: function () {

    }
});
L.FCE.SubActions[ttName] = L.FCE._Base.MenuAction.extend({
    name: ttName,
    value: false,
    options: {
        toolbarIcon: {
            html: '<i class="fa fa-plus-square-o"></i>',
            className: 'leaflet-fce-' + name + '-menu-' + ttName,
            tooltip: 'Toggle Topology Editing'
        }
    },
    addHooks: function () {
        this.parentAction.onClick(this);
    },
    removeHooks: function () {

    }
});


var subActions = [
    L.FCE.SubActions[tsName],
    L.FCE.SubActions[ttName]
];

var subToolbar = new L.Toolbar({
    className: 'leaflet-fce-' + name + '-menu-toolbar',
    actions: subActions,
    options: {
    }
});

L.FCE.Actions[name] = L.FCE._Base.Action.extend({
    name: name,
    options: {
        toolbarIcon: {
            tooltip: 'Change a setting',
            html: '<i class="fa fa-gear"></i>',
            className: 'leaflet-fce-' + name
        },
        subToolbar: subToolbar || null
    },
    addHooks: function () {
        this.onMenuClick();
    }
});