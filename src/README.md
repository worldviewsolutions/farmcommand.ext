# Leaflet.FCE Plugin:  

The Farm Command Editor plugin is lightweight, built using leaflet.toolbar and leaflet.editable.  It provides feature editing + utility services.     

## Prerequisites
Build dependencies are included in the packages.json file.
```
  "dependencies": {
    "leaflet": "^0.7.7", 
    "leaflet-toolbar": "^0.1.2"  
    "leaflet-editable": "^0.7.0" 
  }
```

## Setup
With the exception of leaflet.js (v 0.7.7), all plugin dependencies are provided in the ./libs directory - required dependencies should be loaded at runtime.  
The above prerequisites list, as well as the included example.html, are currently all that are required.
We locally use nodejs, npm, and browserify to manage these dependencies.  


## Code Examples
Plugin basic usage:
```
    // initialize FCE
    var fce = L.FCE.addTo(map);
    // load draw toolbar
    fce.loadToolbar('draw');
    // load edit toolbar
    fce.loadToolbar('edit');
```
Plugin configuration (see Configuration options, below):
```
    // initialize FCE
    var fceDefaults = {};
    var fce = L.FCE(fceDefaults).addTo(map);
    // load draw toolbar
    var drawOptions = {};
    fce.loadToolbar('draw', drawOptions);    
```

Set Plugin mode (global state):
currently, mode definition must be defined on initialize of plugin
```
    var fceDefaults= {modes:
     drawPolygon: {
                draw: {
                    allActions: 'default',
                    actions: {
                        drawMenu: {
                            allActions: 'inactive',
                            subActions: {
                                selectPolygon: {
                                    state: 'select'
                                }
                            }
                        },
                        drawTool: {
                            state: 'enable'
                        }
                    }
                },
                general: {
                    allActions: 'default'
                }
            }
    };
     var fce = L.FCE(fceDefaults).addTo(map);
    // ...after initialize...
    fce.setMode('drawPolygon');   
```

Creating a new layer
Layer creation requires an id or array of id, with a secondary argument for layer style
Empty group layers are created and stored within FCE
```
var ids = 0 || [0,1,2,3];
var results = L.FCE.addData(ids)
results.forEach(function(lyrInfo){
    console.log("added new L.groupLayer for id=" + lyrInfo.id + ": " + lyrInfo.data);
});
```

Loading existing data
geoJSON must be in a feature collection and have unique ID associated with the feature collection
properties named 'uid'
```
var geoJson = {...} || [{...},{...}];
var results = L.FCE.loadData(geoJSON);
results.forEach(function(lyrInfo){
    console.log("added L.groupLayer as geojson.properties.id=" + lyrInfo.id + ": " + lyrInfo.data);
});
```

Editing a layer
Pass in the ID of newly created or loaded layer 
```
L.FCE.setMode('target', layerId)
```

Saving a layer
Pass in the ID or array of IDs and receive geoJSON feature collection
```
var ids = 0 || [0,1,2,3];
var results = L.FCE.exportData(ids);
results.forEach(function(info){
    console.log("exported L.groupLayer as geojson for id=" + info.id + ": " + info.data);
});
```

Clearing edits
Removes all operations from op stack and resets data to start of edit session
```
L.FCE.clearEdits();
```

Delete layer
Remove layer or layers from map and plugin, clears any active edits
```
var ids = 0 || [0,1,2,3];
L.FCE.removeData(ids)
```

Check for existing edits
Returns true if existing edits, false if not. Used to check if edits are made so they can be saved or discarded
before changing layers or app state
```
L.FCE.hasEdits();
```

todo - add code examples/info for:
- set fc/lyr target & enable editing
- leaflet fce public events table

### Configuration Options
Plugin default configuration options may be provided when initializing FCE:
```
    // initialize FCE
    var fceDefaults = {
        draw: {     
            control: {
                position: 'topleft',
                isVisible: true
            },
            actions: {
                drawMenu: {
                    state: 'inactive'
                }
            }
        }
    };
    var fce = L.FCE(fceDefaults).addTo(map); //draw toolbar loads in topleft with drawMenu greyed out    
```
Toolbar configuration options may also be supplied at load:
```  
    // load draw toolbar
    var drawOptions = {     
        control: {
            position: 'topleft',
            isVisible: true
        },
        actions: {
            drawMenu: {
                state: 'inactive'
            }
        }
    };
    fce.loadToolbar('draw', drawOptions); //loads in topleft with drawMenu greyed out    
```

Note: the options configuration structure may change over development life cycle

## Deployment Notes
Demo 1.  Architecture and Leaflet Toolbar UI. Includes General, Draw, and Editing toolbars.
Demo 2.  Operations, Storage, and Map Layers.
Release 2 - 09/07/2016

This release coincides with Demo 2.  
Plugin currently features ability to:
- create, load and show demo toolbar(s).
- apply custom settings (options) to plugin components.
- set existing and custom modes - which can affect visibility, enabled state, active state, and mock click events.
- load leaflet map layers, target layers, and select layer features.
- plugin operations stack management (start, stop, reset, add, remove, etc.) and operational events/data retrieval (by listening to specific leaflet events).



## Versioning

Source code is hosted on bitbucket [project page](https://bitbucket.org/worldviewsolutions/farmcommand.ext)

## Authors
[Worldview Solutions, Inc.](worldviewsolutions.com)
* **Michael Kolonay** - *Technical Lead*
* **Jared Dunbar** - *GIS Web Developer*
* **Brian Brown** - *GIS Web Developer*
* **Jay Cummins** - *GIS Developer*

## License

NA


